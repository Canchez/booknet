package com.example.booknet

import androidx.appcompat.app.AppCompatActivity
import dagger.hilt.android.AndroidEntryPoint

/**
 * Main activity of app
 */
@AndroidEntryPoint
class MainActivity : AppCompatActivity(R.layout.activity_main)