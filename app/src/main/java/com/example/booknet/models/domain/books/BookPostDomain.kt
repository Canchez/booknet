package com.example.booknet.models.domain.books

import com.example.booknet.models.presentation.books.BookStatus

data class BookPostDomain(
    val title: String,
    val pages: Long,
    val description: String?,
    val status: BookStatus,
)
