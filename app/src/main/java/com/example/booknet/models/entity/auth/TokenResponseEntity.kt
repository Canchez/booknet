package com.example.booknet.models.entity.auth

import com.google.gson.annotations.SerializedName

/**
 * Data for Get Token response
 */
data class TokenResponseEntity(
    @SerializedName("refresh") val refreshToken: String,
    @SerializedName("access") val accessToken: String?,
)