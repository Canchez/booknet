package com.example.booknet.models.presentation.chats

import com.example.booknet.models.presentation.users.UserInfo
import org.joda.time.DateTime

/**
 * Presentation model for chat message
 *
 * @param id ID of the chat message
 * @param senderId ID of the sender
 * @param senderData Model of the sender
 * @param content Content of the message
 * @param createdAt [DateTime] when message was created
 */
data class ChatMessage(
    val id: Long,
    val senderId: Long,
    val senderData: UserInfo,
    val content: String,
    val createdAt: DateTime,
)
