package com.example.booknet.models.domain.discussions

import com.example.booknet.models.domain.users.UserInfoDomain
import org.joda.time.DateTime

/**
 * Comment model for domain
 *
 * @param id ID of comment
 * @param message Text of comment
 * @param author Author info
 * @param dateCreated Date of comment creation
 * @param dateModified Date of comment modification
 */
data class CommentDomain(
    val id: Long,
    val message: String,
    val author: UserInfoDomain,
    val dateCreated: DateTime,
    val dateModified: DateTime,
)
