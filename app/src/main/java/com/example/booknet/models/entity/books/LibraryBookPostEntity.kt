package com.example.booknet.models.entity.books

import com.google.gson.annotations.SerializedName

/**
 * Entity for POST requests in library
 *
 * @param bookId ID of the book to add to the library
 * @param status Status of the book
 */
data class LibraryBookPostEntity(
    @SerializedName("book_id") val bookId: Long,
    @SerializedName("status") val status: String,
)