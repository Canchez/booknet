package com.example.booknet.models.entity.users

import com.google.gson.annotations.SerializedName

/**
 * User entity
 *
 * @param id ID of the user
 * @param firstName First Name of user
 * @param lastName Last Name of user
 * @param email Email of user
 * @param avatar Avatar of user
 */
data class UserEntity(
    @SerializedName("id") val id: Long,
    @SerializedName("first_name") val firstName: String,
    @SerializedName("last_name") val lastName: String,
    @SerializedName("email") val email: String,
    @SerializedName("avatar") val avatar: String?,
)