package com.example.booknet.models.entity.chats

import com.example.booknet.models.entity.users.UserEntity
import com.google.gson.annotations.SerializedName

/**
 * Entity model for chat
 *
 * @param id ID of the chat
 * @param name Name of the chat
 * @param image URL for avatar image
 * @param attendeesIds List of attendees IDs of the chat
 * @param attendees List of attendees of the chat
 * @param messages List of messages of the chat
 */
data class ChatEntity(
    @SerializedName("id") val id: Long,
    @SerializedName("name") val name: String,
    @SerializedName("image") val image: String?,
    @SerializedName("attendees") val attendeesIds: List<Long>,
    @SerializedName("attendees_data") val attendees: List<UserEntity>,
    @SerializedName("messages") val messages: List<ChatMessageEntity>
)
