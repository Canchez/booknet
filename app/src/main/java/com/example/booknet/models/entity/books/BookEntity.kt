package com.example.booknet.models.entity.books

import com.google.gson.annotations.SerializedName

/**
 * API Book entity
 *
 * @param id ID of the book
 * @param title Title of the book
 * @param author Author of the book
 * @param genre Genre of the book
 * @param pages Amount of pages of the book
 * @param status Status of book for current user
 * @param viewersCount Amount of viewers of the book
 * @param readersCount Amount of readers of the book
 * @param description Description of the book
 * @param averageRating Average rating of the book
 * @param reviewsCount Amount of reviews for the book
 * @param wantToReadCount Amount of people who wants to read
 * @param wantToTradeCount Amount of people who wants to trade
 */
data class BookEntity(
    @SerializedName("id") val id: Long,
    @SerializedName("title") val title: String,
    @SerializedName("author") val author: String?,
    @SerializedName("genre") val genre: String?,
    @SerializedName("pages") val pages: Long?,
    @SerializedName("status") val status: String,
    @SerializedName("viewers_count") val viewersCount: Int,
    @SerializedName("readers_count") val readersCount: Int,
    @SerializedName("description") val description: String,
    @SerializedName("average_rating") val averageRating: Double,
    @SerializedName("reviews_count") val reviewsCount: Int,
    @SerializedName("want_to_read_count") val wantToReadCount: Int,
    @SerializedName("want_to_trade_count") val wantToTradeCount: Int,
)