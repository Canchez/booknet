package com.example.booknet.models.entity.discussions

import com.google.gson.annotations.SerializedName

/**
 * Comment entity for POST actions
 *
 * @param message Text of comment
 */
data class CommentPostEntity(
    @SerializedName("message") val message: String,
    @SerializedName("discussion") val discussion: Long,
)