package com.example.booknet.models.entity.books

import com.google.gson.annotations.SerializedName

/**
 * Entity for book status
 *
 * @param status Book status
 */
data class BookStatusEntity(
    @SerializedName("status") val status: String,
)