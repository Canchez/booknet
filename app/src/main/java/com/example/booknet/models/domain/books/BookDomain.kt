package com.example.booknet.models.domain.books

/**
 * Domain Book model
 *
 * @param id ID of the book
 * @param title Title of the book
 * @param author Author of the book
 * @param genre Genre of the book
 * @param pages Amount of pages of the book
 * @param status Status of book for current user
 * @param viewersCount Amount of viewers of the book
 * @param readersCount Amount of readers of the book
 * @param description Description of the book
 * @param averageRating Average rating of the book
 * @param reviewsAmount Amount of reviews for the book
 * @param wantToReadAmount Amount of people who wants to read
 * @param wantToTradeAmount Amount of people who wants to trade
 */
data class BookDomain(
    val id: Long,
    val title: String,
    val genre: String?,
    val author: String?,
    val pages: Long?,
    val status: String,
    val viewersCount: Int,
    val readersCount: Int,
    val description: String,
    val averageRating: Double,
    val reviewsAmount: Int,
    val wantToReadAmount: Int,
    val wantToTradeAmount: Int,
)
