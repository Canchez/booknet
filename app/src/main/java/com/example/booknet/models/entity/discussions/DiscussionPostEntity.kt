package com.example.booknet.models.entity.discussions

import com.google.gson.annotations.SerializedName

/**
 * Discussion entity for POST actions
 *
 * @param title Title of the discussion
 * @param description Description of the discussion
 */
data class DiscussionPostEntity(
    @SerializedName("title") val title: String,
    @SerializedName("content") val description: String,
)