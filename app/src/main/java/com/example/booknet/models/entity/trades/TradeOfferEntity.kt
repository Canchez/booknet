package com.example.booknet.models.entity.trades

import com.example.booknet.models.entity.books.BookEntity
import com.example.booknet.models.entity.users.UserEntity
import com.google.gson.annotations.SerializedName

/**
 * Entity model for Trade offer
 *
 * @param id ID of the trade offer
 * @param senderId ID of the sender
 * @param senderData Data of the sender
 * @param receiverId ID of the receiver
 * @param receiverData Data of the sender
 * @param suggestedBooksIds IDs of the suggested books
 * @param suggestedBooksData Data of the suggested books
 * @param requestedBooksIds IDs of the requested books
 * @param requestedBooksData Data of the requested books
 * @param message Message of the trade offer
 * @param status Status of the trade offer
 */
data class TradeOfferEntity(
    @SerializedName("id") val id: Long,
    @SerializedName("sender") val senderId: Long,
    @SerializedName("sender_data") val senderData: UserEntity,
    @SerializedName("receiver") val receiverId: Long,
    @SerializedName("receiver_data") val receiverData: UserEntity,
    @SerializedName("suggested_books") val suggestedBooksIds: List<Long>,
    @SerializedName("suggested_books_data") val suggestedBooksData: List<BookEntity>,
    @SerializedName("requested_books") val requestedBooksIds: List<Long>,
    @SerializedName("requested_books_data") val requestedBooksData: List<BookEntity>,
    @SerializedName("message") val message: String,
    @SerializedName("status") val status: String,
)
