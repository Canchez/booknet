package com.example.booknet.models.entity.chats

import com.google.gson.annotations.SerializedName

/**
 * Entity for creating chat
 *
 * @param chatName Name of the chat
 * @param attendees IDS of chat attendees
 */
data class ChatPostEntity(
    @SerializedName("name") val chatName: String,
    @SerializedName("attendees") val attendees: List<Long>,
)
