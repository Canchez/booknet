package com.example.booknet.models.entity.auth

import com.google.gson.annotations.SerializedName

/**
 * Data for Refresh token response
 */
data class RefreshTokenResponseEntity(
    @SerializedName("refresh") val refreshToken: String,
    @SerializedName("access") val accessToken: String,
)