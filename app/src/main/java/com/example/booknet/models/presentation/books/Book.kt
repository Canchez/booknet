package com.example.booknet.models.presentation.books

import androidx.annotation.StringRes
import com.example.booknet.R

/**
 * Book model for presentation
 *
 * @param id ID of the book
 * @param title Title of the book
 * @param author Author of the book
 * @param genre Genre of the book
 * @param pages Amount of pages of the book
 * @param status Status of book for current user
 * @param readersCount Amount of readers of the book
 * @param description Description of the book
 * @param averageRating Average rating of the book
 * @param reviewsAmount Amount of reviews for the book
 * @param wantToReadAmount Amount of people who wants to read
 * @param wantToTradeAmount Amount of people who wants to trade
 */
data class Book(
    val id: Long,
    val title: String,
    val genre: String?,
    val author: String?,
    val pages: Long?,
    val status: BookStatus,
    val readersCount: Int,
    val description: String,
    val averageRating: Double,
    val reviewsAmount: Int,
    val wantToReadAmount: Int,
    val wantToTradeAmount: Int,
) {
    /**
     * Is book checked in trade offers
     */
    var isChecked: Boolean? = null
}

/**
 * Status of the book
 *
 * @param serverString String value for server
 * @param readableStringId ID of string resource for showing on UI
 */
enum class BookStatus(
    val serverString: String,
    @StringRes val readableStringId: Int?
) {
    FINISHED("finish", R.string.books_details_status_finished),
    READING("reading", R.string.books_details_status_reading),
    WANT_TO_READ("want_to_read", R.string.books_details_status_want_to_read),
    WANT_TO_TRADE("want_to_trade", R.string.books_details_status_want_to_trade),
    NOT_INTERESTED("not_interested", R.string.books_details_status_not_interested),
    NOT_IN_LIBRARY("", null);

    companion object {
        /**
         * Convert [serverString] to [BookStatus]
         */
        fun fromServerString(serverString: String) = when (serverString) {
            "finish" -> FINISHED
            "reading" -> READING
            "want_to_read" -> WANT_TO_READ
            "want_to_trade" -> WANT_TO_TRADE
            "not_interested" -> NOT_INTERESTED
            else -> NOT_IN_LIBRARY
        }
    }
}