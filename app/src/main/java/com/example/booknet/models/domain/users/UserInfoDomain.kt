package com.example.booknet.models.domain.users

/**
 * User info domain model
 *
 * @param id ID of the user
 * @param firstName First Name of user
 * @param lastName Last Name of user
 * @param email Email of user
 * @param avatar Avatar of user
 */
data class UserInfoDomain(
    val id: Long?,
    val firstName: String?,
    val lastName: String?,
    val email: String?,
    val avatar: String?,
)