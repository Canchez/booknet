package com.example.booknet.models.entity.reviews

import com.example.booknet.models.domain.users.UserInfoDomain

/**
 * Review model for domain level
 *
 * @param mark Mark of the review
 * @param feedback Feedback text
 * @param user User Info
 */
data class ReviewDomain(
    val mark: Int,
    val feedback: String,
    val user: UserInfoDomain,
)