package com.example.booknet.models.entity.chats

import com.google.gson.annotations.SerializedName

/**
 * Entity model for outgoing chat message for WebSockets
 *
 * @param message Text of the message
 * @param senderId ID of the sender
 * @param dialogId ID of the chat
 */
data class ChatMessageWSPostEntity(
    @SerializedName("message") val message: String,
    @SerializedName("sender_id") val senderId: Long,
    @SerializedName("dialog_id") val dialogId: Long,
)
