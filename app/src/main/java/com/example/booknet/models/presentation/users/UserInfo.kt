package com.example.booknet.models.presentation.users

/**
 * User info model for presentation on dashboard
 *
 * @param id ID of the user
 * @param firstName First Name of user
 * @param lastName Last Name of user
 * @param email Email of user
 * @param avatar Avatar of user
 */
data class UserInfo(
    val id: Long?,
    val firstName: String?,
    val lastName: String?,
    val email: String?,
    val avatar: String?,
) {
    /**
     * True if user was selected, false - otherwise
     */
    var isSelected: Boolean = false
}
