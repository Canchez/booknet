package com.example.booknet.models.entity.discussions

import com.example.booknet.models.entity.users.UserEntity
import com.google.gson.annotations.SerializedName

/**
 * Comment entity
 *
 * @param id ID of comment
 * @param message Text of comment
 * @param author Author info
 * @param dateCreated Date string of comment creation
 * @param dateModified Date string of comment modification
 */
data class CommentEntity(
    @SerializedName("id") val id: Long,
    @SerializedName("message") val message: String,
    @SerializedName("author") val author: UserEntity,
    @SerializedName("created") val dateCreated: String,
    @SerializedName("modified") val dateModified: String,
)