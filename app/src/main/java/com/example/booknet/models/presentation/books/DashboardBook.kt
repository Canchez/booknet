package com.example.booknet.models.presentation.books

/**
 * Book model for presentation on dashboard
 *
 * @param id ID of the book
 * @param title Title of the book
 */
data class DashboardBook(
    val id: Long,
    val title: String,
)