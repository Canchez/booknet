package com.example.booknet.models.domain.chats

import com.example.booknet.models.domain.users.UserInfoDomain

/**
 * Domain model for chat
 *
 * @param id ID of the chat
 * @param name Name of the chat
 * @param image URL for avatar image
 * @param attendees List of attendees of the chat
 * @param messages List of messages of the chat
 */
data class ChatDomain(
    val id: Long,
    val name: String,
    val image: String?,
    val attendees: List<UserInfoDomain>,
    val messages: List<ChatMessageDomain>,
)
