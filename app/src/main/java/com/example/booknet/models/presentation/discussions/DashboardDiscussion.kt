package com.example.booknet.models.presentation.discussions

/**
 * Discussion model for presentation on dashboard
 *
 * @param id ID of the discussion
 * @param name Name of the discussion
 */
data class DashboardDiscussion(
    val id: Long,
    val name: String
)