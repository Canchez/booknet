package com.example.booknet.models.domain.chats

import com.example.booknet.models.domain.users.UserInfoDomain

/**
 * Domain model for chat message
 *
 * @param id ID of the chat message
 * @param senderId ID of the sender
 * @param senderData Model of the sender
 * @param content Content of the message
 * @param createdAt Date string when message was created
 */
data class ChatMessageDomain(
    val id: Long,
    val senderId: Long,
    val senderData: UserInfoDomain,
    val content: String,
    val createdAt: String,
)
