package com.example.booknet.models.entity.trades

import com.google.gson.annotations.SerializedName

/**
 * Entity model for Trade offer POST/UPDATE requests
 *
 * @param senderId ID of the sender
 * @param receiverId ID of the receiver
 * @param suggestedBooksIds IDs of the suggested books
 * @param requestedBooksIds IDs of the requested books
 * @param message Message of the trade offer
 * @param status Status of the trade offer
 */
data class TradeOfferPostEntity(
    @SerializedName("sender") val senderId: Long,
    @SerializedName("receiver") val receiverId: Long,
    @SerializedName("suggested_books") val suggestedBooksIds: List<Long>,
    @SerializedName("requested_books") val requestedBooksIds: List<Long>,
    @SerializedName("message") val message: String,
    @SerializedName("status") val status: String,
)
