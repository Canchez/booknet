package com.example.booknet.models.entity.auth

import com.google.gson.annotations.SerializedName

/**
 * Data for Refresh token request
 */
data class RefreshTokenRequestEntity(
    @SerializedName("refresh") val refreshToken: String,
)