package com.example.booknet.models.presentation.discussions

import com.example.booknet.models.presentation.users.UserInfo
import org.joda.time.DateTime

/**
 * Comment model for presentation
 *
 * @param id ID of comment
 * @param message Text of comment
 * @param author Author info
 * @param dateCreated Date of comment creation
 */
data class Comment(
    val id: Long,
    val message: String,
    val author: UserInfo,
    val dateCreated: DateTime,
)
