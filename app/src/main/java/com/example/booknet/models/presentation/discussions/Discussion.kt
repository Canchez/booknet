package com.example.booknet.models.presentation.discussions

import com.example.booknet.models.presentation.users.UserInfo

/**
 * Discussion model for presentation
 *
 * @param id ID of the discussion
 * @param title Title of the discussion
 * @param description Description of the discussion
 * @param comments List of comments in discussion
 * @param author Author of the discussion
 */
data class Discussion(
    val id: Long,
    val title: String,
    val description: String,
    val comments: List<Comment>,
    val author: UserInfo,
)