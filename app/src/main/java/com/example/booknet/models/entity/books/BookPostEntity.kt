package com.example.booknet.models.entity.books

import com.google.gson.annotations.SerializedName

/**
 * API entity for book POST requests
 */
data class BookPostEntity(
    @SerializedName("title") val title: String,
    @SerializedName("pages") val pages: Long,
    @SerializedName("description") val description: String?,
    @SerializedName("status") val status: String,
)