package com.example.booknet.models.presentation.reviews

import com.example.booknet.models.presentation.users.UserInfo

/**
 * Review model for presentation
 *
 * @param mark Mark of the review
 * @param feedback Feedback text
 * @param user User Info
 */
data class Review(
    val mark: Int,
    val feedback: String,
    val user: UserInfo,
)
