package com.example.booknet.models.domain.discussions

import com.example.booknet.models.domain.users.UserInfoDomain

/**
 * Discussion model for domain
 *
 * @param id ID of the discussion
 * @param title Title of the discussion
 * @param description Description of the discussion
 * @param comments List of comments in discussion
 * @param author Author of the discussion
 */
data class DiscussionDomain(
    val id: Long,
    val title: String,
    val description: String,
    val comments: List<CommentDomain>,
    val author: UserInfoDomain,
)