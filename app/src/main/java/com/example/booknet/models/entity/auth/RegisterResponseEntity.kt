package com.example.booknet.models.entity.auth

import com.google.gson.annotations.SerializedName

/**
 * Data for Registration response
 */
data class RegisterResponseEntity(
    @SerializedName("email") val email: String,
    @SerializedName("first_name") val firstName: String,
    @SerializedName("last_name") val lastName: String,
)