package com.example.booknet.models.entity.auth

import com.google.gson.annotations.SerializedName

/**
 * Data for Get Token request
 */
data class TokenRequestEntity(
    @SerializedName("email") val email: String,
    @SerializedName("password") val password: String,
)