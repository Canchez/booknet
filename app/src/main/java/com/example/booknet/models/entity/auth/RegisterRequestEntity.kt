package com.example.booknet.models.entity.auth

import com.google.gson.annotations.SerializedName

/**
 * Data for Registration request
 */
data class RegisterRequestEntity(
    @SerializedName("email") val email: String,
    @SerializedName("first_name") val firstName: String,
    @SerializedName("last_name") val lastName: String,
    @SerializedName("password") val password: String,
)