package com.example.booknet.models.entity.chats

import com.example.booknet.models.entity.users.UserEntity
import com.google.gson.annotations.SerializedName

/**
 * Entity model for chat message
 *
 * @param id ID of the chat message
 * @param senderId ID of the sender
 * @param senderData Model of the sender
 * @param content Content of the message
 * @param createdAt Date string when message was created
 */
data class ChatMessageEntity(
    @SerializedName("id") val id: Long,
    @SerializedName("sender") val senderId: Long,
    @SerializedName("sender_data") val senderData: UserEntity,
    @SerializedName("content") val content: String,
    @SerializedName("created") val createdAt: String,
)
