package com.example.booknet.models.presentation.trades

import androidx.annotation.StringRes
import com.example.booknet.models.presentation.books.Book
import com.example.booknet.models.presentation.users.UserInfo

/**
 * Presentation model for Trade offer
 *
 * @param id ID of the trade offer
 * @param senderId ID of the sender
 * @param senderData Data of the sender
 * @param receiverId ID of the receiver
 * @param receiverData Data of the sender
 * @param suggestedBooksIds IDs of the suggested books
 * @param suggestedBooksData Data of the suggested books
 * @param requestedBooksIds IDs of the requested books
 * @param requestedBooksData Data of the requested books
 * @param message Message of the trade offer
 * @param status Status of the trade offer
 */
data class TradeOffer(
    val id: Long,
    val senderId: Long,
    val senderData: UserInfo,
    val receiverId: Long,
    val receiverData: UserInfo,
    val suggestedBooksIds: List<Long>,
    val suggestedBooksData: List<Book>,
    val requestedBooksIds: List<Long>,
    val requestedBooksData: List<Book>,
    val message: String,
    val status: TradeOfferStatus,
)

/**
 * Enum for trade offer statuses
 *
 * @param serverString String of the status that is used on server
 * @param readableStringId ID of the resource with readable representation of the status
 */
enum class TradeOfferStatus(
    val serverString: String,
    @StringRes val readableStringId: Int?,
) {
    /**
     * Trade offer is completed
     */
    COMPLETED("completed", 0),

    /**
     * Trade offer is active
     */
    ACTIVE("active", 0),

    /**
     * Trade offer is rejected by one of the users
     */
    REJECTED("rejected", 0),

    /**
     * Trade offer is cancelled by sender
     */
    CANCELED("canceled", 0),

    /**
     * Unknown status
     */
    UNKNOWN("", null);

    companion object {
        /**
         * Convert [serverString] to [TradeOfferStatus]
         */
        fun fromServerString(serverString: String) = when (serverString) {
            "completed" -> COMPLETED
            "active" -> ACTIVE
            "rejected" -> REJECTED
            "canceled" -> CANCELED
            else -> UNKNOWN
        }
    }
}