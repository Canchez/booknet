package com.example.booknet.models.entity.discussions

import com.example.booknet.models.entity.users.UserEntity
import com.google.gson.annotations.SerializedName

/**
 * Discussion entity
 *
 * @param id ID of the discussion
 * @param title Title of the discussion
 * @param description Description of the discussion
 * @param comments List of comments in discussion
 * @param author Author of the discussion
 */
data class DiscussionEntity(
    @SerializedName("id") val id: Long,
    @SerializedName("title") val title: String,
    @SerializedName("content") val description: String,
    @SerializedName("comments") val comments: List<CommentEntity>,
    @SerializedName("author") val author: UserEntity,
)