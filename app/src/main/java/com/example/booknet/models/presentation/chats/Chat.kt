package com.example.booknet.models.presentation.chats

import com.example.booknet.models.presentation.users.UserInfo

/**
 * Presentation model for chat
 *
 * @param id ID of the chat
 * @param name Name of the chat
 * @param image URL for avatar image
 * @param attendees List of attendees of the chat
 * @param messages List of messages of the chat
 */
data class Chat(
    val id: Long,
    val name: String,
    val image: String?,
    val attendees: List<UserInfo>,
    val messages: List<ChatMessage>,
)