package com.example.booknet.models.entity.chats

import com.google.gson.annotations.SerializedName

/**
 * Entity model for chat incoming message for WebSockets
 *
 * @param message Text of the message
 * @param senderId ID of the sender
 */
data class ChatMessageWSEntity(
    @SerializedName("message") val message: String,
    @SerializedName("sender_id") val senderId: Long,
)
