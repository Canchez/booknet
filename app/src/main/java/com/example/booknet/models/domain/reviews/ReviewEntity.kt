package com.example.booknet.models.domain.reviews

import com.example.booknet.models.entity.users.UserEntity
import com.google.gson.annotations.SerializedName

/**
 * API Review entity
 *
 * @param mark Mark of the review
 * @param feedback Feedback text
 * @param user User Info
 */
data class ReviewEntity(
    @SerializedName("mark") val mark: Int,
    @SerializedName("feedback") val feedback: String,
    @SerializedName("user") val user: UserEntity,
)
