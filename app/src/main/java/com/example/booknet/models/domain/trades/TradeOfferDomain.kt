package com.example.booknet.models.domain.trades

import com.example.booknet.models.domain.books.BookDomain
import com.example.booknet.models.domain.users.UserInfoDomain

/**
 * Domain model for Trade offer
 *
 * @param id ID of the trade offer
 * @param senderId ID of the sender
 * @param senderData Data of the sender
 * @param receiverId ID of the receiver
 * @param receiverData Data of the sender
 * @param suggestedBooksIds IDs of the suggested books
 * @param suggestedBooksData Data of the suggested books
 * @param requestedBooksIds IDs of the requested books
 * @param requestedBooksData Data of the requested books
 * @param message Message of the trade offer
 * @param status Status of the trade offer
 */
data class TradeOfferDomain(
    val id: Long,
    val senderId: Long,
    val senderData: UserInfoDomain,
    val receiverId: Long,
    val receiverData: UserInfoDomain,
    val suggestedBooksIds: List<Long>,
    val suggestedBooksData: List<BookDomain>,
    val requestedBooksIds: List<Long>,
    val requestedBooksData: List<BookDomain>,
    val message: String,
    val status: String,
)
