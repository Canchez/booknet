package com.example.booknet.models.domain.reviews

import com.google.gson.annotations.SerializedName

/**
 * API Review model for POST requests
 *
 * @param mark Mark of the review
 * @param feedback Feedback text
 */
data class ReviewPostEntity(
    @SerializedName("mark") val mark: Int,
    @SerializedName("feedback") val feedback: String,
)
