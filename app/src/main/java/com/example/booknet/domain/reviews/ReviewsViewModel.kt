package com.example.booknet.domain.reviews

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.booknet.data.books.BooksRepository
import com.example.booknet.mappers.reviews.toPresentation
import com.example.booknet.models.presentation.reviews.Review
import com.example.booknet.utils.Resource
import com.example.booknet.utils.transform
import com.example.booknet.utils.transformList
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * ViewModel for reviews
 */
@HiltViewModel
class ReviewsViewModel @Inject constructor(
    private val booksRepository: BooksRepository,
) : ViewModel() {

    val reviewsState: LiveData<Resource<List<Review>>>
        get() = _reviewsState
    private val _reviewsState = MutableLiveData<Resource<List<Review>>>()

    val newReviewState: LiveData<Resource<Review>>
        get() = _newReviewState
    private val _newReviewState = MutableLiveData<Resource<Review>>()

    /**
     * Get reviews for book with [bookId]
     */
    fun getReviews(bookId: Long) = viewModelScope.launch(Dispatchers.IO) {
        _reviewsState.postValue(Resource.loading())
        val reviews = booksRepository.getReviewsForBook(bookId)
            .transformList { it.toPresentation() }
        _reviewsState.postValue(reviews)
    }

    /**
     * Add review with [mark] and [feedback] for book with [bookId]
     */
    fun addReview(bookId: Long, mark: Int, feedback: String) = viewModelScope.launch(Dispatchers.IO) {
        _newReviewState.postValue(Resource.loading())
        val review = booksRepository.addReviewToBook(bookId, mark, feedback)
            .transform { it?.toPresentation() }
        _newReviewState.postValue(review)
    }

    /**
     * Clear state for new review
     */
    fun clearNewReviewState() {
        _newReviewState.postValue(Resource.empty())
    }
}