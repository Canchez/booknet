package com.example.booknet.domain.auth

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.booknet.data.auth.AuthRepository
import com.example.booknet.data.auth.AuthStorage
import com.example.booknet.data.users.UsersRepository
import com.example.booknet.mappers.users.toPresentation
import com.example.booknet.models.entity.auth.RegisterRequestEntity
import com.example.booknet.models.entity.auth.RegisterResponseEntity
import com.example.booknet.models.entity.auth.TokenRequestEntity
import com.example.booknet.models.presentation.users.UserInfo
import com.example.booknet.utils.Resource
import com.example.booknet.utils.Status
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import retrofit2.HttpException
import timber.log.Timber
import javax.inject.Inject

/**
 * A ViewModel for auth
 */
@HiltViewModel
class AuthViewModel @Inject constructor(
    private val authStorage: AuthStorage,
    private val usersRepository: UsersRepository,
    private val authRepository: AuthRepository,
) : ViewModel() {

    val userInfo: LiveData<UserInfo>
        get() = _userInfo
    private val _userInfo = MutableLiveData<UserInfo>()

    val accessToken: String?
        get() = authStorage.accessToken

    val refreshToken: String?
        get() = authStorage.refreshToken

    val authState: LiveData<AuthState>
        get() = _authState
    private val _authState = MutableLiveData<AuthState>()

    val registrationState: LiveData<Resource<RegisterResponseEntity>>
        get() = _registrationState
    private val _registrationState = MutableLiveData<Resource<RegisterResponseEntity>>()

    init {
        if (authStorage.isUserLoggedIn == true) {
            _authState.postValue(AuthState.AuthSuccess)
        } else {
            _authState.postValue(AuthState.NoAuth)
        }

        _userInfo.postValue(authStorage.userInfo.toPresentation())
    }

    /**
     * Register new user with [email], [password], [firstName] and [lastName]
     */
    fun register(email: String, password: String, firstName: String, lastName: String) {
        viewModelScope.launch {
            _registrationState.postValue(Resource.loading())
            val response = authRepository.register(
                RegisterRequestEntity(
                    email = email,
                    firstName = firstName,
                    lastName = lastName,
                    password = password,
                )
            )
            _registrationState.postValue(response)
        }
    }

    fun clearRegisterState() = _registrationState.postValue(Resource.empty())

    /**
     * Login (get token pair) using [email] and [password]
     */
    fun login(email: String, password: String) {
        viewModelScope.launch {
            _authState.postValue(AuthState.AuthLoading)

            val response = authRepository.getAccessToken(
                TokenRequestEntity(email = email, password = password)
            )

            if (response.status == Status.SUCCESS && response.data != null) {
                authStorage.login(response.data)
                authStorage.isUserLoggedIn = true
                _authState.postValue(AuthState.AuthSuccess)
            } else {
                Timber.w("Error with login: $response")
                authStorage.isUserLoggedIn = false
                _authState.postValue(AuthState.AuthError(response.message ?: ""))
            }
        }
    }

    /**
     * Refresh access token using refresh token
     */
    fun refreshToken() {
        viewModelScope.launch {
            _authState.postValue(AuthState.AuthLoading)

            try {
                val response = authRepository.refreshAccessToken(authStorage.refreshTokenData)
                authStorage.refresh(response)

                authStorage.isUserLoggedIn = true
                _authState.postValue(AuthState.AuthSuccess)
            } catch (e: HttpException) {
                Timber.w("Error with refresh http: ${e.stackTraceToString()}")
                authStorage.clearUserInfo()
                authStorage.isUserLoggedIn = false
                _authState.postValue(AuthState.NoAuth)
            } catch (e: Exception) {
                Timber.w("Error with refresh: ${e.stackTraceToString()}")
                authStorage.isUserLoggedIn = false
                authStorage.clearUserInfo()
                _authState.postValue(AuthState.NoAuth)
            }
        }
    }

    /**
     * Clear login data and
     */
    fun logout() {
        viewModelScope.launch {
            authStorage.clearUserInfo()
            authStorage.isUserLoggedIn = false
            _authState.postValue(AuthState.NoAuth)
        }
    }

    /**
     * Get info about current user
     */
    fun getUserInfo() {
        viewModelScope.launch {
            val response = usersRepository.getUserInfo()

            if (response.status == Status.SUCCESS && response.data != null) {
                authStorage.setUserInfo(response.data)
            } else {
                Timber.w("Error with user info: $response")
                authStorage.clearUserInfo()
            }
            _userInfo.postValue(authStorage.userInfo.toPresentation())
        }
    }
}