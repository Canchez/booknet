package com.example.booknet.domain.books

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.booknet.api.DataListWrapper
import com.example.booknet.data.auth.AuthStorage
import com.example.booknet.data.books.BooksRepository
import com.example.booknet.mappers.books.toDashboardPresentation
import com.example.booknet.mappers.books.toPresentation
import com.example.booknet.mappers.users.toPresentation
import com.example.booknet.models.presentation.books.Book
import com.example.booknet.models.presentation.books.DashboardBook
import com.example.booknet.models.presentation.users.UserInfo
import com.example.booknet.utils.*
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

/**
 * ViewModel for books related actions
 */
@HiltViewModel
class BooksViewModel @Inject constructor(
    private val booksRepository: BooksRepository,
    private val authStorage: AuthStorage,
) : ViewModel() {

    val currentBookState: LiveData<Resource<Book>>
        get() = _currentBookState
    private val _currentBookState = MutableLiveData<Resource<Book>>()

    val popularBooksState: LiveData<Resource<List<DashboardBook>>>
        get() = _popularBooksState
    private val _popularBooksState = MutableLiveData<Resource<List<DashboardBook>>>()

    val allBooksState: LiveData<Resource<DataListWrapper<Book>>>
        get() = _allBooksState
    private val _allBooksState = MutableLiveData<Resource<DataListWrapper<Book>>>()

    val nextPageState: LiveData<Resource<Unit>>
        get() = _nextPageState
    private val _nextPageState = MutableLiveData<Resource<Unit>>(Resource.empty())

    val peopleState: LiveData<Resource<List<UserInfo>>>
        get() = _peopleState
    private val _peopleState = MutableLiveData<Resource<List<UserInfo>>>()

    private var totalPages: Int = 1
    private var currentPage: Int = 0
    private var currentSearchQuery: String? = null

    /**
     * Get book with [id]
     */
    fun getBook(id: Long) = viewModelScope.launch(Dispatchers.IO) {
        _currentBookState.postValue(Resource.loading())
        val currentBook = booksRepository.getBook(id)
            .transform { it?.toPresentation() }
        _currentBookState.postValue(currentBook)
    }

    /**
     * Clear state of current book
     */
    fun clearCurrentBookState() = _currentBookState.postValue(Resource.empty())

    /**
     * Get popular books
     */
    fun getPopularBooks() = viewModelScope.launch(Dispatchers.IO) {
        _popularBooksState.postValue(Resource.loading())
        val popularBooks = booksRepository.getMostViewedBooks()
            .transformList { it.toDashboardPresentation() }
        _popularBooksState.postValue(popularBooks)
    }

    /**
     * Search books by [searchQuery]
     */
    fun searchBooks(searchQuery: String = "") = viewModelScope.launch(Dispatchers.IO) {
        _allBooksState.postValue(Resource.loading())
        val allBooks = booksRepository.searchBooks(searchQuery, page = 1)
            .transformWithWrapper { it.toPresentation() }
        currentSearchQuery = searchQuery
        totalPages = allBooks.data?.totalPages ?: 1
        currentPage = 1
        _allBooksState.postValue(allBooks)
    }

    /**
     * Get next page for books search
     */
    @SuppressLint("NullSafeMutableLiveData")
    fun getNextSearchPage() = viewModelScope.launch(Dispatchers.IO) {
        if (currentPage >= totalPages || currentSearchQuery == null) return@launch
        _nextPageState.postValue(Resource.loading())
        currentPage++
        Timber.d("Getting next page: $currentPage")
        val nextPageBooks = booksRepository.searchBooks(currentSearchQuery!!, currentPage)
            .transformWithWrapper { it.toPresentation() }
        if (nextPageBooks.status == Status.SUCCESS) {
            val oldBooks = _allBooksState.value!!
            val combinedBooks = oldBooks.combineWithWrapper(nextPageBooks)
            if (combinedBooks != null) {
                _allBooksState.postValue(combinedBooks)
                _nextPageState.postValue(Resource.success(Unit))
            } else {
                Timber.w("Error with combining paginated results")
                _nextPageState.postValue(Resource.error("Error with combining paginated results"))
                currentPage--
            }
        } else {
            Timber.w("Error with getting next page")
            _nextPageState.postValue(Resource.error("Error with getting next page"))
            currentPage--
        }
    }

    /**
     * Clear state of books search
     */
    fun clearSearchBooksState() {
        _allBooksState.postValue(Resource.empty())
        _nextPageState.postValue(Resource.empty())
        currentPage = 1
        totalPages = 1
        currentSearchQuery = null
    }

    /**
     * Get people who wants to read book with [bookId]
     */
    fun getPeopleWantRead(bookId: Long) = viewModelScope.launch(Dispatchers.IO) {
        _peopleState.postValue(Resource.loading())
        val users = booksRepository.getPeopleWantToRead(bookId)
            .transformList { it.toPresentation() }
        _peopleState.postValue(users)
    }

    /**
     * Get people who wants to trade book with [bookId]
     */
    fun getPeopleWantTrade(bookId: Long) = viewModelScope.launch(Dispatchers.IO) {
        _peopleState.postValue(Resource.loading())
        val users = booksRepository.getPeopleWantToTrade(bookId)
            .transformList { it.toPresentation() }
        _peopleState.postValue(users)
    }

    /**
     * Get ID of current user
     */
    fun getCurrentUserId() = authStorage.userId ?: 0L
}