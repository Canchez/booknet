package com.example.booknet.domain.discussions

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.booknet.api.DataListWrapper
import com.example.booknet.data.auth.AuthStorage
import com.example.booknet.data.discussions.DiscussionsRepository
import com.example.booknet.mappers.discussions.toDashboardPresentation
import com.example.booknet.mappers.discussions.toPresentation
import com.example.booknet.models.presentation.discussions.Comment
import com.example.booknet.models.presentation.discussions.DashboardDiscussion
import com.example.booknet.models.presentation.discussions.Discussion
import com.example.booknet.utils.*
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

/**
 * ViewModel for discussions
 */
@HiltViewModel
class DiscussionsViewModel @Inject constructor(
    private val authStorage: AuthStorage,
    private val discussionsRepository: DiscussionsRepository,
) : ViewModel() {

    val currentDiscussionState: LiveData<Resource<Discussion>>
        get() = _currentDiscussionState
    private val _currentDiscussionState = MutableLiveData<Resource<Discussion>>()

    val currentComments: LiveData<Resource<DataListWrapper<Comment>>>
        get() = _currentComments
    private val _currentComments = MutableLiveData<Resource<DataListWrapper<Comment>>>()

    val popularDiscussionsState: LiveData<Resource<List<DashboardDiscussion>>>
        get() = _popularDiscussionsState
    private val _popularDiscussionsState = MutableLiveData<Resource<List<DashboardDiscussion>>>()

    val createDiscussionState: LiveData<Resource<Discussion>>
        get() = _createDiscussionState
    private val _createDiscussionState = MutableLiveData<Resource<Discussion>>()

    val usersDiscussionsState: LiveData<Resource<List<Discussion>>>
        get() = _usersDiscussionsState
    private val _usersDiscussionsState = MutableLiveData<Resource<List<Discussion>>>()

    val commentedByUserDiscussionsState: LiveData<Resource<List<Discussion>>>
        get() = _commentedByUserDiscussionsState
    private val _commentedByUserDiscussionsState = MutableLiveData<Resource<List<Discussion>>>()

    /**
     * Get popular discussions
     */
    fun getPopularDiscussions() = viewModelScope.launch(Dispatchers.IO) {
        _popularDiscussionsState.postValue(Resource.loading())
        val popularDiscussions = discussionsRepository.getPopularDiscussions()
            .transformList { it.toDashboardPresentation() }
        _popularDiscussionsState.postValue(popularDiscussions)
    }

    /**
     * Get discussion with [discussionId]
     */
    fun getDiscussion(discussionId: Long) = viewModelScope.launch(Dispatchers.IO) {
        _currentDiscussionState.postValue(Resource.loading())
        val currentDiscussion = discussionsRepository.getDiscussion(discussionId)
            .transform { it?.toPresentation() }
        _currentDiscussionState.postValue(currentDiscussion)
    }

    private var totalCommentsPages: Int = 1
    private var currentCommentsPage: Int = 0

    val commentsNextPageState: LiveData<Resource<Unit>>
        get() = _commentsNextPageState
    private val _commentsNextPageState = MutableLiveData<Resource<Unit>>()

    /**
     * Get comments for discussion with [discussionId]
     */
    fun getComments(discussionId: Long) = viewModelScope.launch(Dispatchers.IO) {
        _currentComments.postValue(Resource.loading())
        val currentCommentsResponse = discussionsRepository.getComments(
            discussionId = discussionId,
            page = 1,
        )
            .transformWithWrapper { it.toPresentation() }
        totalCommentsPages = currentCommentsResponse.data?.totalPages ?: 1
        currentCommentsPage = 1
        _currentComments.postValue(currentCommentsResponse)
    }

    /**
     * Get next page of comments
     */
    fun getNextCommentsPage(discussionId: Long) = viewModelScope.launch(Dispatchers.IO) {
        if (currentCommentsPage >= totalCommentsPages) return@launch
        _commentsNextPageState.postValue(Resource.loading())
        currentCommentsPage++
        Timber.d("Getting next comments page: $currentCommentsPage")
        val nextPageComments = discussionsRepository.getComments(
            discussionId = discussionId,
            page = currentCommentsPage,
        )
            .transformWithWrapper { it.toPresentation() }
        if (nextPageComments.status == Status.SUCCESS) {
            val oldComments = _currentComments.value!!
            val combinedComments = oldComments.combineWithWrapper(nextPageComments)
            if (combinedComments != null) {
                _currentComments.postValue(combinedComments!!)
                _commentsNextPageState.postValue(Resource.success(Unit))
            } else {
                Timber.d("Error with combining paginated comments")
                _commentsNextPageState.postValue(Resource.error("Error with combining paginated comments"))
                currentCommentsPage--
            }
        } else {
            Timber.d("Error with getting next page of comments")
            _commentsNextPageState.postValue(Resource.error("Error with getting next page of comments"))
            currentCommentsPage--
        }
    }

    /**
     * Get comments from discussion with [discussionId] again
     */
    private fun updateComments(discussionId: Long) = viewModelScope.launch(Dispatchers.IO) {
        getComments(discussionId)
    }

    val commentAddState: LiveData<Resource<Comment>>
        get() = _commentAddState
    private val _commentAddState = MutableLiveData<Resource<Comment>>()

    /**
     * Add comment with [message] to discussion with [discussionId]
     */
    fun addComment(discussionId: Long, message: String) = viewModelScope.launch(Dispatchers.IO) {
        val newComment = discussionsRepository.addComment(discussionId, message)
            .transform { it?.toPresentation() }
        if (newComment.status == Status.SUCCESS && newComment.data != null) {
            updateComments(discussionId)
        }
        _commentAddState.postValue(newComment)
    }

    /**
     * Get ID of currently logged in user
     */
    fun getCurrentUserId() = authStorage.userId

    /**
     * Create discussion with [title] and [content]
     */
    fun createDiscussion(title: String, content: String) = viewModelScope.launch(Dispatchers.IO) {
        _createDiscussionState.postValue(Resource.loading())
        val result = discussionsRepository.createDiscussion(title = title, content = content)
            .transform { it?.toPresentation() }
        _createDiscussionState.postValue(result)
    }

    /**
     * Clear state of create discussion
     */
    fun clearCreateDiscussionState() = _createDiscussionState.postValue(Resource.empty())

    /**
     * Get discussions created by current user
     */
    fun getUsersDiscussions() = viewModelScope.launch(Dispatchers.IO) {
        _usersDiscussionsState.postValue(Resource.loading())
        val discussions = discussionsRepository.getUsersDiscussions()
            .transformList { it.toPresentation() }
        _usersDiscussionsState.postValue(discussions)
    }

    /**
     * Get discussions commented by current user
     */
    fun getCommentedByUserDiscussions() = viewModelScope.launch(Dispatchers.IO) {
        _commentedByUserDiscussionsState.postValue(Resource.loading())
        val discussions = discussionsRepository.getCommentedByUsersDiscussions()
            .transformList { it.toPresentation() }
        _commentedByUserDiscussionsState.postValue(discussions)
    }

    /**
     * Delete comment with [commentId] from discussion with [discussionId]
     */
    fun deleteComment(discussionId: Long, commentId: Long) = viewModelScope.launch(Dispatchers.IO) {
        discussionsRepository.deleteComment(discussionId = discussionId, commentId = commentId)
        updateComments(discussionId)
    }

    /**
     * True if user is admin of discussion, false - otherwise
     */
    fun isUserAdmin(discussionId: Long): Boolean {
        val discussion = _currentDiscussionState.value?.data
        return discussion != null && discussion.id == discussionId && discussion.author.id == getCurrentUserId()
    }
}