package com.example.booknet.domain.profiles

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.booknet.api.DataListWrapper
import com.example.booknet.data.ProfilesRepository
import com.example.booknet.data.auth.AuthStorage
import com.example.booknet.mappers.books.toPresentation
import com.example.booknet.mappers.users.toPresentation
import com.example.booknet.models.presentation.books.Book
import com.example.booknet.models.presentation.users.UserInfo
import com.example.booknet.utils.*
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

/**
 * ViewModel for profile related actions
 */
@HiltViewModel
class ProfilesViewModel @Inject constructor(
    private val profilesRepository: ProfilesRepository,
    private val authStorage: AuthStorage,
) : ViewModel() {

    val profileBooksState: LiveData<Resource<DataListWrapper<Book>>>
        get() = _profileBooksState
    private val _profileBooksState = MutableLiveData<Resource<DataListWrapper<Book>>>()

    val nextLibraryPageState: LiveData<Resource<Unit>>
        get() = _nextLibraryPageState
    private val _nextLibraryPageState = MutableLiveData<Resource<Unit>>(Resource.empty())

    private var totalLibraryPages: Int = 1
    private var currentLibraryPage: Int = 0

    /**
     * Get books for profile with [profileId]
     */
    fun getProfileBooks(profileId: Long) = viewModelScope.launch(Dispatchers.IO) {
        _profileBooksState.postValue(Resource.loading())
        val profileBooks = profilesRepository.getProfileBooks(profileId, page = 1)
            .transformWithWrapper { it.toPresentation() }
        // TODO Implement pagination
        totalLibraryPages = profileBooks.data?.totalPages ?: 1
        currentLibraryPage = 1
        _profileBooksState.postValue(profileBooks)
    }

    /**
     * Get next page for library for profile with [profileId]
     */
    fun getNextLibraryPage(profileId: Long) = viewModelScope.launch(Dispatchers.IO) {
        if (currentLibraryPage >= totalLibraryPages) return@launch
        _nextLibraryPageState.postValue(Resource.loading())
        currentLibraryPage++
        Timber.d("Getting next library page: $currentLibraryPage")
        val nextPageLibrary = profilesRepository.getProfileBooks(profileId, page = currentLibraryPage)
            .transformWithWrapper { it.toPresentation() }
        if (nextPageLibrary.status == Status.SUCCESS) {
            val oldLibrary = _profileBooksState.value!!
            val combinedLibrary = oldLibrary.combineWithWrapper(nextPageLibrary)
            if (combinedLibrary != null) {
                _profileBooksState.postValue(combinedLibrary!!)
                _nextLibraryPageState.postValue(Resource.success(Unit))
            } else {
                Timber.w("Error with combining paginated results")
                _nextLibraryPageState.postValue(Resource.error("Error with combining paginated results"))
                currentLibraryPage--
            }
        } else {
            Timber.w("Error with getting next page")
            _nextLibraryPageState.postValue(Resource.error("Error with getting next page"))
            currentLibraryPage--
        }
    }

    /**
     * Clear state of profileBooksState
     */
    fun clearProfileBooksState() = _profileBooksState.postValue(Resource.empty())

    val profileState: LiveData<Resource<UserInfo>>
        get() = _profileState
    private val _profileState = MutableLiveData<Resource<UserInfo>>()

    /**
     * Get profile with [profileId]
     */
    fun getProfile(profileId: Long) = viewModelScope.launch(Dispatchers.IO) {
        _profileState.postValue(Resource.loading())
        val profile = profilesRepository.getProfile(profileId)
            .transform { it?.toPresentation() }
        _profileState.postValue(profile)
    }

    /**
     * Get ID of current user
     */
    fun getCurrentUserId() = authStorage.userId ?: 1L
}