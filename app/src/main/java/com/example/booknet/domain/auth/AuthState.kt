package com.example.booknet.domain.auth

/**
 * States of auth
 */
sealed class AuthState {

    /**
     * No auth state
     */
    object NoAuth : AuthState()

    /**
     * Auth is loading state
     */
    object AuthLoading : AuthState()

    /**
     * Auth is successful state
     */
    object AuthSuccess : AuthState()

    /**
     * Auth error state
     */
    data class AuthError(val message: String) : AuthState()


}