package com.example.booknet.domain.chats

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.booknet.api.DataListWrapper
import com.example.booknet.data.ProfilesRepository
import com.example.booknet.data.auth.AuthStorage
import com.example.booknet.data.chats.ChatsRepository
import com.example.booknet.mappers.chats.toPresentation
import com.example.booknet.mappers.users.toPresentation
import com.example.booknet.models.entity.chats.ChatMessageWSEntity
import com.example.booknet.models.entity.chats.ChatPostEntity
import com.example.booknet.models.presentation.chats.Chat
import com.example.booknet.models.presentation.chats.ChatMessage
import com.example.booknet.models.presentation.users.UserInfo
import com.example.booknet.utils.Resource
import com.example.booknet.utils.filterWrapper
import com.example.booknet.utils.transform
import com.example.booknet.utils.transformWithWrapper
import com.google.gson.Gson
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.joda.time.DateTime
import timber.log.Timber
import javax.inject.Inject

/**
 * ViewModel for chats
 */
@HiltViewModel
class ChatsViewModel @Inject constructor(
    private val chatsRepository: ChatsRepository,
    private val profilesRepository: ProfilesRepository,
    private val authStorage: AuthStorage,
) : ViewModel() {

    /**
     * State for list of chats
     */
    val chatsState: LiveData<Resource<DataListWrapper<Chat>>>
        get() = _chatsState
    private val _chatsState = MutableLiveData<Resource<DataListWrapper<Chat>>>()

    /**
     * State for current chat
     */
    val currentChatState: LiveData<Resource<Chat>>
        get() = _currentChatState
    private val _currentChatState = MutableLiveData<Resource<Chat>>()

    /**
     * List of messages of current chat
     */
    val currentMessagesList: LiveData<List<ChatMessage>>
        get() = _currentMessagesList
    private val _currentMessagesList = MutableLiveData<List<ChatMessage>>()

    /**
     * Get chats of user
     */
    fun getChats() = viewModelScope.launch(Dispatchers.IO) {
        _chatsState.postValue(Resource.loading())
        val chats = chatsRepository.getChats(page = 1)
            .transformWithWrapper { it.toPresentation() }
        Timber.d("CHATS:ChatsViewModel:getChats, chats=$chats")
        _chatsState.postValue(chats)
    }

    /**
     * Get details of chat by [chatId]
     */
    fun getChatDetails(chatId: Long) = viewModelScope.launch(Dispatchers.IO) {
        _currentChatState.postValue(Resource.loading())
        val chat = chatsRepository.getChatById(chatId)
            .transform { it?.toPresentation() }
        Timber.d("CHATS:ChatsViewModel:getChatById, chat=$chat")
        _currentChatState.postValue(chat)
        chat.data?.messages?.let {
            _currentMessagesList.postValue(it)
        }
    }

    /**
     * Get ID of current user
     */
    fun getUserId() = authStorage.userId ?: 0L

    /**
     * Handle received message from [messageJson] and add it to list
     */
    fun handleReceivedMessage(messageJson: String) {
        val messageData = Gson().fromJson(messageJson, ChatMessageWSEntity::class.java)
        val senderData = _currentChatState.value?.data?.attendees?.find { it.id == messageData.senderId }
        if (senderData == null) {
            Timber.d("Error getting user data: no user in current chat state")
            return
        }
        val currentTime = DateTime.now()
        val chatMessage = ChatMessage(
            id = _currentMessagesList.value?.size?.toLong()?.plus(1) ?: 1L,
            content = messageData.message,
            senderId = messageData.senderId,
            senderData = senderData,
            createdAt = currentTime,
        )
        val currentMessages = _currentMessagesList.value ?: emptyList()
        _currentMessagesList.postValue(currentMessages.plus(chatMessage))
    }

    val searchPeople: LiveData<Resource<DataListWrapper<UserInfo>>>
        get() = _searchPeople
    private val _searchPeople = MutableLiveData<Resource<DataListWrapper<UserInfo>>>()

    val selectedPeople: LiveData<List<UserInfo>>
        get() = _selectedPeople
    private val _selectedPeople = MutableLiveData<List<UserInfo>>()

    /**
     * Search people for chats using [searchQuery]
     *
     * TODO 24.04.2022: Implement pagination
     */
    fun searchChatParticipants(searchQuery: String) = viewModelScope.launch(Dispatchers.IO) {
        _searchPeople.postValue(Resource.loading())
        val people = profilesRepository.searchProfiles(searchQuery)
            .filterWrapper { it.id != getUserId() }
            .transformWithWrapper { userInfoDomain ->
                val userInfo = userInfoDomain.toPresentation()
                userInfo.isSelected = _selectedPeople.value?.map { it.id }?.contains(userInfo.id) == true
                userInfo
            }
        _searchPeople.postValue(people)
    }

    fun clearSearch() = _searchPeople.postValue(Resource.empty())

    fun clearSelected() = _selectedPeople.postValue(emptyList())

    /**
     * Add or remove [participant] from [_selectedPeople]
     */
    fun addOrRemoveChatParticipant(participant: UserInfo) {
        val alreadySelected = _selectedPeople.value ?: emptyList()
        val newSelected = if (alreadySelected.contains(participant)) {
            alreadySelected.minus(participant)
        } else {
            alreadySelected.plus(participant)
        }
        _selectedPeople.postValue(newSelected)

        val foundUsers = _searchPeople.value
        val newFound = foundUsers
            ?.transformWithWrapper { userInfo ->
                userInfo.isSelected = newSelected.map { it.id }.contains(userInfo.id)
                userInfo
            }
        newFound?.let { _searchPeople.postValue(it) }
    }

    val createChatState: LiveData<Resource<Chat>>
        get() = _createChatState
    private val _createChatState = MutableLiveData<Resource<Chat>>()

    /**
     * Create chat with [name] and attendees from [_selectedPeople]
     */
    fun createChat(name: String) = viewModelScope.launch(Dispatchers.IO) {
        _createChatState.postValue(Resource.loading())
        val attendees = _selectedPeople.value?.map {
            it.id ?: 1L
        } ?: emptyList()
        val allAttendees = attendees
            .plus(getUserId())

        val chatEntity = ChatPostEntity(
            chatName = name,
            attendees = allAttendees,
        )

        val result = chatsRepository.createChat(chatEntity)
            .transform { it?.toPresentation() }

        _createChatState.postValue(result)
    }

    fun clearChatCreate() = _createChatState.postValue(Resource.empty())
}