package com.example.booknet.domain.offers

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.booknet.api.DataListWrapper
import com.example.booknet.data.auth.AuthStorage
import com.example.booknet.data.trades.TradesRepository
import com.example.booknet.mappers.trades.toPresentation
import com.example.booknet.models.entity.trades.TradeOfferPostEntity
import com.example.booknet.models.presentation.trades.TradeOffer
import com.example.booknet.models.presentation.trades.TradeOfferStatus
import com.example.booknet.utils.*
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

/**
 * ViewModel for trade offer related actions
 */
@HiltViewModel
class TradesViewModel @Inject constructor(
    private val tradesRepository: TradesRepository,
    private val authStorage: AuthStorage,
) : ViewModel() {

    val tradeOffersState: LiveData<Resource<DataListWrapper<TradeOffer>>>
        get() = _tradeOffersState
    private val _tradeOffersState = MutableLiveData<Resource<DataListWrapper<TradeOffer>>>()

    val nextPagesState: LiveData<Resource<Unit>>
        get() = _nextPageState
    private val _nextPageState = MutableLiveData<Resource<Unit>>()

    val sendTradeOfferState: LiveData<Resource<TradeOffer>>
        get() = _sendTradeOfferState
    private val _sendTradeOfferState = MutableLiveData<Resource<TradeOffer>>()

    val currentTradeOfferState: LiveData<Resource<TradeOffer>>
        get() = _currentTradeOfferState
    private val _currentTradeOfferState = MutableLiveData<Resource<TradeOffer>>()

    val deleteTradeOfferState: LiveData<Resource<Unit>>
        get() = _deleteTradeOfferState
    private val _deleteTradeOfferState = MutableLiveData<Resource<Unit>>()

    val editTradeOfferState: LiveData<Resource<TradeOffer>>
        get() = _editTradeOfferState
    private val _editTradeOfferState = MutableLiveData<Resource<TradeOffer>>()

    private var totalPages: Int = 1
    private var currentPage: Int = 0

    /**
     * Get trade offers of user
     */
    fun getTradeOffers() = viewModelScope.launch(Dispatchers.IO) {
        _tradeOffersState.postValue(Resource.loading())
        val tradeOffers = tradesRepository.getTradeOffers(page = 1)
            .transformWithWrapper { it.toPresentation() }
        totalPages = tradeOffers.data?.totalPages ?: 1
        currentPage = 1
        _tradeOffersState.postValue(tradeOffers)
    }

    /**
     * Get next page of trade offers
     */
    @SuppressLint("NullSafeMutableLiveData")
    fun getNextTradeOffersPage() = viewModelScope.launch(Dispatchers.IO) {
        if (currentPage >= totalPages) return@launch
        _nextPageState.postValue(Resource.loading())
        currentPage++
        Timber.d("Getting next page of trade offers: $currentPage")
        val nextPageTradeOffers = tradesRepository.getTradeOffers(page = currentPage)
            .transformWithWrapper { it.toPresentation() }
        if (nextPageTradeOffers.status == Status.SUCCESS) {
            val oldTradeOffers = _tradeOffersState.value!!
            val combinedTradeOffers = oldTradeOffers.combineWithWrapper(nextPageTradeOffers)
            if (combinedTradeOffers != null) {
                _tradeOffersState.postValue(combinedTradeOffers)
                _nextPageState.postValue(Resource.success(Unit))
            } else {
                Timber.w("Error with combining paginated results of trade offers")
                _nextPageState.postValue(Resource.error("Error with combining paginated results of trade offers"))
                currentPage--
            }
        } else {
            Timber.w("Error with getting next page of trade offers")
            _nextPageState.postValue(Resource.error("Error with getting next page of trade offers"))
            currentPage--
        }
    }

    /**
     * Clear state of trade offers
     */
    fun clearTradeOffersState() {
        _tradeOffersState.postValue(Resource.empty())
        _nextPageState.postValue(Resource.empty())
        currentPage = 1
        totalPages = 1
    }

    /**
     * Send trade offer with [bookId] to user with [receiverId] with [message]
     *
     * TODO 02.04.2022: Add book picker and use list of book ids
     */
    fun sendTradeOffer(bookId: Long, receiverId: Long, message: String) = viewModelScope.launch(Dispatchers.IO) {
        _sendTradeOfferState.postValue(Resource.loading())
        val tradeResult = tradesRepository.addTradeOffer(
            TradeOfferPostEntity(
                senderId = getCurrentUserId(),
                receiverId = receiverId,
                suggestedBooksIds = listOf(bookId),
                requestedBooksIds = emptyList(),
                message = message,
                status = TradeOfferStatus.ACTIVE.serverString,
            )
        )
            .transform { it?.toPresentation() }
        _sendTradeOfferState.postValue(tradeResult)
    }

    /**
     * Send trade request for [bookId] to user with [receiverId] with [message]
     *
     * TODO 02.04.2022: Add book picker and use list of book ids
     */
    fun sendTradeRequest(bookId: Long, receiverId: Long, message: String) = viewModelScope.launch(Dispatchers.IO) {
        _sendTradeOfferState.postValue(Resource.loading())
        val tradeResult = tradesRepository.addTradeOffer(
            TradeOfferPostEntity(
                senderId = getCurrentUserId(),
                receiverId = receiverId,
                suggestedBooksIds = emptyList(),
                requestedBooksIds = listOf(bookId),
                message = message,
                status = TradeOfferStatus.ACTIVE.serverString,
            )
        )
            .transform { it?.toPresentation() }
        _sendTradeOfferState.postValue(tradeResult)
    }

    /**
     * Get ID of the current user
     */
    fun getCurrentUserId(): Long = authStorage.userId ?: 0L

    /**
     * Get trade offer with [tradeOfferId]
     */
    fun getTradeOfferById(tradeOfferId: Long) = viewModelScope.launch(Dispatchers.IO) {
        _currentTradeOfferState.postValue(Resource.loading())
        val tradeOffer = tradesRepository.getTradeOfferById(tradeOfferId)
            .transform { it?.toPresentation() }
        _currentTradeOfferState.postValue(tradeOffer)
    }


    /**
     * Delete trade offer with [tradeOfferId]
     */
    fun deleteTradeOfferById(tradeOfferId: Long) = viewModelScope.launch(Dispatchers.IO) {
        _deleteTradeOfferState.postValue(Resource.loading())
        val result = tradesRepository.deleteTradeById(tradeOfferId)
        _deleteTradeOfferState.postValue(result)
    }

    /**
     * Clear state of trade offer delete
     */
    fun clearDeleteTradeOfferState() = _deleteTradeOfferState.postValue(Resource.empty())

    /**
     * Edit trade offer with [tradeOfferId]
     *
     * @param senderId ID of the sender
     * @param receiverId ID of the receiver
     * @param newRequestedBooksIds List of new requested books
     * @param newSuggestedBooksIds List of new suggested books
     * @param newMessage New message
     * @param status Status of the trade offer
     */
    fun editTradeOfferById(
        tradeOfferId: Long,
        senderId: Long,
        receiverId: Long,
        newSuggestedBooksIds: List<Long>,
        newRequestedBooksIds: List<Long>,
        newMessage: String,
        status: TradeOfferStatus,
    ) = viewModelScope.launch(Dispatchers.IO) {
        _editTradeOfferState.postValue(Resource.loading())
        val editedTradeOffer = tradesRepository.updateTradeById(
            tradeOfferId,
            TradeOfferPostEntity(
                senderId = senderId,
                receiverId = receiverId,
                suggestedBooksIds = newSuggestedBooksIds,
                requestedBooksIds = newRequestedBooksIds,
                message = newMessage,
                status = status.serverString,
            )
        )
            .transform { it?.toPresentation() }
        _editTradeOfferState.postValue(editedTradeOffer)
        if (editedTradeOffer.status == Status.SUCCESS && editedTradeOffer.data != null) {
            _currentTradeOfferState.postValue(editedTradeOffer)
        }
    }

    /**
     * Clear state of trade offer edit
     */
    fun clearEditTradeOfferState() = _editTradeOfferState.postValue(Resource.empty())
}