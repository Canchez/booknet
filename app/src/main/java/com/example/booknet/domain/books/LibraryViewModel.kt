package com.example.booknet.domain.books

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.booknet.api.DataListWrapper
import com.example.booknet.data.books.BooksRepository
import com.example.booknet.data.books.LibraryRepository
import com.example.booknet.mappers.books.toPresentation
import com.example.booknet.models.domain.books.BookPostDomain
import com.example.booknet.models.presentation.books.Book
import com.example.booknet.models.presentation.books.BookStatus
import com.example.booknet.utils.*
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class LibraryViewModel @Inject constructor(
    private val libraryRepository: LibraryRepository,
    private val booksRepository: BooksRepository,
) : ViewModel() {

    val libraryState: LiveData<Resource<DataListWrapper<Book>>>
        get() = _libraryState
    private val _libraryState = MutableLiveData<Resource<DataListWrapper<Book>>>()

    val addBookState: LiveData<Resource<Book>>
        get() = _addBookState
    private val _addBookState = MutableLiveData<Resource<Book>>()

    val createBookState: LiveData<Resource<Book>>
        get() = _createBookState
    private val _createBookState = MutableLiveData<Resource<Book>>()

    val nextPageState: LiveData<Resource<Unit>>
        get() = _nextPageState
    private val _nextPageState = MutableLiveData<Resource<Unit>>(Resource.empty())

    val changeStatusState: LiveData<Resource<Unit>>
        get() = _changeStatusState
    private val _changeStatusState = MutableLiveData<Resource<Unit>>()

    private var totalPages: Int = 1
    private var currentPage: Int = 0


    fun getLibrary() = viewModelScope.launch {
        _libraryState.postValue(Resource.loading())
        val library = libraryRepository.getLibrary(page = 1)
            .transformWithWrapper { it.toPresentation() }
        totalPages = library.data?.totalPages ?: 1
        currentPage = 1
        _libraryState.postValue(library)
    }

    fun getNextLibraryPage() = viewModelScope.launch(Dispatchers.IO) {
        if (currentPage >= totalPages) return@launch
        _nextPageState.postValue(Resource.loading())
        currentPage++
        Timber.d("Getting next page: $currentPage")
        val nextPageLibrary = libraryRepository.getLibrary(currentPage)
            .transformWithWrapper { it.toPresentation() }
        if (nextPageLibrary.status == Status.SUCCESS) {
            val oldLibrary = _libraryState.value!!
            val combinedLibrary = oldLibrary.combineWithWrapper(nextPageLibrary)
            if (combinedLibrary != null) {
                _libraryState.postValue(combinedLibrary!!)
                _nextPageState.postValue(Resource.success(Unit))
            } else {
                Timber.w("Error with combining paginated results")
                _nextPageState.postValue(Resource.error("Error with combining paginated results"))
                currentPage--
            }
        } else {
            Timber.w("Error with getting next page")
            _nextPageState.postValue(Resource.error("Error with getting next page"))
            currentPage--
        }
    }

    /**
     * Add book with [bookId] to library with [status]
     */
    fun addBookToLibrary(bookId: Long, status: BookStatus) = viewModelScope.launch(Dispatchers.IO) {
        _addBookState.postValue(Resource.loading())
        val result = libraryRepository.addBookToLibrary(
            bookId = bookId,
            status = status.serverString,
        )
            .transform { it?.toPresentation() }
        _addBookState.postValue(result)
    }

    /**
     * Clear state of adding book
     */
    fun clearAddBookState() = _addBookState.postValue(Resource.empty())

    /**
     * Change status of the book with [bookId] to [status]
     */
    fun changeBookStatus(bookId: Long, status: BookStatus) = viewModelScope.launch(Dispatchers.IO) {
        _changeStatusState.postValue(Resource.loading())
        val result = booksRepository.changeBookStatus(
            bookId = bookId,
            status = status.serverString,
        )
        _changeStatusState.postValue(result)
    }

    /**
     * Clear state of changing status
     */
    fun clearChangeBookStatusState() = _changeStatusState.postValue(Resource.empty())

    /**
     * Create book in library with [title], [pages] amount, [description]
     */
    fun createBook(
        title: String,
        pages: Long,
        description: String,
        status: BookStatus,
    ) = viewModelScope.launch(Dispatchers.IO) {
        _createBookState.postValue(Resource.loading())
        val bookDomain = BookPostDomain(
            title = title,
            pages = pages,
            description = description,
            status = status,
        )
        val result = libraryRepository.createBook(bookDomain)
            .transform { it?.toPresentation() }
        _createBookState.postValue(result)
    }

    /**
     * Clear state of creating book
     */
    fun clearCreateBookState() {
        _createBookState.postValue(Resource.empty())
    }

    /**
     * Clear state of library
     */
    fun clearLibraryState() {
        _libraryState.postValue(Resource.empty())
    }
}