package com.example.booknet.data.chats

import com.example.booknet.api.DataListWrapper
import com.example.booknet.api.ResponseHandler
import com.example.booknet.api.chats.ChatsService
import com.example.booknet.mappers.chats.toDomain
import com.example.booknet.models.domain.chats.ChatDomain
import com.example.booknet.models.entity.chats.ChatPostEntity
import com.example.booknet.utils.Resource
import com.example.booknet.utils.transform
import com.example.booknet.utils.transformWithWrapper
import javax.inject.Inject

/**
 * Repository for chat related requests
 */
class ChatsRepository @Inject constructor(
    private val chatsService: ChatsService,
) {

    /**
     * Get chats of user
     *
     * Paginated
     */
    suspend fun getChats(page: Int): Resource<DataListWrapper<ChatDomain>> =
        ResponseHandler.executeRequest(chatsService.getChats(page))
            .transformWithWrapper { it.toDomain() }

    /**
     * Get chat by [chatId]
     */
    suspend fun getChatById(chatId: Long): Resource<ChatDomain> =
        ResponseHandler.executeRequest(chatsService.getChatById(chatId))
            .transform { it?.toDomain() }

    /**
     * Create chat with [chatEntity]
     */
    suspend fun createChat(chatEntity: ChatPostEntity): Resource<ChatDomain> =
        ResponseHandler.executeRequest(chatsService.createChat(chatEntity))
            .transform { it?.toDomain() }
}