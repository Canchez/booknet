package com.example.booknet.data.trades

import com.example.booknet.api.ResponseHandler
import com.example.booknet.api.trades.TradesService
import com.example.booknet.mappers.trades.toDomain
import com.example.booknet.models.entity.trades.TradeOfferPostEntity
import com.example.booknet.utils.transform
import com.example.booknet.utils.transformWithWrapper
import javax.inject.Inject

/**
 * Repository for trade offer requests
 */
class TradesRepository @Inject constructor(
    private val tradesService: TradesService,
) {

    /**
     * Get trade offers
     *
     * Paginated
     */
    suspend fun getTradeOffers(page: Int) =
        ResponseHandler.executeRequest(tradesService.getTrades(page))
            .transformWithWrapper { it.toDomain() }

    /**
     * Add new [tradeOffer]
     */
    suspend fun addTradeOffer(tradeOffer: TradeOfferPostEntity) =
        ResponseHandler.executeRequest(tradesService.addTradeOffer(tradeOffer))
            .transform { it?.toDomain() }

    /**
     * Get trade offer by [id]
     */
    suspend fun getTradeOfferById(id: Long) =
        ResponseHandler.executeRequest(tradesService.getTradeById(id))
            .transform { it?.toDomain() }

    /**
     * Update trade offer with [id] with info from [newTradeOffer]
     */
    suspend fun updateTradeById(id: Long, newTradeOffer: TradeOfferPostEntity) =
        ResponseHandler.executeRequest(tradesService.updateTradeById(id, newTradeOffer))
            .transform { it?.toDomain() }

    /**
     * Delete trade offer by [id]
     */
    suspend fun deleteTradeById(id: Long) =
        ResponseHandler.executeRequest(tradesService.deleteTradeById(id), expectData = false)
}