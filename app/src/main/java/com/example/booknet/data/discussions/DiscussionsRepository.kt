package com.example.booknet.data.discussions

import com.example.booknet.api.ResponseHandler
import com.example.booknet.api.discussions.DiscussionsService
import com.example.booknet.mappers.discussions.toDomain
import com.example.booknet.models.entity.discussions.CommentPostEntity
import com.example.booknet.models.entity.discussions.DiscussionPostEntity
import com.example.booknet.utils.transform
import com.example.booknet.utils.transformList
import com.example.booknet.utils.transformWithWrapper
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Repository for discussions
 */
@Singleton
class DiscussionsRepository @Inject constructor(
    private val discussionsService: DiscussionsService,
) {

    /**
     * Get all discussions
     */
    suspend fun getDiscussions() =
        ResponseHandler.executeRequest(discussionsService.getDiscussions())
            .transformList { it.toDomain() }

    suspend fun getPopularDiscussions() =
        ResponseHandler.executeRequest(discussionsService.getPopularDiscussions())
            .transformList { it.toDomain() }

    suspend fun getUsersDiscussions() =
        ResponseHandler.executeRequest(discussionsService.getUsersDiscussions())
            .transformList { it.toDomain() }

    suspend fun getCommentedByUsersDiscussions() =
        ResponseHandler.executeRequest(discussionsService.getCommentedByUsersDiscussions())
            .transformList { it.toDomain() }

    /**
     * Get discussion by [discussionId]
     */
    suspend fun getDiscussion(discussionId: Long) =
        ResponseHandler.executeRequest(discussionsService.getDiscussionById(discussionId))
            .transform { it?.toDomain() }

    /**
     * Get comments for discussion with [discussionId]
     *
     * Paginated
     */
    suspend fun getComments(discussionId: Long, page: Int) =
        ResponseHandler.executeRequest(discussionsService.getComments(discussionId, page))
            .transformWithWrapper { it.toDomain() }

    /**
     * Add comment with [commentMessage] to discussion with [discussionId]
     */
    suspend fun addComment(discussionId: Long, commentMessage: String) =
        ResponseHandler.executeRequest(discussionsService.addComment(discussionId, CommentPostEntity(commentMessage, discussionId)))
            .transform { it?.toDomain() }

    /**
     * Delete comment with [commentId] from discussion with [discussionId]
     */
    suspend fun deleteComment(discussionId: Long, commentId: Long) =
        ResponseHandler.executeRequest(discussionsService.deleteComment(discussionId = discussionId, commentId = commentId))

    /**
     * Create discussion with [title] and [content]
     */
    suspend fun createDiscussion(title: String, content: String) =
        ResponseHandler.executeRequest(discussionsService.addDiscussion(DiscussionPostEntity(title = title, description = content)))
            .transform { it?.toDomain() }
}