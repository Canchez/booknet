package com.example.booknet.data.auth

import com.example.booknet.api.ResponseHandler
import com.example.booknet.api.auth.AuthService
import com.example.booknet.models.entity.auth.RefreshTokenRequestEntity
import com.example.booknet.models.entity.auth.RegisterRequestEntity
import com.example.booknet.models.entity.auth.TokenRequestEntity
import javax.inject.Inject

/**
 * Repository for auth requests
 */
class AuthRepository @Inject constructor(
    private val authService: AuthService,
) {

    /**
     * Refresh access token with [refreshTokenInfo]
     */
    suspend fun refreshAccessToken(refreshTokenInfo: RefreshTokenRequestEntity) = authService.refreshAccessToken(refreshTokenInfo)

    /**
     * Get access token with [tokenInfo]
     */
    suspend fun getAccessToken(tokenInfo: TokenRequestEntity) =
        ResponseHandler.executeRequest(authService.getToken(tokenInfo))

    /**
     * Register user with [registerInfo]
     */
    suspend fun register(registerInfo: RegisterRequestEntity) =
        ResponseHandler.executeRequest(authService.register(registerInfo))
}