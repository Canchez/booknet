package com.example.booknet.data.users

import com.example.booknet.api.ResponseHandler
import com.example.booknet.api.users.UserService
import com.example.booknet.mappers.users.toDomain
import com.example.booknet.utils.transform
import javax.inject.Inject

class UsersRepository @Inject constructor(
    private val userService: UserService,
) {

    suspend fun getUserInfo() =
        ResponseHandler.executeRequest(userService.getUserInfo())
            .transform { it?.toDomain() }
}