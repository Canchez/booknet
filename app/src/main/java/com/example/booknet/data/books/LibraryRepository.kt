package com.example.booknet.data.books

import com.example.booknet.api.ResponseHandler
import com.example.booknet.api.books.LibraryService
import com.example.booknet.mappers.books.toDomain
import com.example.booknet.mappers.books.toEntity
import com.example.booknet.models.domain.books.BookPostDomain
import com.example.booknet.models.entity.books.LibraryBookPostEntity
import com.example.booknet.utils.transform
import com.example.booknet.utils.transformWithWrapper
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LibraryRepository @Inject constructor(
    private val libraryService: LibraryService,
) {

    /**
     * Get library for current user from [page]
     *
     * Paginated
     */
    suspend fun getLibrary(page: Int) =
        ResponseHandler.executeRequest(libraryService.getLibrary(page))
            .transformWithWrapper { it.toDomain() }

    /**
     * Add book with [bookId] in library with [status]
     */
    suspend fun addBookToLibrary(bookId: Long, status: String) =
        ResponseHandler.executeRequest(libraryService.addBookToLibrary(LibraryBookPostEntity(bookId, status)))
            .transform { it?.toDomain() }

    /**
     * Create [book] in library
     */
    suspend fun createBook(book: BookPostDomain) =
        ResponseHandler.executeRequest(libraryService.createBook(book.toEntity()))
            .transform { it?.toDomain() }
}