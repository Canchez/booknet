package com.example.booknet.data.auth

import android.content.Context
import androidx.core.content.edit
import com.example.booknet.R
import com.example.booknet.models.domain.users.UserInfoDomain
import com.example.booknet.models.entity.auth.RefreshTokenRequestEntity
import com.example.booknet.models.entity.auth.RefreshTokenResponseEntity
import com.example.booknet.models.entity.auth.TokenResponseEntity
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Class for storing auth related info
 */
@Singleton
class AuthStorage @Inject constructor(
    @ApplicationContext context: Context,
) {
    private val preferences = context.getSharedPreferences(context.getString(R.string.preferences_file_name), Context.MODE_PRIVATE)

    /**
     * Access token - used for API requests.
     */
    var accessToken: String?
        get() = preferences.getString(ACCESS_TOKEN, null)
        set(value) {
            preferences.edit {
                remove(ACCESS_TOKEN)
                if (value != null) putString(ACCESS_TOKEN, value)
            }
        }

    /**
     * Refresh token - used to refresh Access token.
     */
    var refreshToken: String?
        get() = preferences.getString(REFRESH_TOKEN, null)
        set(value) {
            preferences.edit {
                remove(REFRESH_TOKEN)
                if (value != null) putString(REFRESH_TOKEN, value)
            }
        }

    /**
     * ID of current user.
     */
    var userId: Long?
        get() = preferences.getLong(USER_ID, 0)
        set(value) {
            preferences.edit {
                remove(USER_ID)
                if (value != null) putLong(USER_ID, value)
            }
        }

    /**
     * First name of the current user.
     */
    private var firstName: String?
        get() = preferences.getString(FIRST_NAME, null)
        set(value) {
            preferences.edit {
                remove(FIRST_NAME)
                if (value != null) putString(FIRST_NAME, value)
            }
        }

    /**
     * Last name of the current user.
     */
    private var lastName: String?
        get() = preferences.getString(LAST_NAME, null)
        set(value) {
            preferences.edit {
                remove(LAST_NAME)
                if (value != null) putString(LAST_NAME, value)
            }
        }

    /**
     * Email of the current user.
     */
    private var email: String?
        get() = preferences.getString(EMAIL, null)
        set(value) {
            preferences.edit {
                remove(EMAIL)
                if (value != null) putString(EMAIL, value)
            }
        }

    /**
     * Avatar of current user.
     */
    private var avatar: String?
        get() = preferences.getString(AVATAR, null)
        set(value) {
            preferences.edit {
                remove(AVATAR)
                if (value != null) putString(AVATAR, value)
            }
        }

    /**
     * Get authorization token for API requests.
     */
    val authorizationHeader: String
        get() = "Bearer $accessToken"

    /**
     * User info
     */
    val userInfo: UserInfoDomain
        get() = UserInfoDomain(userId, firstName, lastName, email, avatar)

    /**
     * Data used to refresh Access Token through the API.
     */
    val refreshTokenData: RefreshTokenRequestEntity
        get() = RefreshTokenRequestEntity(refreshToken = this.refreshToken ?: "")

    /**
     * Refresh tokens from API.
     */
    fun refresh(tokenData: RefreshTokenResponseEntity) {
        accessToken = tokenData.accessToken
        refreshToken = tokenData.refreshToken
    }

    /**
     * Login (get tokens)
     */
    fun login(loginResponseData: TokenResponseEntity) {
        accessToken = loginResponseData.accessToken
        refreshToken = loginResponseData.refreshToken
    }

    /**
     * Set info for current user.
     */
    fun setUserInfo(userData: UserInfoDomain) {
        userId = userData.id
        email = userData.email
        firstName = userData.firstName
        lastName = userData.lastName
        avatar = userData.avatar
    }

    /**
     * Clear user info.
     */
    fun clearUserInfo() {
        accessToken = null
        refreshToken = null

        userId = null
        firstName = null
        lastName = null
        email = null
        avatar = null
    }

    /**
     * Returns true if user is logged in, false - otherwise
     */
    var isUserLoggedIn: Boolean?
        get() = preferences.getBoolean(IS_LOGGED_IN, false)
        set(value) {
            preferences.edit {
                remove(IS_LOGGED_IN)
                if (value != null) putBoolean(IS_LOGGED_IN, value)
            }
        }


    companion object {
        private const val IS_LOGGED_IN = "IS_LOGGED_IN"
        private const val ACCESS_TOKEN = "ACCESS_TOKEN"
        private const val REFRESH_TOKEN = "REFRESH_TOKEN"
        private const val USER_ID = "USER_ID"
        private const val FIRST_NAME = "FIRST_NAME"
        private const val LAST_NAME = "LAST_NAME"
        private const val EMAIL = "EMAIL"
        private const val AVATAR = "AVATAR"
    }
}