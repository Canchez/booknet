package com.example.booknet.data.books

import com.example.booknet.api.ResponseHandler
import com.example.booknet.api.books.BooksService
import com.example.booknet.mappers.books.toDomain
import com.example.booknet.mappers.reviews.toDomain
import com.example.booknet.mappers.users.toDomain
import com.example.booknet.models.domain.books.BookDomain
import com.example.booknet.models.domain.reviews.ReviewPostEntity
import com.example.booknet.models.entity.books.BookStatusEntity
import com.example.booknet.models.entity.reviews.ReviewDomain
import com.example.booknet.utils.Resource
import com.example.booknet.utils.transform
import com.example.booknet.utils.transformList
import com.example.booknet.utils.transformWithWrapper
import javax.inject.Inject

/**
 * Repository for books
 */
class BooksRepository @Inject constructor(
    private val booksService: BooksService,
) {

    /**
     * Search books by [searchQuery]
     */
    suspend fun searchBooks(searchQuery: String, page: Int) =
        ResponseHandler.executeRequest(booksService.searchBooks(searchQuery, page))
            .transformWithWrapper { it.toDomain() }

    /**
     * Get most viewed books
     */
    suspend fun getMostViewedBooks(): Resource<List<BookDomain>> =
        ResponseHandler.executeRequest(booksService.getMostViewedBooks())
            .transformList { it.toDomain() }

    /**
     * Get book by [id]
     */
    suspend fun getBook(id: Long): Resource<BookDomain> =
        ResponseHandler.executeRequest(booksService.getBookById(id))
            .transform { it?.toDomain() }

    /**
     * Get reviews for book with [bookId]
     */
    suspend fun getReviewsForBook(bookId: Long): Resource<List<ReviewDomain>> =
        ResponseHandler.executeRequest(booksService.getReviewsByBookId(bookId))
            .transformList { it.toDomain() }

    /**
     * Add review with [mark] and [feedback] for book with [bookId]
     */
    suspend fun addReviewToBook(bookId: Long, mark: Int, feedback: String): Resource<ReviewDomain> =
        ResponseHandler.executeRequest(booksService.addReviewToBookById(bookId, ReviewPostEntity(mark, feedback)))
            .transform { it?.toDomain() }

    /**
     * Get people who wants to read book with [bookId]
     */
    suspend fun getPeopleWantToRead(bookId: Long) =
        ResponseHandler.executeRequest(booksService.getPeopleWantToRead(bookId))
            .transformList { it.toDomain() }

    /**
     * Get people who wants to trade book with [bookId]
     */
    suspend fun getPeopleWantToTrade(bookId: Long) =
        ResponseHandler.executeRequest(booksService.getPeopleWantToTrade(bookId))
            .transformList { it.toDomain() }

    /**
     * Change status of the book with [bookId] to [status]
     */
    suspend fun changeBookStatus(bookId: Long, status: String) =
        ResponseHandler.executeRequest(booksService.changeBookStatus(bookId, BookStatusEntity(status)), expectData = false)

}