package com.example.booknet.data

import com.example.booknet.api.DataListWrapper
import com.example.booknet.api.ResponseHandler
import com.example.booknet.api.profiles.ProfilesService
import com.example.booknet.mappers.books.toDomain
import com.example.booknet.mappers.users.toDomain
import com.example.booknet.models.domain.books.BookDomain
import com.example.booknet.models.domain.users.UserInfoDomain
import com.example.booknet.utils.Resource
import com.example.booknet.utils.transform
import com.example.booknet.utils.transformWithWrapper
import javax.inject.Inject

/**
 * Repository for profile related requests
 */
class ProfilesRepository @Inject constructor(
    private val profilesService: ProfilesService,
) {

    /**
     * Get books of the user with [profileId]
     *
     * Paginated
     */
    suspend fun getProfileBooks(profileId: Long, page: Int): Resource<DataListWrapper<BookDomain>> =
        ResponseHandler.executeRequest(profilesService.getProfileBooks(profileId, page))
            .transformWithWrapper { it.toDomain() }

    /**
     * Search profiles using [searchQuery]
     *
     * Paginated
     */
    suspend fun searchProfiles(searchQuery: String): Resource<DataListWrapper<UserInfoDomain>> =
        ResponseHandler.executeRequest(profilesService.searchProfiles(searchQuery))
            .transformWithWrapper { it.toDomain() }

    /**
     * Get profile with [profileId]
     */
    suspend fun getProfile(profileId: Long): Resource<UserInfoDomain> =
        ResponseHandler.executeRequest(profilesService.getProfile(profileId))
            .transform { it?.toDomain() }
}