package com.example.booknet.ui.reviews.adapters

import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.booknet.R
import com.example.booknet.databinding.ItemReviewBinding
import com.example.booknet.di.GlideApp
import com.example.booknet.models.presentation.reviews.Review
import com.example.booknet.models.presentation.users.UserInfo
import com.example.booknet.utils.SPACE_DELIMITER
import com.example.booknet.utils.ViewUtils
import com.example.booknet.utils.getFirstAndLast
import com.example.booknet.utils.joinNotEmpty

/**
 * Adapter for reviews
 */
class ReviewsAdapter(
    private val onUserAvatarClicked: (UserInfo) -> Unit,
) : ListAdapter<Review, ReviewsAdapter.ViewHolder>(ReviewDiffCallback) {

    class ViewHolder(
        private val viewBinding: ItemReviewBinding,
        private val onUserAvatarClicked: (Int) -> Unit,
    ) : RecyclerView.ViewHolder(viewBinding.root) {

        fun bind(review: Review) {
            viewBinding.apply {
                val context = root.context
                tvUserName.text = listOf(review.user.firstName, review.user.lastName).joinNotEmpty(SPACE_DELIMITER)
                tvReviewText.text = review.feedback

                val stars = listOf(
                    layoutStars.ivStar1,
                    layoutStars.ivStar2,
                    layoutStars.ivStar3,
                    layoutStars.ivStar4,
                    layoutStars.ivStar5,
                )

                stars.subList(0, review.mark).forEach {
                    it.imageTintList = ColorStateList.valueOf(context.getColor(R.color.white))
                }

                val avatarPlaceholder = ViewUtils.createRoundNamePlaceholder(
                    values = listOf(review.user.firstName ?: "", review.user.lastName ?: "").getFirstAndLast(),
                    textColor = ResourcesCompat.getColor(context.resources, R.color.primary, context.theme),
                    backColor = ResourcesCompat.getColor(context.resources, R.color.white, context.theme),
                )

                GlideApp.with(context)
                    .load(review.user.avatar)
                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                    .placeholder(avatarPlaceholder)
                    .error(avatarPlaceholder)
                    .into(ivUserAvatar)

                ivUserAvatar.setOnClickListener { onUserAvatarClicked(absoluteAdapterPosition) }
            }
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding = ItemReviewBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(
            viewBinding = itemBinding,
            onUserAvatarClicked = { position ->
                getItem(position)?.let { review ->
                    onUserAvatarClicked(review.user)
                }
            }
        )
    }
}

/**
 * DiffCallback for [Review]
 */
object ReviewDiffCallback : DiffUtil.ItemCallback<Review>() {
    override fun areItemsTheSame(oldItem: Review, newItem: Review): Boolean {
        return oldItem.user.id == newItem.user.id
    }

    override fun areContentsTheSame(oldItem: Review, newItem: Review): Boolean {
        return oldItem == newItem
    }
}