package com.example.booknet.ui.reviews

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.booknet.R
import com.example.booknet.databinding.FragmentReviewsBinding
import com.example.booknet.domain.reviews.ReviewsViewModel
import com.example.booknet.ui.books.BookDetailsFragment.Companion.BOOK_ID
import com.example.booknet.ui.profiles.UserProfileFragment
import com.example.booknet.ui.reviews.adapters.ReviewsAdapter
import com.example.booknet.utils.Status
import com.example.booknet.utils.showToastMessage
import dagger.hilt.android.AndroidEntryPoint

/**
 * Fragment for review list
 */
@AndroidEntryPoint
class ReviewsFragment : Fragment(R.layout.fragment_reviews) {

    private val viewBinding by viewBinding(FragmentReviewsBinding::bind)

    private val reviewsViewModel: ReviewsViewModel by activityViewModels()

    private val navController by lazy { findNavController() }

    private lateinit var reviewsAdapter: ReviewsAdapter

    private var bookId: Long = 0L

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bookId = arguments?.getLong(BOOK_ID) ?: 0L

        setupToolbar()
        setupListeners()
        setupReviewsList()
        setupObservers()

        reviewsViewModel.getReviews(bookId)
    }

    /**
     * Setup toolbar listeners
     */
    private fun setupToolbar() {
        viewBinding.tbReviews.setNavigationOnClickListener { navController.navigateUp() }
    }

    /**
     * Setup listeners on UI
     */
    private fun setupListeners() {
        viewBinding.btnAddReview.setOnClickListener {
            val arguments = bundleOf(
                BOOK_ID to bookId,
            )
            navController.navigate(R.id.action_reviewsFragment_to_addReviewFragment, arguments)
        }
    }

    /**
     * Setup observers for ViewModel
     */
    private fun setupObservers() {
        reviewsViewModel.reviewsState.observe(viewLifecycleOwner) {
            viewBinding.pbReviewsLoading.isVisible = it.status == Status.LOADING
            when (it.status) {
                Status.SUCCESS -> {
                    reviewsAdapter.submitList(it.data)
                }
                Status.ERROR -> {
                    // TODO Implement
                    showToastMessage(it.message ?: "")
                }
                else -> {
                    // Ignore
                }
            }
        }
    }

    /**
     * Setup list of reviews
     */
    private fun setupReviewsList() {
        reviewsAdapter = ReviewsAdapter(
            onUserAvatarClicked = {
                val arguments = bundleOf(
                    UserProfileFragment.USER_ID to it.id,
                )
            },
        )
        viewBinding.rvReviews.apply {
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            adapter = reviewsAdapter
        }
    }
}