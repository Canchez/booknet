package com.example.booknet.ui.trades.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.booknet.databinding.ItemTradeOfferBookBinding
import com.example.booknet.models.presentation.books.Book

/**
 * Adapter for books in trade offers
 */
class TradeOfferBooksAdapter(
    private val showCheckboxes: Boolean,
    private val onCheckBoxClicked: ((book: Book, isChecked: Boolean) -> Unit)? = null,
) : ListAdapter<Book, TradeOfferBooksAdapter.ViewHolder>(TradeOfferBookDiffCallback) {

    class ViewHolder(
        private val viewBinding: ItemTradeOfferBookBinding,
        private val showCheckboxes: Boolean,
        private val onCheckBoxClicked: ((position: Int, isChecked: Boolean) -> Unit)? = null,
    ) : RecyclerView.ViewHolder(viewBinding.root) {

        fun bind(book: Book) {
            viewBinding.apply {
                tvBookName.text = book.title
                tvBookAuthor.text = book.author
                tvBookDescription.text = book.description

                cbChecked.apply {
                    isVisible = showCheckboxes
                    setOnCheckedChangeListener { _, isChecked ->
                        onCheckBoxClicked?.invoke(absoluteAdapterPosition, isChecked)
                    }
                    isChecked = book.isChecked == true
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding = ItemTradeOfferBookBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(
            viewBinding = itemBinding,
            showCheckboxes = showCheckboxes,
            onCheckBoxClicked = { position, isChecked ->
                getItem(position)?.let { book ->
                    onCheckBoxClicked?.invoke(book, isChecked)
                }
            },
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}

/**
 * Diff callback for [Book]
 */
object TradeOfferBookDiffCallback : DiffUtil.ItemCallback<Book>() {
    override fun areItemsTheSame(oldItem: Book, newItem: Book): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Book, newItem: Book): Boolean {
        return oldItem == newItem
    }
}