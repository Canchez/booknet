package com.example.booknet.ui.books

import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.ListPopupWindow
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.booknet.R
import com.example.booknet.databinding.FragmentAddBookBinding
import com.example.booknet.domain.books.LibraryViewModel
import com.example.booknet.models.presentation.books.BookStatus
import com.example.booknet.utils.Status
import com.example.booknet.utils.showToastMessage
import dagger.hilt.android.AndroidEntryPoint

/**
 * Fragment for creating new book
 */
@AndroidEntryPoint
class LibraryCreateFragment : Fragment(R.layout.fragment_add_book) {

    private val viewBinding by viewBinding(FragmentAddBookBinding::bind)

    private val libraryViewModel: LibraryViewModel by activityViewModels()

    private val navController by lazy { findNavController() }

    private var chosenStatus: BookStatus = BookStatus.NOT_INTERESTED

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupToolbar()
        setupListeners()
        setupObservers()
    }

    /**
     * Setup toolbar listeners
     */
    private fun setupToolbar() {
        viewBinding.tbAddBook.setNavigationOnClickListener { navController.navigateUp() }
    }

    /**
     * Setup UI listeners
     */
    private fun setupListeners() {
        viewBinding.apply {
            btnAddToLibrary.setOnClickListener {
                libraryViewModel.createBook(
                    title = etBookTitle.text.toString(),
                    pages = etPages.text.toString().toLong(),
                    description = etDescription.text.toString(),
                    status = chosenStatus,
                )
            }

            tvStatusValue.setOnClickListener {
                openStatusPopup()
            }
        }
    }

    /**
     * Setup ViewModel observers
     */
    private fun setupObservers() {
        libraryViewModel.createBookState.observe(viewLifecycleOwner) {
            when (it.status) {
                Status.SUCCESS -> {
                    showToastMessage(R.string.library_add_book_success)
                    libraryViewModel.clearCreateBookState()
                }
                Status.LOADING -> {
                    // TODO Implement
                }
                Status.ERROR -> {
                    showToastMessage(R.string.library_add_book_error)
                    libraryViewModel.clearCreateBookState()
                }
                else -> {
                    // Ignore
                }
            }
        }
    }

    /**
     * Open popup with statuses
     */
    private fun openStatusPopup() {
        val statusesStrings = BookStatus.values().mapNotNull { bookStatus ->
            bookStatus.readableStringId?.let {
                getString(it)
            }
        }
        val statuses = BookStatus.values().filter { it.readableStringId != null }
        val arrayAdapter = ArrayAdapter(requireContext(), R.layout.item_popup_option, statusesStrings)
        val dropdownPopup = ListPopupWindow(requireContext()).apply {
            anchorView = viewBinding.tvStatusValue
            isModal = true
            setOnItemClickListener { _, _, position, _ ->
                chosenStatus = statuses[position]
                viewBinding.tvStatusValue.text = chosenStatus.readableStringId?.let {
                    getString(it)
                }
                dismiss()
            }
            setAdapter(arrayAdapter)
        }
        dropdownPopup.show()
    }
}