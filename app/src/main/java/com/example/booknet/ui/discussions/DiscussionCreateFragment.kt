package com.example.booknet.ui.discussions

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.booknet.R
import com.example.booknet.databinding.FragmentDiscussionCreateBinding
import com.example.booknet.domain.discussions.DiscussionsViewModel
import com.example.booknet.utils.Status
import com.example.booknet.utils.showToastMessage

/**
 * Fragment for discussion create
 */
class DiscussionCreateFragment : Fragment(R.layout.fragment_discussion_create) {

    private val viewBinding by viewBinding(FragmentDiscussionCreateBinding::bind)

    private val discussionsViewModel: DiscussionsViewModel by activityViewModels()

    private val navController by lazy { findNavController() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupToolbar()
        setupListeners()
        setupObservers()
    }

    /**
     * Setup toolbar listeners
     */
    private fun setupToolbar() {
        viewBinding.tbDiscussionCreate.setNavigationOnClickListener { navController.popBackStack() }
    }

    /**
     * Setup UI listeners
     */
    private fun setupListeners() {
        viewBinding.apply {
            btnCreateDiscussion.setOnClickListener {
                discussionsViewModel.createDiscussion(
                    title = etDiscussionTitle.text.toString(),
                    content = etDiscussionContent.text.toString(),
                )
            }

            etDiscussionTitle.doOnTextChanged { _, _, _, _ ->
                checkButtonEnabled()
            }
            etDiscussionContent.doOnTextChanged { _, _, _, _ ->
                checkButtonEnabled()
            }
        }
    }

    /**
     * Check if should enable button
     */
    private fun checkButtonEnabled() {
        viewBinding.apply {
            val title = etDiscussionTitle.text.toString()
            val content = etDiscussionContent.text.toString()
            btnCreateDiscussion.isEnabled = title.isNotBlank() && content.isNotBlank()
        }
    }

    /**
     * Setup ViewModel observers
     */
    private fun setupObservers() {
        discussionsViewModel.createDiscussionState.observe(viewLifecycleOwner) {
            viewBinding.pbLoading.isVisible = it.status == Status.LOADING
            when (it.status) {
                Status.SUCCESS -> {
                    showToastMessage(R.string.discussion_create_success)
                    discussionsViewModel.clearCreateDiscussionState()
                    navController.popBackStack()
                }
                Status.ERROR -> {
                    showToastMessage(R.string.discussion_create_error)
                    discussionsViewModel.clearCreateDiscussionState()
                }
                else -> {
                    // Ignore
                }
            }
        }
    }
}