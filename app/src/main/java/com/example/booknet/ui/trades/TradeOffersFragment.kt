package com.example.booknet.ui.trades

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.booknet.R
import com.example.booknet.databinding.FragmentTradeOffersBinding
import com.example.booknet.domain.offers.TradesViewModel
import com.example.booknet.ui.trades.adapters.TradeOffersAdapter
import com.example.booknet.utils.Status
import com.example.booknet.utils.showToastMessage
import timber.log.Timber

/**
 * Fragment for list of trade offers
 */
class TradeOffersFragment : Fragment(R.layout.fragment_trade_offers) {

    private val viewBinding by viewBinding(FragmentTradeOffersBinding::bind)

    private val tradesViewModel: TradesViewModel by activityViewModels()

    private val navController by lazy { findNavController() }

    private lateinit var offersAdapter: TradeOffersAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupToolbar()
        setupListeners()
        setupObservers()
        setupList()
        tradesViewModel.getTradeOffers()
    }

    /**
     * Setup toolbar listeners
     */
    private fun setupToolbar() {
        viewBinding.tbOffers.setNavigationOnClickListener { navController.popBackStack() }
    }

    /**
     * Setup UI listeners
     */
    private fun setupListeners() {
        viewBinding.apply {
            rvOffers.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    if (!recyclerView.canScrollVertically(1) && recyclerView.scrollState == RecyclerView.SCROLL_STATE_IDLE
                        && tradesViewModel.nextPagesState.value?.status != Status.LOADING
                    ) {
                        tradesViewModel.getNextTradeOffersPage()
                    }
                    super.onScrollStateChanged(recyclerView, newState)
                }
            })
        }
    }

    /**
     * Setup ViewModel observers
     */
    private fun setupObservers() {
        tradesViewModel.tradeOffersState.observe(viewLifecycleOwner) {
            viewBinding.pbOffersLoading.isVisible = it.status == Status.LOADING
            when (it.status) {
                Status.ERROR -> {
                    showToastMessage(R.string.book_trade_offers_error)
                    Timber.e("Error with getting trade offers: $it")
                    tradesViewModel.clearTradeOffersState()
                }
                Status.SUCCESS -> {
                    offersAdapter.submitList(it.data?.results)
                    tradesViewModel.clearTradeOffersState()
                }
                else -> {
                    // Ignore
                }
            }
        }

        tradesViewModel.nextPagesState.observe(viewLifecycleOwner) {
            Timber.d("Next page state: $it")
        }
    }

    /**
     * Setup trade offers list
     */
    private fun setupList() {
        offersAdapter = TradeOffersAdapter(
            currentUserId = tradesViewModel.getCurrentUserId(),
            onItemClicked = {
                val arguments = bundleOf(
                    TradeOfferDetailsFragment.TRADE_OFFER_ID to it.id,
                )
                navController.navigate(R.id.action_tradeOffersFragment_to_tradeOfferDetailsFragment, arguments)
            },
        )
        viewBinding.rvOffers.apply {
            adapter = offersAdapter
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            addItemDecoration(DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL))
        }
    }
}