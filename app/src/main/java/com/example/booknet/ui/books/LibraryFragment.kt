package com.example.booknet.ui.books

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.booknet.R
import com.example.booknet.databinding.FragmentLibraryBinding
import com.example.booknet.domain.profiles.ProfilesViewModel
import com.example.booknet.ui.books.adapters.LibraryBookAdapter
import com.example.booknet.utils.Status
import com.example.booknet.utils.showToastMessage

class LibraryFragment : Fragment(R.layout.fragment_library) {

    private val viewBinding by viewBinding(FragmentLibraryBinding::bind)

    private val profilesViewModel: ProfilesViewModel by activityViewModels()

    private val navController by lazy { findNavController() }

    private lateinit var booksAdapter: LibraryBookAdapter

    private var userId: Long = 0L

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        userId = arguments?.getLong(USER_ID) ?: profilesViewModel.getCurrentUserId()
        setupToolbar()
        setupUI()
        setupListeners()
        setupObservers()
        setupBooksList()
        profilesViewModel.getProfileBooks(userId)
    }

    /**
     * Setup UI depending on [userId]
     */
    private fun setupUI() {
        viewBinding.apply {
            btnAddBook.isVisible = userId == profilesViewModel.getCurrentUserId()
            btnSearchBooks.isVisible = userId == profilesViewModel.getCurrentUserId()
        }
    }

    /**
     * Setup toolbar listeners
     */
    private fun setupToolbar() {
        viewBinding.tbLibrary.setNavigationOnClickListener { navController.navigateUp() }
    }

    /**
     * Setup UI listeners
     */
    private fun setupListeners() {
        viewBinding.apply {
            btnAddBook.setOnClickListener {
                navController.navigate(R.id.action_libraryFragment_to_libraryCreateFragment)
            }

            btnSearchBooks.setOnClickListener {
                navController.navigate(R.id.action_libraryFragment_to_booksSearchFragment)
            }

            rvBooks.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                // Load new page when scrolled to end
                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    if (!recyclerView.canScrollVertically(1) && recyclerView.scrollState == RecyclerView.SCROLL_STATE_IDLE
                        && profilesViewModel.nextLibraryPageState.value?.status != Status.LOADING
                    ) {
                        profilesViewModel.getNextLibraryPage(userId)
                    }
                    super.onScrollStateChanged(recyclerView, newState)
                }
            })
        }
    }

    /**
     * Setup ViewModel observers
     */
    private fun setupObservers() {
        profilesViewModel.profileBooksState.observe(viewLifecycleOwner) {
            viewBinding.pbLibraryLoading.isVisible = it.status == Status.LOADING
            when (it.status) {
                Status.ERROR -> {
                    // TODO Implement
                    showToastMessage(it.message ?: "")
                    profilesViewModel.clearProfileBooksState()
                }
                Status.SUCCESS -> {
                    booksAdapter.submitList(it.data?.results)
                    profilesViewModel.clearProfileBooksState()
                }
                else -> {
                    // Ignore
                }
            }
        }
    }

    /**
     * Setup list for books
     */
    private fun setupBooksList() {
        booksAdapter = LibraryBookAdapter {
            val arguments = bundleOf(
                BookDetailsFragment.BOOK_ID to it.id,
            )
            navController.navigate(R.id.action_libraryFragment_to_bookDetailsFragment, arguments)
        }

        viewBinding.rvBooks.apply {
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            adapter = booksAdapter
        }
    }

    companion object {
        const val USER_ID = "USER_ID"
    }
}