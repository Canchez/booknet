package com.example.booknet.ui.books

import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.booknet.R
import com.example.booknet.databinding.FragmentBooksSearchBinding
import com.example.booknet.domain.books.BooksViewModel
import com.example.booknet.ui.books.adapters.LibraryBookAdapter
import com.example.booknet.utils.Status
import timber.log.Timber

class BooksSearchFragment : Fragment(R.layout.fragment_books_search) {

    private val viewBinding by viewBinding(FragmentBooksSearchBinding::bind)

    private val booksViewModel: BooksViewModel by activityViewModels()

    private val navController by lazy { findNavController() }

    private lateinit var booksAdapter: LibraryBookAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupToolbar()
        setupBooksList()
        setupListeners()
        setupObservers()
        booksViewModel.searchBooks()
    }

    override fun onDestroy() {
        booksViewModel.clearSearchBooksState()
        super.onDestroy()
    }

    private fun setupToolbar() {
        viewBinding.tbBooks.setNavigationOnClickListener { navController.navigateUp() }
    }

    private fun setupListeners() {
        viewBinding.apply {
            ivSearch.setOnClickListener {
                booksViewModel.searchBooks(etSearch.text.toString())
            }

            etSearch.setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    booksViewModel.searchBooks(etSearch.text.toString())
                    return@setOnEditorActionListener true
                }
                return@setOnEditorActionListener false
            }

            rvBooks.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                // Load new page when scrolled to end
                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    if (!recyclerView.canScrollVertically(1) && recyclerView.scrollState == RecyclerView.SCROLL_STATE_IDLE
                        && booksViewModel.nextPageState.value?.status != Status.LOADING
                    ) {
                        booksViewModel.getNextSearchPage()
                    }
                    super.onScrollStateChanged(recyclerView, newState)
                }
            })
        }
    }

    private fun setupObservers() {
        booksViewModel.allBooksState.observe(viewLifecycleOwner) {
            viewBinding.apply {
                pbBooksLoading.isVisible = it.status == Status.LOADING
                tvBooksError.isVisible = it.status == Status.ERROR
            }
            when (it.status) {
                Status.ERROR -> {
                    viewBinding.tvBooksError.text = it.message
                    Timber.e("Error searching books: $it")
                }
                Status.SUCCESS -> {
                    booksAdapter.submitList(it.data?.results)
                }
                else -> {
                    // Ignore
                }
            }
        }
    }

    private fun setupBooksList() {
        booksAdapter = LibraryBookAdapter {
            val arguments = bundleOf(
                BookDetailsFragment.BOOK_ID to it.id,
            )
            navController.navigate(R.id.action_booksSearchFragment_to_bookDetailsFragment, arguments)
        }

        viewBinding.rvBooks.apply {
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            adapter = booksAdapter
        }
    }
}