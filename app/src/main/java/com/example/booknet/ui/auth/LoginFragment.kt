package com.example.booknet.ui.auth

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.booknet.R
import com.example.booknet.databinding.FragmentLoginBinding
import com.example.booknet.domain.auth.AuthState
import com.example.booknet.domain.auth.AuthViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * A Fragment for Login screen
 */
@AndroidEntryPoint
class LoginFragment : Fragment(R.layout.fragment_login) {

    private val viewBinding by viewBinding(FragmentLoginBinding::bind)

    private val authViewModel: AuthViewModel by activityViewModels()

    private val navController by lazy { findNavController() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupListeners()
        setupLoginFields()
        setupObservers()
        viewBinding.btnLogin.isEnabled = false
    }

    /**
     * Setup UI listeners
     */
    private fun setupListeners() {
        viewBinding.apply {
            btnLogin.setOnClickListener {
                val email = fields.teEmail.text?.toString()
                val password = fields.tePassword.text?.toString()

                if (email.isNullOrBlank() || password.isNullOrBlank()) return@setOnClickListener

                authViewModel.login(
                    email = email,
                    password = password
                )
            }

            btnRegister.setOnClickListener { navController.navigate(R.id.action_loginFragment_to_registerFragment) }

            fields.teEmail.addTextChangedListener {
                clearErrors()
                btnLogin.isEnabled = !areFieldsEmpty()
            }

            fields.tePassword.addTextChangedListener {
                clearErrors()
                btnLogin.isEnabled = !areFieldsEmpty()
            }
        }
    }

    /**
     * Show only login fields: Email and Password
     */
    private fun setupLoginFields() {
        viewBinding.fields.apply {
            tilEmail.isVisible = true
            tilPassword.isVisible = true
            tilFirstName.isVisible = false
            tilLastName.isVisible = false
            tilConfirmPassword.isVisible = false
        }
    }

    /**
     * Return true if at least one of the fields is empty
     */
    private fun areFieldsEmpty(): Boolean {
        return viewBinding.fields.run {
            teEmail.text.isNullOrBlank() || tePassword.text.isNullOrBlank()
        }
    }

    /***
     * Setup Auth observers
     */
    private fun setupObservers() {
        authViewModel.authState.observe(viewLifecycleOwner) {
            when (it) {
                AuthState.AuthLoading -> {
                    clearErrors()
                    setButtonsEnabled(false)
                }
                is AuthState.AuthError -> {
                    clearErrors()
                    viewBinding.fields.tilEmail.error = it.message
                    setButtonsEnabled(true)
                }
                AuthState.AuthSuccess -> navController.navigate(R.id.action_loginFragment_to_dashboardFragment)
                AuthState.NoAuth -> {
                    setButtonsEnabled(true)
                    // Ignore
                }
            }
        }
    }

    /**
     * Clear error messages
     */
    private fun clearErrors() {
        viewBinding.fields.apply {
            tilEmail.error = null
            tilPassword.error = null
        }
    }

    /**
     * Enable or disable buttons depending on [isEnabled]
     */
    private fun setButtonsEnabled(isEnabled: Boolean) {
        viewBinding.apply {
            btnLogin.isEnabled = isEnabled
            btnRegister.isEnabled = isEnabled
            btnResetPassword.isEnabled = isEnabled
        }
    }

}