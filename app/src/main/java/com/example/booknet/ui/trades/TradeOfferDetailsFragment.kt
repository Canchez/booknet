package com.example.booknet.ui.trades

import android.app.AlertDialog
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import androidx.annotation.MenuRes
import androidx.appcompat.view.menu.MenuBuilder
import androidx.core.os.bundleOf
import androidx.core.view.forEach
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.booknet.R
import com.example.booknet.databinding.FragmentTradeOfferDetailsBinding
import com.example.booknet.databinding.ItemPersonBinding
import com.example.booknet.domain.offers.TradesViewModel
import com.example.booknet.models.presentation.books.Book
import com.example.booknet.models.presentation.trades.TradeOffer
import com.example.booknet.models.presentation.trades.TradeOfferStatus
import com.example.booknet.models.presentation.users.UserInfo
import com.example.booknet.ui.profiles.UserProfileFragment
import com.example.booknet.ui.trades.adapters.TradeOfferBooksAdapter
import com.example.booknet.utils.Resource
import com.example.booknet.utils.Status
import com.example.booknet.utils.ViewUtils
import com.example.booknet.utils.showToastMessage
import timber.log.Timber

/**
 * Fragment for details of trade offer
 */
class TradeOfferDetailsFragment : Fragment(R.layout.fragment_trade_offer_details) {

    private val viewBinding by viewBinding(FragmentTradeOfferDetailsBinding::bind)

    private val tradesViewModel: TradesViewModel by activityViewModels()

    private val navController by lazy { findNavController() }

    private lateinit var suggestedBooksAdapter: TradeOfferBooksAdapter
    private lateinit var requestedBooksAdapter: TradeOfferBooksAdapter

    private var tradeOfferId: Long = 0L

    private var isEditMode: Boolean = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tradeOfferId = arguments?.getLong(TRADE_OFFER_ID, 0L) ?: 0L

        setupToolbar()
        setupListeners()
        setupObservers()
        setupBookLists()

        tradesViewModel.getTradeOfferById(tradeOfferId)
    }

    /**
     * Inflate menu depending on [isEditMode]
     */
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        Timber.d("Creating options menu")
        val menuResource = if (isEditMode) R.menu.trade_offer_edit_menu else R.menu.trade_offer_menu
        inflater.inflate(menuResource, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    /**
     * Setup toolbar menu and actions
     */
    private fun setupToolbar() {
        viewBinding.tbOffer.apply {
            setNavigationOnClickListener { navController.popBackStack() }

            val menuResource = if (isEditMode) R.menu.trade_offer_edit_menu else R.menu.trade_offer_menu
            inflateToolbarMenu(menuResource)

            setOnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.trade_offer_edit -> {
                        toEditMode()
                        true
                    }
                    R.id.trade_offer_delete -> {
                        openDeleteConfirmDialog()
                        true
                    }
                    R.id.trade_offer_edit_save -> {
                        toDisplayMode()
                        saveEdited()
                        true
                    }
                    R.id.trade_offer_edit_cancel -> {
                        toDisplayMode()
                        setupTradeOffer(tradesViewModel.currentTradeOfferState.value!!)
                        true
                    }
                    else -> {
                        Timber.w("Unknown item: $item")
                        false
                    }
                }
            }
        }
    }

    /**
     * Open dialog with delete confirmation
     */
    private fun openDeleteConfirmDialog() {
        AlertDialog.Builder(requireContext()).apply {
            setTitle(R.string.book_trade_offer_delete_title)
            setMessage(R.string.book_trade_offer_delete_message)
            setPositiveButton(R.string.book_trade_offer_delete_confirm) { dialog, _ ->
                tradesViewModel.deleteTradeOfferById(tradeOfferId)
                dialog.dismiss()
            }
            setNegativeButton(R.string.book_trade_offer_delete_cancel) { dialog, _ ->
                dialog.dismiss()
            }
        }
            .show()
    }

    /**
     * Setup UI listeners
     */
    private fun setupListeners() {
        viewBinding.apply {
            btnChangeSuggested.setOnClickListener {
                val currentTradeOffer = tradesViewModel.currentTradeOfferState.value?.data ?: return@setOnClickListener
                createBookPickerDialog(
                    title = getString(R.string.book_trade_offer_suggested_books),
                    userId = currentTradeOffer.senderId,
                    checkedBooks = suggestedBooksAdapter.currentList,
                    adapter = suggestedBooksAdapter,
                )
            }

            btnChangeRequested.setOnClickListener {
                val currentTradeOffer = tradesViewModel.currentTradeOfferState.value?.data ?: return@setOnClickListener
                createBookPickerDialog(
                    title = getString(R.string.book_trade_offer_requested_books),
                    userId = currentTradeOffer.receiverId,
                    checkedBooks = requestedBooksAdapter.currentList,
                    adapter = requestedBooksAdapter,
                )
            }
        }
    }

    /**
     * Create dialog to choose books
     */
    private fun createBookPickerDialog(
        title: String,
        userId: Long,
        checkedBooks: List<Book>,
        adapter: TradeOfferBooksAdapter,
    ) {
        val dialog = BookPickerDialog(
            title = title,
            userId = userId,
            alreadyCheckedBooks = checkedBooks,
            confirmListener = {
                adapter.submitList(it.toList())
            },
        )
        dialog.show(parentFragmentManager, BOOK_PICKER_FRAGMENT)
    }

    /**
     * Setup ViewModel observers
     */
    private fun setupObservers() {
        tradesViewModel.apply {
            currentTradeOfferState.observe(viewLifecycleOwner) {
                setupTradeOffer(it)
            }

            editTradeOfferState.observe(viewLifecycleOwner) {
                viewBinding.apply {
                    pbOfferLoading.isVisible = it.status == Status.LOADING
                    when (it.status) {
                        Status.SUCCESS -> {
                            showToastMessage(R.string.book_trade_offer_edit_success)
                            clearEditTradeOfferState()
                        }
                        Status.ERROR -> {
                            showToastMessage(R.string.book_trade_offer_edit_error)
                            tradesViewModel.getTradeOfferById(tradeOfferId)
                            clearEditTradeOfferState()
                        }
                        else -> {
                            // Ignore
                        }
                    }
                }
            }

            deleteTradeOfferState.observe(viewLifecycleOwner) {
                viewBinding.apply {
                    pbOfferLoading.isVisible = it.status == Status.LOADING
                    when (it.status) {
                        Status.SUCCESS -> {
                            showToastMessage(R.string.book_trade_offer_delete_success)
                            clearDeleteTradeOfferState()
                            navController.popBackStack()
                        }
                        Status.ERROR -> {
                            showToastMessage(it.message ?: "")
                            clearDeleteTradeOfferState()
                        }
                        else -> {
                            // Ignore
                        }
                    }
                }
            }
        }
    }

    /**
     * Setup trade offer UI from [tradeOfferResource]
     */
    private fun setupTradeOffer(tradeOfferResource: Resource<TradeOffer>) {
        viewBinding.apply {
            pbOfferLoading.isVisible = tradeOfferResource.status == Status.LOADING
            when (tradeOfferResource.status) {
                Status.SUCCESS -> {
                    if (tradeOfferResource.data == null) {
                        Timber.e("Empty data!")
                        return
                    }
                    val tradeOffer = tradeOfferResource.data
                    setupUser(lReceiver, tradeOffer.receiverData)
                    setupUser(lSender, tradeOffer.senderData)

                    etMessage.setText(tradeOffer.message)

                    suggestedBooksAdapter.submitList(tradeOffer.suggestedBooksData)
                    requestedBooksAdapter.submitList(tradeOffer.requestedBooksData)
                }
                Status.ERROR -> {
                    showToastMessage(tradeOfferResource.message ?: "")
                }
                else -> {
                    // Ignore
                }
            }
        }
    }

    /**
     * Setup user section to [itemBinding] with [userInfo]
     */
    private fun setupUser(
        itemBinding: ItemPersonBinding,
        userInfo: UserInfo,
    ) {
        ViewUtils.setupPersonItem(
            context = requireContext(),
            itemPersonBinding = itemBinding,
            userInfo = userInfo,
            isOptionsVisible = false
        )

        itemBinding.root.setOnClickListener {
            val arguments = bundleOf(
                UserProfileFragment.USER_ID to userInfo.id,
            )
            navController.navigate(R.id.action_tradeOfferDetailsFragment_to_userProfileFragment, arguments)
        }
    }

    /**
     * Save edited trade offer
     */
    private fun saveEdited() {
        viewBinding.apply {
            val currentTradeOffer = tradesViewModel.currentTradeOfferState.value?.data ?: run {
                showToastMessage(R.string.book_trade_offer_edit_error)
                return
            }

            val newSuggestedBooks = suggestedBooksAdapter.currentList.map { it.id }
            val newRequestedBooks = requestedBooksAdapter.currentList.map { it.id }
            val newMessage = etMessage.text.toString()

            val senderId = currentTradeOffer.senderId
            val receiverId = currentTradeOffer.receiverId

            tradesViewModel.editTradeOfferById(
                tradeOfferId = tradeOfferId,
                senderId = senderId,
                receiverId = receiverId,
                newRequestedBooksIds = newRequestedBooks,
                newSuggestedBooksIds = newSuggestedBooks,
                newMessage = newMessage,
                status = TradeOfferStatus.ACTIVE,
            )
        }
    }

    /**
     * Inflate [menu] for toolbar
     */
    private fun inflateToolbarMenu(@MenuRes menuResource: Int) {
        viewBinding.tbOffer.apply {
            menu.clear()
            val menuBuilder = menu as MenuBuilder
            menuBuilder.forEach { item ->
                item.icon?.setTint(resources.getColor(R.color.black, requireContext().theme))
            }
            inflateMenu(menuResource)
        }
    }

    /**
     * Setup edit mode
     */
    private fun toEditMode() {
        isEditMode = true
        viewBinding.apply {
            val currentTradeOffer = tradesViewModel.currentTradeOfferState.value?.data

            etMessage.isEnabled = currentTradeOffer?.senderId == tradesViewModel.getCurrentUserId()
            clEditBookLists.isVisible = true

            inflateToolbarMenu(R.menu.trade_offer_edit_menu)
        }
    }

    /**
     * Setup display mode
     */
    private fun toDisplayMode() {
        isEditMode = false
        viewBinding.apply {
            etMessage.isEnabled = false
            clEditBookLists.isVisible = false

            inflateToolbarMenu(R.menu.trade_offer_menu)
        }
    }

    /**
     * Setup lists for books
     */
    private fun setupBookLists() {
        suggestedBooksAdapter = TradeOfferBooksAdapter(showCheckboxes = false)
        requestedBooksAdapter = TradeOfferBooksAdapter(showCheckboxes = false)

        viewBinding.apply {
            setupBookList(rvSuggestedBooks, suggestedBooksAdapter)
            setupBookList(rvRequestedBooks, requestedBooksAdapter)
        }
    }

    /**
     * Setup [recyclerView] with [bookAdapter]
     */
    private fun setupBookList(
        recyclerView: RecyclerView,
        bookAdapter: TradeOfferBooksAdapter,
    ) {
        recyclerView.apply {
            adapter = bookAdapter
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            addItemDecoration(DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL))
        }
    }

    companion object {
        const val TRADE_OFFER_ID = "TRADE_OFFER_ID"

        private const val BOOK_PICKER_FRAGMENT = "BOOK_PICKER_FRAGMENT"
    }
}