package com.example.booknet.ui.auth

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.booknet.BuildConfig
import com.example.booknet.R
import com.example.booknet.databinding.FragmentAuthBinding
import com.example.booknet.domain.auth.AuthState
import com.example.booknet.domain.auth.AuthViewModel
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

/**
 * Fragment for Auth
 */
@AndroidEntryPoint
class AuthFragment : Fragment(R.layout.fragment_auth) {

    private val viewBinding by viewBinding(FragmentAuthBinding::bind)

    private val authViewModel: AuthViewModel by activityViewModels()

    private val navController by lazy { findNavController() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Timber.d("AuthFragment created!")

        viewBinding.splash.tvAppVersionTag.text = getString(R.string.app_version_tag, BuildConfig.VERSION_NAME)

        authViewModel.refreshToken()

        setupObserver()
    }

    /**
     * Setup observer of auth state
     */
    private fun setupObserver() {
        authViewModel.authState.observe(viewLifecycleOwner) {
            when (it) {
                AuthState.AuthSuccess -> navController.navigate(R.id.action_authFragment_to_dashboardFragment)
                AuthState.NoAuth -> setupLoginView()
                AuthState.AuthLoading -> showSplashScreen()
                is AuthState.AuthError -> {
                    Snackbar.make(requireView(), it.message, Snackbar.LENGTH_SHORT).show()
                    setupLoginView()
                }
            }
        }
    }

    /**
     * Show login screen
     */
    private fun setupLoginView() {
        navController.navigate(R.id.action_authFragment_to_loginFragment)
    }

    /**
     * Show splash screen
     */
    private fun showSplashScreen() {
        viewBinding.apply {
            splash.root.isVisible = true
        }
    }
}