package com.example.booknet.ui.auth

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.booknet.R
import com.example.booknet.databinding.FragmentRegisterBinding
import com.example.booknet.domain.auth.AuthViewModel
import com.example.booknet.utils.Status
import com.example.booknet.utils.showToastMessage
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RegisterFragment : Fragment(R.layout.fragment_register) {

    private val viewBinding by viewBinding(FragmentRegisterBinding::bind)

    private val authViewModel: AuthViewModel by activityViewModels()

    private val navController by lazy { findNavController() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupListeners()
        setupObservers()
        setupRegisterFields()
        viewBinding.btnRegister.isEnabled = false
    }

    override fun onDestroy() {
        authViewModel.clearRegisterState()
        super.onDestroy()
    }

    /**
     * Setup ViewModel observers
     */
    private fun setupObservers() {
        authViewModel.registrationState.observe(viewLifecycleOwner) {
            clearErrors()
            viewBinding.btnRegister.isEnabled = it.status == Status.SUCCESS || it.status == Status.ERROR
            when (it.status) {
                Status.SUCCESS -> {
                    showToastMessage(R.string.register_success)
                }
                Status.ERROR -> {
                    showToastMessage(R.string.register_error)
                    viewBinding.fields.tilEmail.error = it.message
                }
                else -> {
                    // Ignore
                }
            }
        }
    }

    /**
     * Setup UI listeners
     */
    private fun setupListeners() {
        viewBinding.apply {
            btnRegister.setOnClickListener {
                val email = fields.teEmail.text?.toString()
                val password = fields.tePassword.text?.toString()
                val confirmPassword = fields.teConfirmPassword.text?.toString()
                val firstName = fields.teFirstName.text?.toString()
                val lastName = fields.teLastName.text?.toString()

                if (password != confirmPassword) {
                    fields.tilConfirmPassword.error = resources.getString(R.string.passwords_dont_match)
                    return@setOnClickListener
                }
                if (email.isNullOrBlank() || password.isNullOrBlank() || firstName.isNullOrBlank() || lastName.isNullOrBlank()) return@setOnClickListener
                authViewModel.register(
                    email = email,
                    password = password,
                    firstName = firstName,
                    lastName = lastName,
                )
            }

            btnToLogin.setOnClickListener { navController.popBackStack() }

            fields.teEmail.addTextChangedListener {
                clearErrors()
                btnRegister.isEnabled = !areFieldsEmpty()
            }

            fields.teFirstName.addTextChangedListener {
                clearErrors()
                btnRegister.isEnabled = !areFieldsEmpty()
            }

            fields.teLastName.addTextChangedListener {
                clearErrors()
                btnRegister.isEnabled = !areFieldsEmpty()
            }

            fields.tePassword.addTextChangedListener {
                clearErrors()
                btnRegister.isEnabled = !areFieldsEmpty()
            }

            fields.teConfirmPassword.addTextChangedListener {
                clearErrors()
                btnRegister.isEnabled = !areFieldsEmpty()
            }

        }
    }

    /**
     * Clear error messages
     */
    private fun clearErrors() {
        viewBinding.fields.apply {
            tilEmail.error = null
            tilPassword.error = null
            tilConfirmPassword.error = null
            tilFirstName.error = null
            tilLastName.error = null
        }
    }

    /**
     * Show registration fields: Email, Password, Confirm Password, First Name, Last Name
     */
    private fun setupRegisterFields() {
        viewBinding.fields.apply {
            tilEmail.isVisible = true
            tilPassword.isVisible = true
            tilFirstName.isVisible = true
            tilLastName.isVisible = true
            tilConfirmPassword.isVisible = true
        }
    }

    /**
     * Return true if at least one of the fields is empty
     */
    private fun areFieldsEmpty(): Boolean {
        return viewBinding.fields.run {
            teEmail.text.isNullOrBlank() || tePassword.text.isNullOrBlank() || teFirstName.text.isNullOrBlank() || teLastName.text.isNullOrBlank() || teConfirmPassword.text.isNullOrBlank()
        }
    }

}