package com.example.booknet.ui.discussions.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.booknet.R
import com.example.booknet.databinding.ItemDiscussionCommentBinding
import com.example.booknet.di.GlideApp
import com.example.booknet.models.presentation.discussions.Comment
import com.example.booknet.models.presentation.users.UserInfo
import com.example.booknet.utils.SPACE_DELIMITER
import com.example.booknet.utils.ViewUtils
import com.example.booknet.utils.getFirstAndLast
import com.example.booknet.utils.joinNotEmpty

/**
 * Adapter for comments in discussions
 */
class CommentsAdapter(
    private val currentUserId: Long,
    private val onItemClick: (Comment, View) -> Unit,
    private val onUserAvatarClicked: (UserInfo) -> Unit,
) : ListAdapter<Comment, CommentsAdapter.ViewHolder>(CommentDiffCallback) {

    class ViewHolder(
        private val viewBinding: ItemDiscussionCommentBinding,
        private val currentUserId: Long,
        private val onItemClick: ((Int, View) -> Unit)? = null,
        private val onUserAvatarClicked: (Int) -> Unit,
    ) : RecyclerView.ViewHolder(viewBinding.root) {

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(absoluteAdapterPosition, itemView)
            }
        }

        fun bind(comment: Comment) {
            viewBinding.apply {
                val context = root.context
                tvUserName.text = listOf(comment.author.firstName, comment.author.lastName).joinNotEmpty(SPACE_DELIMITER)
                tvMessageDate.text = comment.dateCreated.toString("dd.MM.yyyy HH:mm:ss")

                tvMessageText.text = comment.message

                val avatarPlaceholder = ViewUtils.createRectNamePlaceholder(
                    values = listOf(comment.author.firstName ?: "", comment.author.lastName ?: "").getFirstAndLast(),
                    textColor = ResourcesCompat.getColor(context.resources, R.color.white, context.theme),
                    backColor = ResourcesCompat.getColor(context.resources, R.color.comment_background, context.theme)
                )

                GlideApp.with(context)
                    .load(comment.author.avatar)
                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                    .placeholder(avatarPlaceholder)
                    .error(avatarPlaceholder)
                    .into(ivUserAvatar)

                ivUserAvatar.setOnClickListener {
                    onUserAvatarClicked(absoluteAdapterPosition)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding = ItemDiscussionCommentBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(
            viewBinding = itemBinding,
            currentUserId = currentUserId,
            onItemClick = { position, view ->
                getItem(position)?.let { comment ->
                    onItemClick(comment, view)
                }
            },
            onUserAvatarClicked = { position ->
                getItem(position)?.let { comment ->
                    onUserAvatarClicked(comment.author)
                }
            }
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}

/**
 * Diff Callback for [Comment]
 */
object CommentDiffCallback : DiffUtil.ItemCallback<Comment>() {
    override fun areItemsTheSame(oldItem: Comment, newItem: Comment): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Comment, newItem: Comment): Boolean {
        return oldItem == newItem
    }
}