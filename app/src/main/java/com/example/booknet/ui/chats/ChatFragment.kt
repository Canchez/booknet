package com.example.booknet.ui.chats

import android.content.res.ColorStateList
import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.booknet.R
import com.example.booknet.api.ApiParameters
import com.example.booknet.api.WebSocketUtils
import com.example.booknet.databinding.FragmentChatBinding
import com.example.booknet.domain.chats.ChatsViewModel
import com.example.booknet.models.entity.chats.ChatMessageWSPostEntity
import com.example.booknet.ui.chats.adapters.ChatMessagesAdapter
import com.example.booknet.ui.profiles.UserProfileFragment
import com.example.booknet.utils.Status
import com.example.booknet.utils.showToastMessage
import com.google.gson.Gson
import okhttp3.Response
import okhttp3.WebSocket
import okhttp3.WebSocketListener
import okio.ByteString
import timber.log.Timber

/**
 * Fragment for chat with messages
 */
class ChatFragment : Fragment(R.layout.fragment_chat) {

    private val viewBinding by viewBinding(FragmentChatBinding::bind)

    private val chatsViewModel: ChatsViewModel by activityViewModels()

    private val navController by lazy { findNavController() }

    private val chatOkHttpClient by lazy { WebSocketUtils.createWebSocketClient() }

    private var chatWebSocket: WebSocket? = null

    private val webSocketListener = object : WebSocketListener() {
        override fun onOpen(webSocket: WebSocket, response: Response) {
            Timber.d("WebSocket:onOpen, Chat socket=$webSocket, response=$response")
            activity?.runOnUiThread {
                enableInput()
            }
        }

        override fun onMessage(webSocket: WebSocket, text: String) {
            Timber.d("WebSocket:onMessage:String, Chat socket=$webSocket, text=$text")
            handleReceivedMessage(text)
        }

        override fun onMessage(webSocket: WebSocket, bytes: ByteString) {
            Timber.d("WebSocket:onMessage:ByteString, Chat socket=$webSocket, text=$bytes")
        }

        override fun onClosing(webSocket: WebSocket, code: Int, reason: String) {
            Timber.d("WebSocket:onClosing, Chat socket=$webSocket, code=$code, reason=$reason")
            webSocket.close(code, reason)
            chatWebSocket = null
        }

        override fun onClosed(webSocket: WebSocket, code: Int, reason: String) {
            Timber.d("WebSocket:onClosed, Chat socket=$webSocket, code=$code, reason=$reason")
            activity?.runOnUiThread {
                disableInputWithError()
            }
            chatWebSocket = null
        }

        override fun onFailure(webSocket: WebSocket, t: Throwable, response: Response?) {
            Timber.d("WebSocket:onFailure, Chat socket=$webSocket, response=$response, t=$t, t.message=${t.message}, t.stackTrace=${t.stackTraceToString()}")
            activity?.runOnUiThread {
                disableInputWithError()
            }
            chatWebSocket = null
        }
    }

    private lateinit var chatMessagesAdapter: ChatMessagesAdapter

    private var chatId: Long = 0L
    private var chatName: String = ""

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        chatId = arguments?.getLong(ChatsListFragment.CHAT_ID) ?: 0L
        chatName = arguments?.getString(ChatsListFragment.CHAT_NAME) ?: ""

        setupToolbar()
        setupListeners()
        setupMessagesList()
        setupObservers()

        chatsViewModel.getChatDetails(chatId)
    }

    /**
     * Setup toolbar listeners
     */
    private fun setupToolbar() {
        viewBinding.tbChat.apply {
            title = chatName
            setNavigationOnClickListener {
                navController.popBackStack()
            }
        }
    }

    /**
     * Open web sockets when resuming
     */
    override fun onResume() {
        super.onResume()
        openWebSocket()
    }

    /**
     * Close web sockets when pausing
     */
    override fun onPause() {
        closeWebSocket()
        super.onPause()
    }

    /**
     * Shutdown client when destroyed
     */
    override fun onDestroy() {
        chatOkHttpClient.dispatcher.executorService.shutdown()
        super.onDestroy()
    }

    /**
     * Setup listeners for UI
     */
    private fun setupListeners() {
        viewBinding.apply {
            ivSend.setOnClickListener {
                val message = etMessageEditor.text.toString()
                if (message.isNotBlank()) {
                    val messageData = ChatMessageWSPostEntity(
                        message = message,
                        senderId = chatsViewModel.getUserId(),
                        dialogId = chatId,
                    )
                    val messageJson = Gson().toJson(messageData)
                    Timber.d("WebSocket:sendMessage, message=${messageJson}")
                    chatWebSocket?.send(messageJson) ?: showToastMessage(R.string.chat_connection_lost)
                    etMessageEditor.text = null
                }
            }
        }
    }

    /**
     * Setup ViewModel observers
     */
    private fun setupObservers() {
        chatsViewModel.currentChatState.observe(viewLifecycleOwner) {
            viewBinding.apply {
                pbChatLoading.isVisible = it.status == Status.LOADING
                when (it.status) {
                    Status.SUCCESS -> {
                        Timber.d("CHATS:ChatFragment, loaded chat ${it.data}")
                    }
                    Status.ERROR -> {
                        showToastMessage(it.message ?: "")
                    }
                    else -> {
                        // Ignore
                    }
                }
            }
        }

        chatsViewModel.currentMessagesList.observe(viewLifecycleOwner) {
            chatMessagesAdapter.submitList(it)
            val scrolled = viewBinding.rvMessages.postDelayed(
                {
                    Timber.d("CHATS:ChatFragment:currentMessagesList:observe:scroll")
                    viewBinding.rvMessages.scrollToPosition(chatMessagesAdapter.currentList.size - 1)
                },
                100L
            )
            Timber.d("CHATS:ChatFragment:currentMessagesList:observe, scrolled=$scrolled")
        }
    }

    /**
     * Setup chat messages list
     */
    private fun setupMessagesList() {
        chatMessagesAdapter = ChatMessagesAdapter(
            currentUserId = chatsViewModel.getUserId(),
            onUserAvatarClicked = {
                val arguments = bundleOf(
                    UserProfileFragment.USER_ID to it.id,
                )
                navController.navigate(R.id.action_chatFragment_to_userProfileFragment, arguments)
            }
        )
        viewBinding.rvMessages.apply {
            adapter = chatMessagesAdapter
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false).apply {
                stackFromEnd = true
            }
        }
    }

    /**
     * Enable inputs and change icon to send
     */
    private fun enableInput() {
        viewBinding.apply {
            etMessageEditor.isEnabled = true
            ivSend.isEnabled = true
            ivSend.isVisible = true
            pbEditLoading.isVisible = false
            ivSend.setImageResource(R.drawable.ic_send)
            ivSend.imageTintList = ColorStateList.valueOf(resources.getColor(R.color.primary, requireContext().theme))
        }
    }

    /**
     * Disable input and change icon to error
     */
    private fun disableInputWithError() {
        try {
            viewBinding.apply {
                etMessageEditor.isEnabled = false
                ivSend.isEnabled = false
                ivSend.isVisible = true
                pbEditLoading.isVisible = false
                ivSend.setImageResource(R.drawable.ic_warning)
                ivSend.imageTintList = ColorStateList.valueOf(resources.getColor(R.color.brand_red, requireContext().theme))
            }
        } catch (e: IllegalStateException) {
            Timber.w("Disable after destroy")
        }
    }

    /**
     * Disable input and show loading
     */
    private fun disableInputWithLoading() {
        viewBinding.apply {
            etMessageEditor.isEnabled = false
            ivSend.isEnabled = false
            ivSend.isVisible = false
            pbEditLoading.isVisible = true
        }
    }


    /**
     * Handle new [message] from web sockets
     */
    private fun handleReceivedMessage(message: String) {
        chatsViewModel.handleReceivedMessage(message)
    }

    /**
     * Open chat web socket
     */
    private fun openWebSocket() {
        disableInputWithLoading()
        val socketUrl = "${ApiParameters.WEB_SOCKET_BASE_URL}/chat/$chatId/"
        Timber.d("WebSocket:openWebSocket, url=$socketUrl")
        val request = WebSocketUtils.buildWebSocketRequest(socketUrl)
        chatWebSocket = chatOkHttpClient.newWebSocket(request, webSocketListener)
    }

    /**
     * Close chat web socket
     */
    private fun closeWebSocket() {
        chatWebSocket?.close(NORMAL_CLOSURE_CODE, "Fragment paused")
    }

    companion object {
        /**
         * Code for the case of normal web socket closing
         */
        private const val NORMAL_CLOSURE_CODE = 1000
    }
}