package com.example.booknet.ui.reviews

import android.content.res.ColorStateList
import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.booknet.R
import com.example.booknet.databinding.FragmentReviewCreateBinding
import com.example.booknet.domain.reviews.ReviewsViewModel
import com.example.booknet.ui.books.BookDetailsFragment.Companion.BOOK_ID
import com.example.booknet.utils.Status
import com.example.booknet.utils.showToastMessage
import dagger.hilt.android.AndroidEntryPoint

/**
 * Fragment for adding review
 */
@AndroidEntryPoint
class AddReviewFragment : Fragment(R.layout.fragment_review_create) {

    private val viewBinding by viewBinding(FragmentReviewCreateBinding::bind)

    private val reviewsViewModel: ReviewsViewModel by activityViewModels()

    private val navController by lazy { findNavController() }

    private var bookId: Long = 0L

    private var selectedStars: Int? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bookId = arguments?.getLong(BOOK_ID) ?: 0L

        setupToolbar()
        setupListeners()
        updateStars()
        setupObservers()
    }

    /**
     * Setup toolbar listeners
     */
    private fun setupToolbar() {
        viewBinding.tbReview.setNavigationOnClickListener { navController.navigateUp() }
    }

    /**
     * Setup listeners on UI
     */
    private fun setupListeners() {
        viewBinding.apply {
            btnAddReview.setOnClickListener {
                if (selectedStars != null && etReviewText.text.isNotEmpty()) {
                    reviewsViewModel.addReview(bookId, selectedStars!!, etReviewText.text.toString())
                }
            }

            etReviewText.doOnTextChanged { _, _, _, _ ->
                checkIfShouldEnable()
            }

            layoutStars.apply {
                ivStar1.setOnClickListener {
                    selectedStars = 1
                    checkIfShouldEnable()
                    updateStars()
                }
                ivStar2.setOnClickListener {
                    selectedStars = 2
                    checkIfShouldEnable()
                    updateStars()
                }
                ivStar3.setOnClickListener {
                    selectedStars = 3
                    checkIfShouldEnable()
                    updateStars()
                }
                ivStar4.setOnClickListener {
                    selectedStars = 4
                    checkIfShouldEnable()
                    updateStars()
                }
                ivStar5.setOnClickListener {
                    selectedStars = 5
                    checkIfShouldEnable()
                    updateStars()
                }
            }
        }
    }

    /**
     * Check if Add review button should be enabled
     */
    private fun checkIfShouldEnable() {
        viewBinding.apply {
            btnAddReview.isEnabled = !etReviewText.text.isNullOrEmpty() && selectedStars != null
        }
    }

    /**
     * Update stars colors
     */
    private fun updateStars() {
        viewBinding.layoutStars.apply {
            val stars = listOf(
                ivStar1,
                ivStar2,
                ivStar3,
                ivStar4,
                ivStar5,
            )

            stars.forEach {
                it.imageTintList = ColorStateList.valueOf(requireContext().getColor(R.color.shadow_grey))
            }

            selectedStars?.let { mark ->
                stars.subList(0, mark).forEach {
                    it.imageTintList = ColorStateList.valueOf(requireContext().getColor(R.color.primary))
                }
            }
        }
    }

    /**
     * Setup observers for ViewModel
     */
    private fun setupObservers() {
        reviewsViewModel.newReviewState.observe(viewLifecycleOwner) {
            viewBinding.pbReviewLoading.isVisible = it.status == Status.LOADING
            when (it.status) {
                Status.SUCCESS -> {
                    showToastMessage(R.string.review_success)
                    viewBinding.pbReviewLoading.isVisible = false
                    reviewsViewModel.clearNewReviewState()
                    navController.navigateUp()
                }
                Status.ERROR -> {
                    // TODO Implement
                    showToastMessage(it.message ?: "")
                    reviewsViewModel.clearNewReviewState()
                }
                else -> {
                    // Ignore
                }
            }
        }
    }
}