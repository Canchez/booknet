package com.example.booknet.ui.books.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.booknet.databinding.ItemPersonBinding
import com.example.booknet.models.presentation.users.UserInfo
import com.example.booknet.utils.ViewUtils

/**
 * Adapter for list of people
 */
class PeopleAdapter(
    private val currentUserId: Long,
    private val onItemClicked: (UserInfo) -> Unit,
    private val onOptionsClicked: (UserInfo, View) -> Unit,
) : ListAdapter<UserInfo, PeopleAdapter.ViewHolder>(PeopleDiffCallback) {

    class ViewHolder(
        private val viewBinding: ItemPersonBinding,
        private val currentUserId: Long,
        private val onItemClicked: (Int) -> Unit,
        private val onOptionsClicked: (Int, View) -> Unit
    ) : RecyclerView.ViewHolder(viewBinding.root) {

        init {
            viewBinding.apply {
                root.setOnClickListener { onItemClicked(absoluteAdapterPosition) }
                ivOptions.setOnClickListener { onOptionsClicked(absoluteAdapterPosition, root) }
            }
        }

        fun bind(userInfo: UserInfo) {
            ViewUtils.setupPersonItem(
                context = viewBinding.root.context,
                itemPersonBinding = viewBinding,
                userInfo = userInfo,
                isOptionsVisible = userInfo.id != currentUserId,
            )
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding = ItemPersonBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(
            viewBinding = itemBinding,
            currentUserId = currentUserId,
            onItemClicked = { position ->
                getItem(position)?.let { userInfo ->
                    onItemClicked(userInfo)
                }
            },
            onOptionsClicked = { position, view ->
                getItem(position)?.let { userInfo ->
                    onOptionsClicked(userInfo, view)
                }
            },
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}

/**
 * DiffCallback for [UserInfo]
 */
object PeopleDiffCallback : DiffUtil.ItemCallback<UserInfo>() {

    override fun areContentsTheSame(oldItem: UserInfo, newItem: UserInfo): Boolean {
        return oldItem == newItem && oldItem.isSelected == newItem.isSelected
    }

    override fun areItemsTheSame(oldItem: UserInfo, newItem: UserInfo): Boolean {
        return oldItem.id == newItem.id
    }
}