package com.example.booknet.ui.chats.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.booknet.R
import com.example.booknet.databinding.ItemChatMessageIncomingBinding
import com.example.booknet.databinding.ItemChatMessageOutgoingBinding
import com.example.booknet.di.GlideApp
import com.example.booknet.models.presentation.chats.ChatMessage
import com.example.booknet.models.presentation.users.UserInfo
import com.example.booknet.utils.ViewUtils

/**
 * Adapter for chat messages
 */
class ChatMessagesAdapter(
    private val currentUserId: Long,
    private val onUserAvatarClicked: (UserInfo) -> Unit,
) : ListAdapter<ChatMessage, ChatMessagesAdapter.MessageViewHolder>(ChatMessageDiffCallback) {

    /**
     * Base ViewHolder for messages
     */
    abstract class MessageViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        /**
         * Bind values from [chatMessage] to UI
         */
        abstract fun bind(chatMessage: ChatMessage)
    }

    /**
     * ViewHolder for incoming messages
     */
    class IncomingMessageViewHolder(
        view: View,
        private val onUserAvatarClicked: (Int) -> Unit,
        private val viewBinding: ItemChatMessageIncomingBinding,
    ) : MessageViewHolder(view) {

        override fun bind(chatMessage: ChatMessage) {
            viewBinding.apply {
                tvMessageText.text = chatMessage.content

                tvMessageDate.text = chatMessage.createdAt.toString("dd.MM.yyyy HH:mm:ss")

                val context = root.context
                val resources = context.resources
                val userInfo = chatMessage.senderData

                tvUserName.text = context.getString(R.string.books_people_full_name, userInfo.firstName, userInfo.lastName)
                val textDrawable = ViewUtils.createRoundNamePlaceholder(
                    values = listOf(userInfo.firstName, userInfo.lastName),
                    textColor = ResourcesCompat.getColor(resources, R.color.primary, context.theme),
                    backColor = ResourcesCompat.getColor(resources, R.color.white, context.theme)
                )
                if (userInfo.avatar == null) {
                    ivUserAvatar.setImageDrawable(textDrawable)
                } else {
                    GlideApp.with(context)
                        .load(userInfo.avatar)
                        .placeholder(textDrawable)
                        .error(textDrawable)
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .circleCrop()
                        .into(ivUserAvatar)
                }

                ivUserAvatar.setOnClickListener {
                    onUserAvatarClicked(absoluteAdapterPosition)
                }
            }
        }
    }

    /**
     * ViewHolder for outgoing messages
     */
    class OutgoingMessageViewHolder(
        view: View,
        private val viewBinding: ItemChatMessageOutgoingBinding,
    ) : MessageViewHolder(view) {

        override fun bind(chatMessage: ChatMessage) {
            viewBinding.apply {
                tvMessageText.text = chatMessage.content

                tvMessageDate.text = chatMessage.createdAt.toString("dd.MM.yyyy HH:mm:ss")
            }
        }
    }

    override fun onBindViewHolder(holder: MessageViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun getItemViewType(position: Int): Int {
        val message = getItem(position)
        return if (currentUserId == message.senderId) ITEM_OUTGOING else ITEM_INCOMING
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return if (viewType == ITEM_INCOMING) {
            val itemBinding = ItemChatMessageIncomingBinding.inflate(inflater, parent, false)
            IncomingMessageViewHolder(
                view = itemBinding.root,
                viewBinding = itemBinding,
                onUserAvatarClicked = { position ->
                    getItem(position)?.let { chatMessage ->
                        onUserAvatarClicked(chatMessage.senderData)
                    }
                }
            )
        } else {
            val itemBinding = ItemChatMessageOutgoingBinding.inflate(inflater, parent, false)
            OutgoingMessageViewHolder(
                view = itemBinding.root,
                viewBinding = itemBinding,
            )
        }
    }

    companion object {
        private const val ITEM_INCOMING = 1
        private const val ITEM_OUTGOING = 2
    }
}

/**
 * DiffCallback for [ChatMessage]
 */
object ChatMessageDiffCallback : DiffUtil.ItemCallback<ChatMessage>() {
    override fun areContentsTheSame(oldItem: ChatMessage, newItem: ChatMessage): Boolean {
        return oldItem == newItem
    }

    override fun areItemsTheSame(oldItem: ChatMessage, newItem: ChatMessage): Boolean {
        return oldItem.id == newItem.id
    }
}