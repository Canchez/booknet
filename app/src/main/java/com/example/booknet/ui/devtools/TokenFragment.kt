package com.example.booknet.ui.devtools

import android.content.ClipData
import android.content.ClipboardManager
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat.getSystemService
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.booknet.R
import com.example.booknet.databinding.FragmentTokenBinding
import com.example.booknet.domain.auth.AuthViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * Fragment for tokens (Debug info)
 */
@AndroidEntryPoint
class TokenFragment : Fragment(R.layout.fragment_token) {

    private val viewBinding by viewBinding(FragmentTokenBinding::bind)

    private val authViewModel: AuthViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupToolbar()
        setupTokens()
        setupListeners()
    }

    /**
     * Setup toolbar listeners
     */
    private fun setupToolbar() {
        viewBinding.tbToken.setNavigationOnClickListener { findNavController().navigateUp() }
    }

    /**
     * Setup tokens to UI
     */
    private fun setupTokens() {
        viewBinding.apply {
            tvAccessTokenValue.text = authViewModel.accessToken
            tvRefreshTokenValue.text = authViewModel.refreshToken
        }
    }

    /**
     * Setup UI listeners
     */
    private fun setupListeners() {
        val clipboard = getSystemService(requireContext(), ClipboardManager::class.java)
        viewBinding.apply {
            tvAccessTokenValue.setOnClickListener {
                val clip = ClipData.newPlainText("Copied text", tvAccessTokenValue.text)
                clipboard?.setPrimaryClip(clip)
                Toast.makeText(requireContext(), "Access token copied", Toast.LENGTH_SHORT).show()
            }

            tvRefreshTokenValue.setOnClickListener {
                val clip = ClipData.newPlainText("Copied text", tvRefreshTokenValue.text)
                clipboard?.setPrimaryClip(clip)
                Toast.makeText(requireContext(), "Refresh token copied", Toast.LENGTH_SHORT).show()
            }
        }
    }
}