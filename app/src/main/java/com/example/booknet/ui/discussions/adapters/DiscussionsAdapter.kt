package com.example.booknet.ui.discussions.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.booknet.databinding.ItemDiscussionBinding
import com.example.booknet.models.presentation.discussions.DashboardDiscussion
import com.example.booknet.models.presentation.discussions.Discussion

/**
 * Adapter for discussions
 */
class DiscussionsAdapter(
    private val onItemClick: ((Discussion) -> Unit)? = null
) : ListAdapter<Discussion, DiscussionsAdapter.ViewHolder>(DiscussionDiffCallback) {

    class ViewHolder(
        private val viewBinding: ItemDiscussionBinding,
        private val onItemClick: ((Int) -> Unit)? = null
    ) : RecyclerView.ViewHolder(viewBinding.root) {

        init {
            itemView.setOnClickListener { onItemClick?.invoke(absoluteAdapterPosition) }
        }

        fun bind(discussion: Discussion) {
            viewBinding.apply {
                tvDiscussionTitle.text = discussion.title
                tvDiscussionContent.text = discussion.description
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding = ItemDiscussionBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(
            viewBinding = itemBinding,
            onItemClick = { position ->
                getItem(position)?.let { discussion ->
                    onItemClick?.invoke(discussion)
                }
            }
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}

/**
 * Diff Callback for [DashboardDiscussion]
 */
object DiscussionDiffCallback : DiffUtil.ItemCallback<Discussion>() {

    override fun areContentsTheSame(oldItem: Discussion, newItem: Discussion): Boolean {
        return oldItem == newItem
    }

    override fun areItemsTheSame(oldItem: Discussion, newItem: Discussion): Boolean {
        return oldItem.id == newItem.id
    }

}