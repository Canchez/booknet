package com.example.booknet.ui.books

import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.ListPopupWindow
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.booknet.R
import com.example.booknet.databinding.DialogBookOfferBinding
import com.example.booknet.databinding.FragmentPeopleBinding
import com.example.booknet.domain.books.BooksViewModel
import com.example.booknet.domain.offers.TradesViewModel
import com.example.booknet.ui.books.adapters.PeopleAdapter
import com.example.booknet.ui.profiles.UserProfileFragment
import com.example.booknet.utils.Status
import com.example.booknet.utils.showToastMessage
import timber.log.Timber

/**
 * Mode for [BookPeopleFragment]
 */
enum class PeopleMode {
    WANT_TO_READ,
    WANT_TO_TRADE;

    companion object {
        /**
         * Convert [value] to [PeopleMode]
         */
        fun fromInt(value: Int) = when (value) {
            0 -> WANT_TO_READ
            else -> WANT_TO_TRADE
        }
    }
}

/**
 * Fragment for people who wants to read/trade book
 */
class BookPeopleFragment : Fragment(R.layout.fragment_people) {

    private val viewBinding by viewBinding(FragmentPeopleBinding::bind)

    private val booksViewModel: BooksViewModel by activityViewModels()
    private val tradesViewModel: TradesViewModel by activityViewModels()

    private val navController by lazy { findNavController() }

    private lateinit var peopleAdapter: PeopleAdapter

    private var bookId: Long = 0L
    private var peopleMode: PeopleMode = PeopleMode.WANT_TO_READ
    private var isInLibrary: Boolean = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bookId = arguments?.getLong(BookDetailsFragment.BOOK_ID) ?: 0L
        val peopleModeInt = arguments?.getInt(BookDetailsFragment.PEOPLE_MODE) ?: PeopleMode.WANT_TO_READ.ordinal
        peopleMode = PeopleMode.fromInt(peopleModeInt)
        if (peopleMode == PeopleMode.WANT_TO_READ) {
            booksViewModel.getPeopleWantRead(bookId)
        } else {
            booksViewModel.getPeopleWantTrade(bookId)
        }
        isInLibrary = arguments?.getBoolean(BookDetailsFragment.IS_IN_LIBRARY) ?: false
        setupToolbar()
        setupObservers()
        setupList()
    }

    /**
     * Setup toolbar title and listeners
     */
    private fun setupToolbar() {
        viewBinding.tbPeople.apply {
            setNavigationOnClickListener {
                navController.popBackStack()
            }

            val titleId = if (peopleMode == PeopleMode.WANT_TO_READ) R.string.books_people_want_to_read else R.string.books_people_want_to_trade
            title = getString(titleId)
        }
    }

    /**
     * Setup ViewModel observers
     */
    private fun setupObservers() {
        booksViewModel.peopleState.observe(viewLifecycleOwner) {
            viewBinding.apply {
                pbPeopleLoading.isVisible = it.status == Status.LOADING
                rvPeople.isVisible = it.status == Status.SUCCESS
                when (it.status) {
                    Status.SUCCESS -> {
                        peopleAdapter.submitList(it.data)
                    }
                    Status.ERROR -> {
                        showToastMessage(it.message ?: "")
                    }
                    else -> {
                        // Ignore
                    }
                }
            }
        }
    }

    /**
     * Setup list for people
     */
    private fun setupList() {
        peopleAdapter = PeopleAdapter(
            currentUserId = booksViewModel.getCurrentUserId(),
            onItemClicked = { user ->
                val arguments = bundleOf(
                    UserProfileFragment.USER_ID to user.id,
                )
                navController.navigate(R.id.action_bookPeopleFragment_to_userProfileFragment, arguments)
            },
            onOptionsClicked = { user, view ->
                openOptionsPopup(
                    userId = user.id ?: 0L,
                    optionsView = view,
                )
            }
        )

        viewBinding.rvPeople.apply {
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            adapter = peopleAdapter
        }
    }

    /**
     * Open popup with options
     */
    private fun openOptionsPopup(userId: Long, optionsView: View) {
        val options = listOf(
            if (peopleMode == PeopleMode.WANT_TO_TRADE) {
                getString(R.string.book_request_title)
            } else {
                getString(R.string.book_offer_title)
            },
        )
        val arrayAdapter = ArrayAdapter(requireContext(), R.layout.item_popup_option, options)
        val dropdownPopup = ListPopupWindow(requireContext())
            .apply {
                anchorView = optionsView
                isModal = true
                setOnItemClickListener { _, _, position, _ ->
                    when (position) {
                        0 -> {
                            if (peopleMode == PeopleMode.WANT_TO_TRADE) {
                                openRequestPopup(userId)
                            } else {
                                if (isInLibrary) {
                                    openOfferPopup(userId)
                                } else {
                                    openBookLibraryPopup()
                                }
                            }
                        }
                        else -> {
                            Timber.w("Unknown item clicked")
                        }
                    }
                    dismiss()
                }
                setAdapter(arrayAdapter)
            }
        dropdownPopup.show()
    }

    /**
     * Open popup for warning about not having a book
     */
    private fun openBookLibraryPopup() {
        AlertDialog.Builder(requireContext())
            .setTitle(R.string.book_offer_add_book_title)
            .setMessage(R.string.book_offer_add_book_text)
            .setPositiveButton(R.string.book_offer_add_book_add) { dialog, _ ->
                dialog.dismiss()
            }
            .setCancelable(true)
            .show()
    }

    /**
     * Open popup for book offer
     */
    private fun openOfferPopup(userId: Long) {
        val dialogView = DialogBookOfferBinding.inflate(layoutInflater)
        AlertDialog.Builder(requireContext())
            .setTitle(R.string.book_offer_title)
            .setCancelable(true)
            .setView(dialogView.root)
            .setPositiveButton(R.string.book_offer_request_message_send) { dialog, _ ->
                tradesViewModel.sendTradeOffer(
                    bookId = bookId,
                    receiverId = userId,
                    message = dialogView.etOfferMessage.text.toString(),
                )
                dialog.dismiss()
            }
            .setNegativeButton(R.string.dialog_cancel) { dialog, _ ->
                dialog.dismiss()
            }
            .show()
    }

    /**
     * Open popup for book request
     */
    private fun openRequestPopup(userId: Long) {
        val dialogView = DialogBookOfferBinding.inflate(layoutInflater)
        AlertDialog.Builder(requireContext())
            .setTitle(R.string.book_request_title)
            .setCancelable(true)
            .setView(dialogView.root)
            .setPositiveButton(R.string.book_offer_request_message_send) { dialog, _ ->
                tradesViewModel.sendTradeRequest(
                    bookId = bookId,
                    receiverId = userId,
                    message = dialogView.etOfferMessage.text.toString(),
                )
                dialog.dismiss()
            }
            .setNegativeButton(R.string.dialog_cancel) { dialog, _ ->
                dialog.dismiss()
            }
            .show()
    }
}