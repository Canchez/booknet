package com.example.booknet.ui.trades.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.booknet.R
import com.example.booknet.databinding.ItemOfferBinding
import com.example.booknet.models.presentation.trades.TradeOffer

/**
 * Adapter for trade offers
 */
class TradeOffersAdapter(
    private val currentUserId: Long,
    private val onItemClicked: (TradeOffer) -> Unit,
) : ListAdapter<TradeOffer, TradeOffersAdapter.ViewHolder>(TradeOfferDiffCallback) {

    class ViewHolder(
        private val viewBinding: ItemOfferBinding,
        private val currentUserId: Long,
        private val onItemClicked: (Int) -> Unit,
    ) : RecyclerView.ViewHolder(viewBinding.root) {

        init {
            viewBinding.apply {
                root.setOnClickListener { onItemClicked(absoluteAdapterPosition) }
            }
        }

        fun bind(tradeOffer: TradeOffer) {
            viewBinding.apply {
                val youString = root.context.getString(R.string.book_offer_you)
                tvMessage.text = tradeOffer.message

                val sender = tradeOffer.senderData
                tvSender.text = if (tradeOffer.senderId != currentUserId) "${sender.firstName} ${sender.lastName}" else youString

                val receiver = tradeOffer.receiverData
                tvReceiver.text = if (tradeOffer.receiverId != currentUserId) "${receiver.firstName} ${receiver.lastName}" else youString

            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding = ItemOfferBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(
            viewBinding = itemBinding,
            currentUserId = currentUserId,
            onItemClicked = { position ->
                getItem(position)?.let { offerRequest ->
                    onItemClicked(offerRequest)
                }
            },
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

}

/**
 * Diff Callback for [TradeOffer]
 */
object TradeOfferDiffCallback : DiffUtil.ItemCallback<TradeOffer>() {
    override fun areContentsTheSame(oldItem: TradeOffer, newItem: TradeOffer): Boolean {
        return oldItem == newItem
    }

    override fun areItemsTheSame(oldItem: TradeOffer, newItem: TradeOffer): Boolean {
        return oldItem.id == newItem.id
    }
}

