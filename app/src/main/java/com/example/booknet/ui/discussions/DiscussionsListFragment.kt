package com.example.booknet.ui.discussions

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.booknet.R
import com.example.booknet.databinding.FragmentDiscuissionsListBinding
import com.example.booknet.ui.common.adapters.FragmentsPagerAdapter
import com.google.android.material.tabs.TabLayoutMediator

/**
 * Fragment for discussions lists
 */
class DiscussionsListFragment : Fragment(R.layout.fragment_discuissions_list) {

    private val viewBinding by viewBinding(FragmentDiscuissionsListBinding::bind)

    private val navController by lazy { findNavController() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupToolbar()
        setupViewPager()
    }

    private fun setupToolbar() {
        viewBinding.tbDiscussions.setNavigationOnClickListener { navController.popBackStack() }
    }

    private fun setupViewPager() {
        val userDiscussionsFragment = UserDiscussionsFragment.newInstance()
        val commentedDiscussionsFragment = CommentedDiscussionsFragment.newInstance()
        val discussionsPagerAdapter = FragmentsPagerAdapter(
            listOf(
                userDiscussionsFragment,
                commentedDiscussionsFragment,
            ),
            childFragmentManager,
            lifecycle
        )

        val tabNames = listOf(
            getString(R.string.discussions_list_users_title),
            getString(R.string.discussions_list_commented_title),
        )

        viewBinding.apply {
            vpDiscussions.adapter = discussionsPagerAdapter
            TabLayoutMediator(tlDiscussionsTabs, vpDiscussions) { tab, position ->
                tab.text = tabNames[position]
            }.attach()
        }
    }

}