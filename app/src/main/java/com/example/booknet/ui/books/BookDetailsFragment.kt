package com.example.booknet.ui.books

import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import androidx.core.content.res.ResourcesCompat
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.booknet.R
import com.example.booknet.databinding.DialogChangeBookStatusBinding
import com.example.booknet.databinding.FragmentBookDetailsBinding
import com.example.booknet.di.GlideApp
import com.example.booknet.domain.books.BooksViewModel
import com.example.booknet.domain.books.LibraryViewModel
import com.example.booknet.models.presentation.books.Book
import com.example.booknet.models.presentation.books.BookStatus
import com.example.booknet.utils.*
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

/**
 * Fragment for Book Details screen
 */
@AndroidEntryPoint
class BookDetailsFragment : Fragment(R.layout.fragment_book_details) {

    private val viewBinding by viewBinding(FragmentBookDetailsBinding::bind)

    private val booksViewModel: BooksViewModel by activityViewModels()
    private val libraryViewModel: LibraryViewModel by activityViewModels()

    private val navController by lazy { findNavController() }

    private var bookId: Long = 0L

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        booksViewModel.clearCurrentBookState()

        bookId = arguments?.getLong(BOOK_ID) ?: 0L

        setupToolbar()
        setupListeners()
        setupObservers()

        booksViewModel.getBook(bookId)
    }

    /**
     * Setup toolbar listeners
     */
    private fun setupToolbar() {
        viewBinding.tbBook.setNavigationOnClickListener { navController.popBackStack() }
    }

    /**
     * Setup UI listeners
     */
    private fun setupListeners() {
        viewBinding.apply {
            btnAddToLibrary.setOnClickListener {
                openStatusDialog(isInLibrary = false)
            }

            btnChangeStatus.setOnClickListener {
                openStatusDialog(isInLibrary = true)
            }

            llReviews.setOnClickListener {
                val arguments = bundleOf(
                    BOOK_ID to bookId,
                )
                navController.navigate(R.id.action_bookDetailsFragment_to_reviewsFragment, arguments)
            }

            tvPeopleWantToRead.setOnClickListener {
                val bookStatus = booksViewModel.currentBookState.value?.data?.status ?: false
                val arguments = bundleOf(
                    BOOK_ID to bookId,
                    PEOPLE_MODE to PeopleMode.WANT_TO_READ.ordinal,
                    IS_IN_LIBRARY to (bookStatus != BookStatus.NOT_IN_LIBRARY)
                )
                navController.navigate(R.id.action_bookDetailsFragment_to_bookPeopleFragment, arguments)
            }

            tvPeopleWantToTrade.setOnClickListener {
                val arguments = bundleOf(
                    BOOK_ID to bookId,
                    PEOPLE_MODE to PeopleMode.WANT_TO_TRADE.ordinal,
                )
                navController.navigate(R.id.action_bookDetailsFragment_to_bookPeopleFragment, arguments)
            }
        }
    }

    /**
     * Open dialog with statuses
     *
     * Set listeners for buttons depending on [isInLibrary]
     *
     * @param isInLibrary True if book is in the user's library, false - otherwise
     */
    private fun openStatusDialog(isInLibrary: Boolean) {
        val dialogBuilder = AlertDialog.Builder(requireContext())
        val dialogView = DialogChangeBookStatusBinding.inflate(layoutInflater)
        dialogBuilder
            .setTitle(R.string.books_details_select_status)
            .setCancelable(true)
            .setView(dialogView.root)
            .setNegativeButton(R.string.dialog_cancel) { dialog, _ ->
                dialog.dismiss()
            }
        val dialog = dialogBuilder.show()
        dialogView.apply {
            tvStatusFinished.setOnClickListener {
                Timber.d("Clicked Finished")
                if (isInLibrary) {
                    libraryViewModel.changeBookStatus(bookId, BookStatus.FINISHED)
                } else {
                    libraryViewModel.addBookToLibrary(bookId, BookStatus.FINISHED)
                }
                dialog.dismiss()
            }

            tvStatusReading.setOnClickListener {
                Timber.d("Clicked Reading")
                if (isInLibrary) {
                    libraryViewModel.changeBookStatus(bookId, BookStatus.READING)
                } else {
                    libraryViewModel.addBookToLibrary(bookId, BookStatus.READING)
                }
                dialog.dismiss()
            }

            tvStatusWantToRead.setOnClickListener {
                Timber.d("Clicked Wanna read")
                if (isInLibrary) {
                    libraryViewModel.changeBookStatus(bookId, BookStatus.WANT_TO_READ)
                } else {
                    libraryViewModel.addBookToLibrary(bookId, BookStatus.WANT_TO_READ)
                }
                dialog.dismiss()
            }

            tvStatusWantToTrade.setOnClickListener {
                Timber.d("Clicked Wanna trade")
                if (isInLibrary) {
                    libraryViewModel.changeBookStatus(bookId, BookStatus.WANT_TO_TRADE)
                } else {
                    libraryViewModel.addBookToLibrary(bookId, BookStatus.WANT_TO_TRADE)
                }
                dialog.dismiss()
            }

            tvStatusNotInteresed.setOnClickListener {
                Timber.d("Clicked Not interested")
                if (isInLibrary) {
                    libraryViewModel.changeBookStatus(bookId, BookStatus.NOT_INTERESTED)
                } else {
                    libraryViewModel.addBookToLibrary(bookId, BookStatus.NOT_INTERESTED)
                }
                dialog.dismiss()
            }
        }
    }

    /**
     * Setup ViewModel observers
     */
    private fun setupObservers() {
        // Observe current book state
        booksViewModel.currentBookState.observe(viewLifecycleOwner) {
            setBookInfo(it)
        }

        // Observe state of status change
        libraryViewModel.changeStatusState.observe(viewLifecycleOwner) {
            when (it.status) {
                Status.SUCCESS -> {
                    booksViewModel.getBook(bookId)
                    showToastMessage(R.string.books_details_status_change_success)
                    libraryViewModel.clearChangeBookStatusState()
                }
                Status.ERROR -> {
                    showToastMessage(R.string.books_details_status_change_error, it.responseCode)
                    libraryViewModel.clearChangeBookStatusState()
                }
                else -> {
                    // Ignore
                }
            }
        }

        // Observe state of adding book in library
        libraryViewModel.addBookState.observe(viewLifecycleOwner) {
            setBookInfo(it)
        }
    }

    /**
     * Set UI with book info from [bookResource]
     */
    private fun setBookInfo(bookResource: Resource<Book>) {
        viewBinding.pbBookLoading.isVisible = bookResource.status == Status.LOADING
        when (bookResource.status) {
            Status.ERROR -> {
                // TODO Implement
                showToastMessage(bookResource.message ?: "")
                libraryViewModel.clearAddBookState()
            }
            Status.SUCCESS -> {
                val bookInfo = bookResource.data ?: return
                viewBinding.apply {
                    setBasicBookInfo(bookInfo)
                    setPeopleSection(bookInfo)
                    setButtons(bookInfo)
                    setBookCover(bookInfo)
                }
                libraryViewModel.clearAddBookState()
            }
            Status.LOADING -> {
                viewBinding.apply {
                    tvPeopleWantToRead.isVisible = false
                    vPeopleReadDivider.isVisible = false
                    tvPeopleWantToTrade.isVisible = false
                    vPeopleTradeDivider.isVisible = false
                    btnAddToLibrary.isVisible = false
                    btnChangeStatus.isVisible = false
                }
            }
            else -> {
                // Ignore
            }
        }
    }

    /**
     * Set basic book info from [bookInfo]: title, author, rating, etc.
     */
    private fun setBasicBookInfo(bookInfo: Book) {
        viewBinding.apply {
            tbBook.title = bookInfo.title
            tvBookTitle.text = bookInfo.title
            bookInfo.status.readableStringId?.let {
                tvStatusValue.text = getString(it)
            }
            llStatus.isVisible = bookInfo.status != BookStatus.NOT_IN_LIBRARY
            tvAuthorValue.text = bookInfo.author
            tvReadersValue.text = bookInfo.readersCount.toString()
            tvPagesValue.text = bookInfo.pages.toString()
            tvRatingValue.text = bookInfo.averageRating.toString()
            tvReviewsValue.text = bookInfo.reviewsAmount.toString()
            tvDescriptionValue.text = bookInfo.description
        }
    }

    /**
     * Set people section with [bookInfo]
     */
    private fun setPeopleSection(bookInfo: Book) {
        viewBinding.apply {
            tvPeopleWantToRead.text =
                resources.getQuantityString(R.plurals.books_details_people_want_to_read, bookInfo.wantToReadAmount, bookInfo.wantToReadAmount)
            tvPeopleWantToTrade.text =
                resources.getQuantityString(R.plurals.books_details_people_want_to_trade, bookInfo.wantToTradeAmount, bookInfo.wantToTradeAmount)

            tvPeopleWantToRead.isVisible = bookInfo.wantToReadAmount > 0
            vPeopleReadDivider.isVisible = bookInfo.wantToReadAmount > 0
            tvPeopleWantToTrade.isVisible = bookInfo.wantToTradeAmount > 0
            vPeopleTradeDivider.isVisible = bookInfo.wantToTradeAmount > 0
        }
    }

    /**
     * Set buttons depending on [bookInfo]
     */
    private fun setButtons(bookInfo: Book) {
        viewBinding.apply {
            val isBookInLibrary = bookInfo.status != BookStatus.NOT_IN_LIBRARY
            btnAddToLibrary.isVisible = !isBookInLibrary
            btnChangeStatus.isVisible = isBookInLibrary
        }
    }

    /**
     * Set book cover from [bookInfo]
     */
    private fun setBookCover(bookInfo: Book) {
        viewBinding.apply {
            val bookPlaceholder = ViewUtils.createRoundNamePlaceholder(
                values = bookInfo.title.split(SPACE_DELIMITER).getFirstAndLast(),
                textColor = ResourcesCompat.getColor(resources, R.color.primary, requireContext().theme),
                backColor = ResourcesCompat.getColor(resources, R.color.white, requireContext().theme),
            )
            GlideApp.with(root)
                .load(bookPlaceholder) // TODO Add thumbnail
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .placeholder(bookPlaceholder)
                .error(bookPlaceholder)
                .into(ivBookCover)
        }
    }

    companion object {
        const val BOOK_ID = "BOOK_ID"
        const val PEOPLE_MODE = "PEOPLE_MODE"
        const val IS_IN_LIBRARY = "IS_IN_LIBRARY"
    }
}