package com.example.booknet.ui.chats

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.booknet.R
import com.example.booknet.databinding.FragmentChatCreateBinding
import com.example.booknet.domain.chats.ChatsViewModel
import com.example.booknet.ui.chats.adapters.ChatParticipantsAdapter
import com.example.booknet.utils.Status
import com.example.booknet.utils.showToastMessage

/**
 * Fragment for chat creation
 */
class ChatCreateFragment : Fragment(R.layout.fragment_chat_create) {

    private val viewBinding by viewBinding(FragmentChatCreateBinding::bind)

    private val chatsViewModel: ChatsViewModel by activityViewModels()

    private val navController by lazy { findNavController() }

    private val chatParticipantsAdapter = ChatParticipantsAdapter(
        onItemClicked = {
            chatsViewModel.addOrRemoveChatParticipant(it)
        },
        onOptionsClicked = { userInfo, view ->
            // TODO Implement or remove
        }
    )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupToolbar()
        setupListeners()
        setupParticipantsList()
        setupObservers()
    }

    override fun onDestroy() {
        chatsViewModel.apply {
            clearChatCreate()
            clearSelected()
        }
        super.onDestroy()
    }

    /**
     * Setup toolbar listeners
     */
    private fun setupToolbar() {
        viewBinding.tbChatCreate.setNavigationOnClickListener {
            navController.popBackStack()
        }
    }

    /**
     * Setup UI listeners
     */
    private fun setupListeners() {
        viewBinding.apply {
            btnCreate.setOnClickListener {
                val chatName = etChatName.text.toString()
                if (chatName.isNotBlank()) {
                    chatsViewModel.createChat(chatName)
                } else {
                    showToastMessage(R.string.chat_create_empty)
                }
            }

            btnChooseParticipants.setOnClickListener {
                navController.navigate(R.id.action_chatCreateFragment_to_chatParticipantsFragment)
            }
        }
    }

    /**
     * Setup ViewModel observers
     */
    private fun setupObservers() {
        chatsViewModel.selectedPeople.observe(viewLifecycleOwner) {
            chatParticipantsAdapter.submitList(it)
        }

        chatsViewModel.createChatState.observe(viewLifecycleOwner) {
            viewBinding.pbPeopleLoading.isVisible = it.status == Status.LOADING
            when (it.status) {
                Status.SUCCESS -> {
                    showToastMessage(R.string.chat_create_success)
                    navController.popBackStack()
                }
                Status.ERROR -> {
                    showToastMessage(it.message ?: "")
                }
                else -> {
                    // Ignore
                }
            }
        }
    }

    /**
     * Setup list of participants
     */
    private fun setupParticipantsList() {
        viewBinding.rvParticipants.apply {
            adapter = chatParticipantsAdapter
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        }
    }

}