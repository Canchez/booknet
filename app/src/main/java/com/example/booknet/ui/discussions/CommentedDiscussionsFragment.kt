package com.example.booknet.ui.discussions

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.booknet.R
import com.example.booknet.databinding.FragmentCommentedDiscussionsBinding
import com.example.booknet.domain.discussions.DiscussionsViewModel
import com.example.booknet.ui.discussions.adapters.DiscussionsAdapter
import com.example.booknet.utils.Status
import com.example.booknet.utils.showToastMessage

/**
 * Fragment for discussions commented by user
 */
class CommentedDiscussionsFragment : Fragment(R.layout.fragment_commented_discussions) {

    private val viewBinding by viewBinding(FragmentCommentedDiscussionsBinding::bind)

    private val discussionsViewModel: DiscussionsViewModel by activityViewModels()

    private val navController by lazy { findNavController() }

    private lateinit var discussionsAdapter: DiscussionsAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupObservers()
        setupBooksList()
        discussionsViewModel.getCommentedByUserDiscussions()
    }

    private fun setupObservers() {
        discussionsViewModel.commentedByUserDiscussionsState.observe(viewLifecycleOwner) {
            viewBinding.pbLoading.isVisible = it.status == Status.LOADING
            when (it.status) {
                Status.ERROR -> {
                    showToastMessage(R.string.discussions_list_error)
                }
                Status.SUCCESS -> {
                    discussionsAdapter.submitList(it.data)
                }
                else -> {
                    // Ignore
                }
            }
        }
    }

    private fun setupBooksList() {
        discussionsAdapter = DiscussionsAdapter {
            val arguments = bundleOf(
                DiscussionDetailsFragment.DISCUSSION_ID to it.id,
            )
            navController.navigate(R.id.action_usersDiscussionsFragment_to_discussionDetailsFragment, arguments)
        }

        viewBinding.rvDiscussions.apply {
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            adapter = discussionsAdapter
        }
    }

    companion object {
        fun newInstance() = CommentedDiscussionsFragment()
    }
}