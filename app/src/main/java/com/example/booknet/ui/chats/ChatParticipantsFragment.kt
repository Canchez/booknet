package com.example.booknet.ui.chats

import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.booknet.R
import com.example.booknet.databinding.FragmentChatParticipantsBinding
import com.example.booknet.domain.chats.ChatsViewModel
import com.example.booknet.models.presentation.users.UserInfo
import com.example.booknet.ui.chats.adapters.ChatParticipantsAdapter
import com.example.booknet.utils.Status
import com.example.booknet.utils.showToastMessage

/**
 * Fragment for searching and adding chat participants
 */
class ChatParticipantsFragment : Fragment(R.layout.fragment_chat_participants) {

    private val viewBinding by viewBinding(FragmentChatParticipantsBinding::bind)

    private val chatsViewModel: ChatsViewModel by activityViewModels()

    private val navController by lazy { findNavController() }

    private val chatParticipantsAdapter = ChatParticipantsAdapter(
        onItemClicked = ::addOrRemoveUser,
        onOptionsClicked = { userInfo, view ->
            // TODO Implement or remove
        }
    )

    /**
     * Add or remove [user] and notify adapter
     */
    private fun addOrRemoveUser(user: UserInfo) {
        chatsViewModel.addOrRemoveChatParticipant(user)
        chatParticipantsAdapter.notifyDataSetChanged()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupToolbar()
        setupParticipantsList()
        setupListeners()
        setupObservers()
    }

    override fun onDestroy() {
        chatsViewModel.clearSearch()
        super.onDestroy()
    }

    /**
     * Setup toolbar listeners
     */
    private fun setupToolbar() {
        viewBinding.tbChatParticipants.apply {
            setNavigationOnClickListener {
                navController.popBackStack()
            }

            setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.menu_save -> {
                        navController.popBackStack()
                        true
                    }
                    else -> false
                }
            }
        }
    }

    /**
     * Setup UI listeners
     */
    private fun setupListeners() {
        viewBinding.apply {
            ivSearch.setOnClickListener {
                if (etSearch.text.isNotBlank()) chatsViewModel.searchChatParticipants(etSearch.text.toString())
            }

            etSearch.setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (etSearch.text.isNotBlank()) chatsViewModel.searchChatParticipants(etSearch.text.toString())
                    return@setOnEditorActionListener true
                }
                return@setOnEditorActionListener false
            }
        }
    }

    /**
     * Setup ViewModel observers
     */
    private fun setupObservers() {
        chatsViewModel.searchPeople.observe(viewLifecycleOwner) {
            viewBinding.apply {
                pbPeopleLoading.isVisible = it.status == Status.LOADING
                rvPeople.isVisible = it.status == Status.SUCCESS
            }
            when (it.status) {
                Status.SUCCESS -> {
                    val people = it.data?.results ?: emptyList()
                    chatParticipantsAdapter.submitList(people)
                }
                Status.ERROR -> {
                    // TODO Implement
                    showToastMessage(it.message ?: "")
                }
                else -> {
                    // Ignore
                }
            }
        }
    }

    /**
     * Setup list of participants
     */
    private fun setupParticipantsList() {
        viewBinding.rvPeople.apply {
            adapter = chatParticipantsAdapter
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        }
    }

}