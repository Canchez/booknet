package com.example.booknet.ui.dashboard.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.booknet.R
import com.example.booknet.databinding.ItemBookBinding
import com.example.booknet.di.GlideApp
import com.example.booknet.models.presentation.books.DashboardBook
import com.example.booknet.utils.SPACE_DELIMITER
import com.example.booknet.utils.ViewUtils
import com.example.booknet.utils.getFirstAndLast

/**
 * Adapter for books
 *
 * @param onItemClicked Action for click on item
 */
class DashboardBooksAdapter(
    private val onItemClicked: ((DashboardBook) -> Unit)? = null
) : ListAdapter<DashboardBook, DashboardBooksAdapter.ViewHolder>(BookDiffCallback) {

    /**
     * ViewHolder for book
     *
     * @param onItemClick Action for click on item
     */
    class ViewHolder(
        private val viewBinding: ItemBookBinding,
        private val onItemClick: ((Int) -> Unit)? = null,
    ) : RecyclerView.ViewHolder(viewBinding.root) {

        init {
            itemView.setOnClickListener { onItemClick?.invoke(absoluteAdapterPosition) }
        }

        fun bind(book: DashboardBook) {
            viewBinding.apply {
                val context = root.context
                tvBookName.text = book.title
                val bookPlaceholder = ViewUtils.createRoundNamePlaceholder(
                    values = book.title.split(SPACE_DELIMITER).getFirstAndLast(),
                    textColor = ResourcesCompat.getColor(context.resources, R.color.primary, context.theme),
                    backColor = ResourcesCompat.getColor(context.resources, R.color.white, context.theme),
                )
                GlideApp.with(root)
                    .load(bookPlaceholder) // TODO Add thumbnail
                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                    .placeholder(bookPlaceholder)
                    .error(bookPlaceholder)
                    .into(ivBook)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding = ItemBookBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemBinding) { getItem(it)?.let { item -> onItemClicked?.invoke(item) } }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}

/**
 * Diff callback for [DashboardBook]
 */
object BookDiffCallback : DiffUtil.ItemCallback<DashboardBook>() {
    override fun areItemsTheSame(oldItem: DashboardBook, newItem: DashboardBook): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: DashboardBook, newItem: DashboardBook): Boolean {
        return oldItem == newItem
    }
}