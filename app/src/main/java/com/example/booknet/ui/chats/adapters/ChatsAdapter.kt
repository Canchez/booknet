package com.example.booknet.ui.chats.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.booknet.R
import com.example.booknet.databinding.ItemChatBinding
import com.example.booknet.di.GlideApp
import com.example.booknet.models.presentation.chats.Chat
import com.example.booknet.utils.ViewUtils

/**
 * Adapter for chats
 */
class ChatsAdapter(
    private val onItemClick: (chat: Chat) -> Unit,
) : ListAdapter<Chat, ChatsAdapter.ViewHolder>(ChatDiffCallback) {

    class ViewHolder(
        private val viewBinding: ItemChatBinding,
        private val onItemClick: (position: Int) -> Unit,
    ) : RecyclerView.ViewHolder(viewBinding.root) {

        init {
            viewBinding.root.setOnClickListener { onItemClick(absoluteAdapterPosition) }
        }

        fun bind(chat: Chat) {
            viewBinding.apply {
                val resources = root.context.resources
                tvChatName.text = chat.name
                tvParticipants.text = resources.getString(R.string.chat_item_participants, chat.attendees.size)
                tvChatLastMessage.text = chat.messages.lastOrNull()?.content ?: resources.getString(R.string.chat_no_messages)

                val context = root.context
                val textDrawable = ViewUtils.createRectNamePlaceholder(
                    values = listOf(chat.name),
                    textColor = ResourcesCompat.getColor(resources, R.color.primary, context.theme),
                    backColor = ResourcesCompat.getColor(resources, R.color.white, context.theme),
                )
                if (chat.image.isNullOrEmpty()) {
                    ivChatAvatar.setImageDrawable(textDrawable)
                } else {
                    GlideApp.with(context)
                        .load(chat.image)
                        .placeholder(textDrawable)
                        .error(textDrawable)
                        .into(ivChatAvatar)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding = ItemChatBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(
            viewBinding = itemBinding,
            onItemClick = { position ->
                getItem(position)?.let { chat ->
                    onItemClick(chat)
                }
            }
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}

/**
 * DiffCallback for [Chat]
 */
object ChatDiffCallback : DiffUtil.ItemCallback<Chat>() {
    override fun areContentsTheSame(oldItem: Chat, newItem: Chat): Boolean {
        return oldItem == newItem
    }

    override fun areItemsTheSame(oldItem: Chat, newItem: Chat): Boolean {
        return oldItem.id == newItem.id
    }
}

