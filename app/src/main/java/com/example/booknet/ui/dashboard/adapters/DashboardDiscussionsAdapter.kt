package com.example.booknet.ui.dashboard.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.booknet.R
import com.example.booknet.databinding.ItemDiscussionDashboardBinding
import com.example.booknet.models.presentation.discussions.DashboardDiscussion

/**
 * Adapter for discussions on dashboard
 *
 * @param onItemClicked Action for click on item
 */
class DashboardDiscussionsAdapter(
    private val onItemClicked: ((DashboardDiscussion) -> Unit)? = null
) : ListAdapter<DashboardDiscussion, DashboardDiscussionsAdapter.ViewHolder>(DashboardDiscussionDiffCallback) {

    /**
     * ViewHolder for discussion
     *
     * @param onItemClick Action for click on item
     */
    class ViewHolder(
        view: View,
        private val onItemClick: ((Int) -> Unit)? = null,
    ) : RecyclerView.ViewHolder(view) {

        private val viewBinding by viewBinding(ItemDiscussionDashboardBinding::bind)

        init {
            itemView.setOnClickListener { onItemClick?.invoke(absoluteAdapterPosition) }
        }

        fun bind(discussion: DashboardDiscussion) {
            viewBinding.apply {
                tvDiscussionName.text = discussion.name
                ivDiscussion.setImageResource(R.drawable.ic_discussions)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_discussion_dashboard, parent, false)
        return ViewHolder(view) { getItem(it)?.let { item -> onItemClicked?.invoke(item) } }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

}

/**
 * Diff Callback for [DashboardDiscussion]
 */
object DashboardDiscussionDiffCallback : DiffUtil.ItemCallback<DashboardDiscussion>() {

    override fun areContentsTheSame(oldItem: DashboardDiscussion, newItem: DashboardDiscussion): Boolean {
        return oldItem == newItem
    }

    override fun areItemsTheSame(oldItem: DashboardDiscussion, newItem: DashboardDiscussion): Boolean {
        return oldItem.id == newItem.id
    }

}