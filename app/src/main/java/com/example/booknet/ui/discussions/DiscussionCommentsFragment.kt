package com.example.booknet.ui.discussions

import android.os.Bundle
import android.view.View
import android.widget.PopupMenu
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.booknet.R
import com.example.booknet.databinding.FragmentDiscussionCommentsBinding
import com.example.booknet.domain.discussions.DiscussionsViewModel
import com.example.booknet.models.presentation.discussions.Comment
import com.example.booknet.ui.discussions.adapters.CommentsAdapter
import com.example.booknet.ui.profiles.UserProfileFragment
import com.example.booknet.utils.Status
import com.example.booknet.utils.showToastMessage

/**
 * Fragment for discussion comments list
 */
class DiscussionCommentsFragment : Fragment(R.layout.fragment_discussion_comments) {

    private val viewBinding by viewBinding(FragmentDiscussionCommentsBinding::bind)

    private val discussionsViewModel: DiscussionsViewModel by activityViewModels()

    private val navController by lazy { findNavController() }

    private lateinit var commentsAdapter: CommentsAdapter

    private var discussionId: Long = 0L
    private var discussionTitle: String = ""

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        discussionId = arguments?.getLong(DiscussionDetailsFragment.DISCUSSION_ID) ?: 0L
        discussionTitle = arguments?.getString(DiscussionDetailsFragment.DISCUSSION_TITLE) ?: ""

        setupToolbar()
        setupListeners()
        setupObservers()
        setupCommentsList()

        discussionsViewModel.getComments(discussionId)
    }

    /**
     * Setup toolbar title and listeners
     */
    private fun setupToolbar() {
        viewBinding.tbDiscussionComments.apply {
            setNavigationOnClickListener {
                navController.popBackStack()
            }
            title = discussionTitle
        }
    }

    /**
     * Setup UI listeners
     */
    private fun setupListeners() {
        viewBinding.apply {
            ivSend.setOnClickListener {
                if (etMessageEditor.text.isNotEmpty()) {
                    discussionsViewModel.addComment(discussionId, etMessageEditor.text.toString())
                    etMessageEditor.text = null
                }
            }

            rvComments.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    if (!recyclerView.canScrollVertically(1) && recyclerView.scrollState == RecyclerView.SCROLL_STATE_IDLE
                        && discussionsViewModel.commentsNextPageState.value?.status != Status.LOADING
                    ) {
                        discussionsViewModel.getNextCommentsPage(discussionId)
                    }
                    super.onScrollStateChanged(recyclerView, newState)
                }
            })
        }
    }

    /**
     * Setup ViewModel observer for comments
     */
    private fun setupObservers() {
        discussionsViewModel.currentComments.observe(viewLifecycleOwner) {
            viewBinding.pbDiscussionCommentsLoading.isVisible = it.status == Status.LOADING
            when (it.status) {
                Status.SUCCESS -> {
                    val comments = it.data?.results ?: return@observe
                    commentsAdapter.submitList(comments)
                }
                Status.ERROR -> {
                    showToastMessage(R.string.discussion_comments_error)
                }
                else -> {
                    // Ignore
                }
            }
        }

        discussionsViewModel.commentsNextPageState.observe(viewLifecycleOwner) {
            viewBinding.pbPageLoading.isVisible = it.status == Status.LOADING
            if (it.status == Status.ERROR) showToastMessage(it.message ?: "")
        }

        discussionsViewModel.commentAddState.observe(viewLifecycleOwner) {
            if (it.status == Status.ERROR) showToastMessage(it.message ?: "")
        }

    }

    /**
     * Setup list of comments
     */
    private fun setupCommentsList() {
        val currentUserId = discussionsViewModel.getCurrentUserId() ?: 0L
        commentsAdapter = CommentsAdapter(
            currentUserId = currentUserId,
            onItemClick = { comment, view ->
                if (comment.author.id == currentUserId || discussionsViewModel.isUserAdmin(discussionId)) {
                    openMessagePopup(comment, view)
                }
            },
            onUserAvatarClicked = {
                val arguments = bundleOf(
                    UserProfileFragment.USER_ID to it.id,
                )
                navController.navigate(R.id.action_discussionCommentsFragment_to_userProfileFragment, arguments)
            }
        )
        viewBinding.rvComments.apply {
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            adapter = commentsAdapter
        }
    }

    /**
     * Open popup for message actions
     */
    private fun openMessagePopup(comment: Comment, view: View) {
        val popupMenu = PopupMenu(requireContext(), view)
        val inflater = popupMenu.menuInflater
        popupMenu.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.comment_delete -> {
                    discussionsViewModel.deleteComment(discussionId, comment.id)
                }
                R.id.comment_edit -> {
                    // TODO AR 16.02.2022: Implement
                }
            }
            true
        }
        inflater.inflate(R.menu.comment_admin, popupMenu.menu)
        popupMenu.show()
    }
}