package com.example.booknet.ui.profiles

import android.os.Bundle
import android.view.View
import androidx.core.content.res.ResourcesCompat
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.booknet.R
import com.example.booknet.databinding.FragmentUserProfileBinding
import com.example.booknet.di.GlideApp
import com.example.booknet.domain.profiles.ProfilesViewModel
import com.example.booknet.models.presentation.users.UserInfo
import com.example.booknet.ui.books.LibraryFragment
import com.example.booknet.utils.Status
import com.example.booknet.utils.ViewUtils
import com.example.booknet.utils.showToastMessage

/**
 * Fragment for user profile
 */
class UserProfileFragment : Fragment(R.layout.fragment_user_profile) {

    private val viewBinding by viewBinding(FragmentUserProfileBinding::bind)

    private val profilesViewModel: ProfilesViewModel by activityViewModels()

    private val navController by lazy { findNavController() }

    private var userId: Long = 0L

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        userId = arguments?.getLong(USER_ID) ?: profilesViewModel.getCurrentUserId()

        setupToolbar()
        setupListeners()
        setupObserver()

        profilesViewModel.getProfile(userId)
    }

    /**
     * Setup toolbar listeners
     */
    private fun setupToolbar() {
        viewBinding.tbProfile.setNavigationOnClickListener { navController.popBackStack() }
    }

    /**
     * Setup UI listeners
     */
    private fun setupListeners() {
        viewBinding.btnLibrary.setOnClickListener {
            val arguments = bundleOf(
                LibraryFragment.USER_ID to userId
            )
            navController.navigate(R.id.action_userProfileFragment_to_libraryFragment, arguments)
        }
    }

    /**
     * Setup ViewModel observers
     */
    private fun setupObserver() {
        profilesViewModel.profileState.observe(viewLifecycleOwner) {
            viewBinding.pbProfileLoading.isVisible = it.status == Status.LOADING
            when (it.status) {
                Status.SUCCESS -> {
                    if (it.data == null) return@observe
                    setupProfile(it.data)
                }
                Status.ERROR -> {
                    showToastMessage(it.message ?: "")
                }
                else -> {
                    // Ignore
                }
            }
        }
    }

    /**
     * Setup profile for [userInfo]
     */
    private fun setupProfile(userInfo: UserInfo) {
        viewBinding.apply {
            setupAvatar(userInfo)
            tvFirstNameValue.text = userInfo.firstName
            tvLastNameValue.text = userInfo.lastName
            tvEmailValue.text = userInfo.email
        }
    }

    /**
     * Setup avatar for [userInfo]
     */
    private fun setupAvatar(userInfo: UserInfo) {
        val textDrawable = ViewUtils.createRoundNamePlaceholder(
            values = listOf(userInfo.firstName, userInfo.lastName),
            textColor = ResourcesCompat.getColor(resources, R.color.primary, requireContext().theme),
            backColor = ResourcesCompat.getColor(resources, R.color.white, requireContext().theme)
        )
        if (userInfo.avatar == null) {
            viewBinding.ivAvatar.setImageDrawable(textDrawable)
        } else {
            GlideApp.with(requireContext())
                .load(userInfo.avatar)
                .placeholder(textDrawable)
                .error(textDrawable)
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .circleCrop()
                .into(viewBinding.ivAvatar)
        }
    }

    companion object {
        const val USER_ID = "USER_ID"
    }
}