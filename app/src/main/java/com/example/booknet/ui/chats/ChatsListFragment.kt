package com.example.booknet.ui.chats

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.booknet.R
import com.example.booknet.databinding.FragmentChatsListBinding
import com.example.booknet.domain.chats.ChatsViewModel
import com.example.booknet.ui.chats.adapters.ChatsAdapter
import com.example.booknet.utils.Status

/**
 * Fragment for list of chats
 */
class ChatsListFragment : Fragment(R.layout.fragment_chats_list) {

    private val viewBinding by viewBinding(FragmentChatsListBinding::bind)

    private val chatsViewModel: ChatsViewModel by activityViewModels()

    private val navController by lazy { findNavController() }

    private val chatsAdapter: ChatsAdapter = ChatsAdapter { chat ->
        val arguments = bundleOf(
            CHAT_ID to chat.id,
            CHAT_NAME to chat.name,
        )
        navController.navigate(R.id.action_chatsListFragment_to_chatFragment, arguments)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupToolbar()
        setupListeners()
        setupChatsList()
        setupObservers()
        chatsViewModel.getChats()
    }

    /**
     * Setup toolbar listeners
     */
    private fun setupToolbar() {
        viewBinding.tbChats.setNavigationOnClickListener {
            navController.popBackStack()
        }
    }

    /**
     * Setup UI listeners
     */
    private fun setupListeners() {
        viewBinding.btnCreate.setOnClickListener {
            navController.navigate(R.id.action_chatsListFragment_to_chatCreateFragment)
        }
    }

    /**
     * Setup ViewModel observers
     */
    private fun setupObservers() {
        chatsViewModel.chatsState.observe(viewLifecycleOwner) {
            viewBinding.pbChatsLoading.isVisible = it.status == Status.LOADING
            when (it.status) {
                Status.SUCCESS -> {
                    chatsAdapter.submitList(it.data?.results)
                }
                Status.ERROR -> {
                    // TODO Implement
                }
                else -> {
                    // Ignore
                }
            }
        }
    }

    /**
     * Setup chats list
     */
    private fun setupChatsList() {
        viewBinding.rvChats.apply {
            adapter = chatsAdapter
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        }
    }

    companion object {
        const val CHAT_ID = "CHAT_ID"
        const val CHAT_NAME = "CHAT_NAME"
    }
}