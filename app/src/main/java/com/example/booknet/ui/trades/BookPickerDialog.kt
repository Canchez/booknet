package com.example.booknet.ui.trades

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.booknet.R
import com.example.booknet.databinding.DialogBookPickerBinding
import com.example.booknet.domain.profiles.ProfilesViewModel
import com.example.booknet.models.presentation.books.Book
import com.example.booknet.ui.trades.adapters.TradeOfferBooksAdapter
import com.example.booknet.utils.Status
import com.example.booknet.utils.showToastMessage
import timber.log.Timber

/**
 * Dialog for picking books in trade offers
 *
 * @param title Title of the dialog
 * @param userId ID of the user to get library
 * @param alreadyCheckedBooks Books that were checked before
 * @param confirmListener Action to perform on confirm
 */
class BookPickerDialog(
    private val title: String,
    private val userId: Long,
    private val alreadyCheckedBooks: List<Book>,
    private val confirmListener: (Set<Book>) -> Unit,
) : DialogFragment(R.layout.dialog_book_picker) {

    private val profilesViewModel: ProfilesViewModel by activityViewModels()

    private lateinit var dialogViewBinding: DialogBookPickerBinding

    private var checkedBooksSet: MutableSet<Book> = mutableSetOf()

    private var booksAdapter = TradeOfferBooksAdapter(
        showCheckboxes = true,
        onCheckBoxClicked = ::changeCheckedList
    )

    /**
     * Add or remove [book] to/from [checkedBooksSet] depending on [isChecked]
     */
    private fun changeCheckedList(book: Book, isChecked: Boolean) {
        if (isChecked) {
            checkedBooksSet.add(book)
        } else {
            checkedBooksSet.remove(book)
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialogBuilder = AlertDialog.Builder(requireContext())
        dialogViewBinding = DialogBookPickerBinding.inflate(layoutInflater)
        dialogBuilder.setView(dialogViewBinding.root)

        checkedBooksSet = alreadyCheckedBooks.toMutableSet()
        Timber.d("CheckedSet=$checkedBooksSet")

        dialogViewBinding.apply {
            tvTitle.text = title

            rvBooks.apply {
                adapter = booksAdapter
                layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
                addItemDecoration(DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL))
            }

            btnCancel.setOnClickListener { dismiss() }

            btnOk.setOnClickListener {
                Timber.d("confirm:CheckedBooksSet=$checkedBooksSet")
                confirmListener.invoke(checkedBooksSet)
                dismiss()
            }
        }

        profilesViewModel.getProfileBooks(userId)

        return dialogBuilder.create()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupObservers()
    }

    /**
     * Setup ViewModel observer for profile books
     */
    private fun setupObservers() {
        profilesViewModel.profileBooksState.observe(viewLifecycleOwner) {
            dialogViewBinding.apply {
                pbLoading.isVisible = it.status == Status.LOADING
                when (it.status) {
                    Status.SUCCESS -> {
                        val result = it.data?.results
                        val checkedBookIds = checkedBooksSet.map { it.id }
                        val mappedBooks = result?.map {
                            val modifiedBook = it.copy()
                            modifiedBook.isChecked = checkedBookIds.contains(modifiedBook.id)
                            modifiedBook
                        }

                        booksAdapter.submitList(mappedBooks)
                        profilesViewModel.clearProfileBooksState()
                    }
                    Status.ERROR -> {
                        showToastMessage(R.string.book_picker_library_error)
                        profilesViewModel.clearProfileBooksState()
                        dismiss()
                    }
                    else -> {
                        // Ignore
                    }
                }
            }
        }
    }
}