package com.example.booknet.ui.discussions

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.booknet.R
import com.example.booknet.databinding.FragmentDiscussionDetailsBinding
import com.example.booknet.domain.discussions.DiscussionsViewModel
import com.example.booknet.ui.profiles.UserProfileFragment
import com.example.booknet.utils.Status
import com.example.booknet.utils.ViewUtils
import com.example.booknet.utils.showToastMessage
import dagger.hilt.android.AndroidEntryPoint

/**
 * Fragment for Discussion screen
 */
@AndroidEntryPoint
class DiscussionDetailsFragment : Fragment(R.layout.fragment_discussion_details) {

    private val viewBinding by viewBinding(FragmentDiscussionDetailsBinding::bind)

    private val discussionsViewModel: DiscussionsViewModel by activityViewModels()

    private val navController by lazy { findNavController() }

    private var discussionId: Long = 0L

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        discussionId = arguments?.getLong(DISCUSSION_ID) ?: 0L
        discussionsViewModel.getDiscussion(discussionId)
        setupToolbar()
        setupListeners()
        setupObservers()
    }

    /**
     * Setup toolbar listeners
     */
    private fun setupToolbar() {
        viewBinding.tbDiscussionDetails.setNavigationOnClickListener { navController.navigateUp() }
    }

    /**
     * Setup UI listeners
     */
    private fun setupListeners() {
        viewBinding.apply {
            btnOpenComments.setOnClickListener {
                val arguments = bundleOf(
                    DISCUSSION_ID to discussionId,
                    DISCUSSION_TITLE to discussionsViewModel.currentDiscussionState.value?.data?.title
                )
                navController.navigate(R.id.action_discussionDetailsFragment_to_discussionCommentsFragment, arguments)
            }
        }
    }

    /**
     * Setup ViewModel observers
     */
    private fun setupObservers() {
        discussionsViewModel.currentDiscussionState.observe(viewLifecycleOwner) {
            viewBinding.pbDiscussionLoading.isVisible = it.status == Status.LOADING
            when (it.status) {
                Status.ERROR -> {
                    // TODO 11.12.2021: Implement
                    showToastMessage(it.message ?: "")
                }
                Status.SUCCESS -> {
                    val discussion = it.data ?: return@observe
                    viewBinding.apply {
                        tbDiscussionDetails.title = discussion.title
                        tvDiscussionTitle.text = discussion.title
                        tvDescriptionValue.text = discussion.description
                        ViewUtils.setupPersonItem(
                            context = requireContext(),
                            itemPersonBinding = lAuthor,
                            userInfo = discussion.author,
                            isOptionsVisible = false
                        )

                        lAuthor.root.setOnClickListener {
                            val arguments = bundleOf(
                                UserProfileFragment.USER_ID to discussion.author.id,
                            )
                            navController.navigate(R.id.action_discussionDetailsFragment_to_userProfileFragment, arguments)
                        }
                    }
                }
                else -> {
                    // Ignore
                }
            }
        }
    }

    companion object {
        const val DISCUSSION_ID = "DISCUSSION_ID"
        const val DISCUSSION_TITLE = "DISCUSSION_TITLE"
    }

}