package com.example.booknet.ui.dashboard

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.core.os.bundleOf
import androidx.core.view.GravityCompat
import androidx.core.view.MenuCompat
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import by.kirich1409.viewbindingdelegate.viewBinding
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.booknet.R
import com.example.booknet.databinding.FragmentDashboardBinding
import com.example.booknet.di.GlideApp
import com.example.booknet.domain.auth.AuthState
import com.example.booknet.domain.auth.AuthViewModel
import com.example.booknet.domain.books.BooksViewModel
import com.example.booknet.domain.discussions.DiscussionsViewModel
import com.example.booknet.models.presentation.users.UserInfo
import com.example.booknet.ui.books.BookDetailsFragment
import com.example.booknet.ui.dashboard.adapters.DashboardBooksAdapter
import com.example.booknet.ui.dashboard.adapters.DashboardDiscussionsAdapter
import com.example.booknet.ui.discussions.DiscussionDetailsFragment
import com.example.booknet.utils.*
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

/**
 * A Fragment for Dashboard
 */
@AndroidEntryPoint
class DashboardFragment : Fragment(R.layout.fragment_dashboard) {

    private val viewBinding by viewBinding(FragmentDashboardBinding::bind)

    private val authViewModel: AuthViewModel by activityViewModels()
    private val booksViewModel: BooksViewModel by activityViewModels()
    private val discussionsViewModel: DiscussionsViewModel by activityViewModels()

    private val navController by lazy { findNavController() }

    private lateinit var popularBooksAdapter: DashboardBooksAdapter
    private lateinit var popularDiscussionsAdapter: DashboardDiscussionsAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupToolbar()
        setupDrawerMenu()
        setupRecyclerViews()
        hideDrawerHeader()
        authViewModel.getUserInfo()
        setupObservers()
    }

    override fun onStart() {
        super.onStart()
        booksViewModel.getPopularBooks()
        discussionsViewModel.getPopularDiscussions()
    }

    /**
     * Setup toolbar actions
     */
    private fun setupToolbar() {
        viewBinding.tbDashboard.apply {
            MenuCompat.setGroupDividerEnabled(menu, true)
            setNavigationOnClickListener {
                val drawer = viewBinding.root
                if (drawer.isDrawerOpen(GravityCompat.START)) drawer.closeDrawer(GravityCompat.START) else drawer.openDrawer(GravityCompat.START)
            }
        }
    }

    /**
     * Setup observers of ViewModel
     */
    private fun setupObservers() {
        setupAuthObserver()
        setupPopularBooksObserver()
        setupPopularDiscussionsObserver()
        setupUserInfoObserver()
    }

    /**
     * Setup observer for auth states
     */
    private fun setupAuthObserver() {
        authViewModel.authState.observe(viewLifecycleOwner) {
            when (it) {
                AuthState.AuthSuccess, AuthState.AuthLoading -> {
                    // Ignore
                }
                AuthState.NoAuth, is AuthState.AuthError -> {
                    navController.navigate(R.id.action_dashboardFragment_to_authFragment)
                }
            }
        }
    }

    /**
     * Setup observer for popular books
     */
    private fun setupPopularBooksObserver() {
        booksViewModel.popularBooksState.observe(viewLifecycleOwner) {
            viewBinding.apply {
                pbPopularBooks.isVisible = it.status == Status.LOADING
                rvPopularBooks.isInvisible = it.status == Status.LOADING || it.status == Status.ERROR
                tvPopularBooksError.isVisible = it.status == Status.ERROR
            }
            when (it.status) {
                Status.SUCCESS -> {
                    popularBooksAdapter.submitList(it.data)
                }
                Status.ERROR -> {
                    showToastMessage(it.message ?: "")
                    viewBinding.tvPopularBooksError.text = it.message
                    Timber.e("Error with books loading: ${it.message}")
                }
                else -> {
                    // Ignore
                }
            }
        }
    }

    /**
     * Setup observer for popular discussions
     */
    private fun setupPopularDiscussionsObserver() {
        discussionsViewModel.popularDiscussionsState.observe(viewLifecycleOwner) {
            viewBinding.apply {
                pbPopularDiscussions.isVisible = it.status == Status.LOADING
                rvPopularDiscussions.isInvisible = it.status == Status.LOADING || it.status == Status.ERROR
                tvPopularDiscussionsError.isVisible = it.status == Status.ERROR
            }
            when (it.status) {
                Status.SUCCESS -> {
                    popularDiscussionsAdapter.submitList(it.data)
                }
                Status.ERROR -> {
                    showToastMessage(it.message ?: "")
                    viewBinding.tvPopularDiscussionsError.text = it.message
                    Timber.e("Error with discussions loading")
                }
                else -> {
                    // Ignore
                }
            }
        }
    }

    /**
     * Setup observer for user info
     */
    private fun setupUserInfoObserver() {
        authViewModel.userInfo.observe(viewLifecycleOwner) {
            it?.let { userInfoModel -> setupDrawerHeader(userInfoModel) }
        }
    }

    /**
     * Setup recycler views
     */
    private fun setupRecyclerViews() {
        viewBinding.apply {
            popularBooksAdapter = DashboardBooksAdapter {
                val arguments = bundleOf(
                    BookDetailsFragment.BOOK_ID to it.id,
                )
                navController.navigate(R.id.action_dashboardFragment_to_bookDetailsFragment, arguments)
            }
            rvPopularBooks.apply {
                adapter = popularBooksAdapter
                layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
                setHasFixedSize(true)
            }
            popularDiscussionsAdapter = DashboardDiscussionsAdapter { discussion ->
                val arguments = bundleOf(
                    DiscussionDetailsFragment.DISCUSSION_ID to discussion.id,
                )
                navController.navigate(R.id.action_dashboardFragment_to_discussionDetailsFragment, arguments)
            }
            rvPopularDiscussions.apply {
                adapter = popularDiscussionsAdapter
                layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
                setHasFixedSize(true)
            }
        }
    }

    /**
     * Setup drawer menu actions
     */
    private fun setupDrawerMenu() {
        viewBinding.nvDashboardNavDrawer.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.nav_drawer_logout -> {
                    closeDrawer()
                    authViewModel.logout()
                    false
                }
                R.id.nav_drawer_offers -> {
                    closeDrawer()
                    navController.navigate(R.id.action_dashboardFragment_to_tradeOffersFragment)
                    false
                }
                R.id.nav_drawer_library -> {
                    closeDrawer()
                    navController.navigate(R.id.action_dashboardFragment_to_libraryFragment)
                    false
                }
                R.id.nav_drawer_account -> {
                    closeDrawer()
                    navController.navigate(R.id.action_dashboardFragment_to_userProfileFragment)
                    false
                }
                R.id.nav_drawer_chats -> {
                    closeDrawer()
                    navController.navigate(R.id.action_dashboardFragment_to_chatsListFragment)
                    false
                }
                R.id.nav_drawer_discussions -> {
                    closeDrawer()
                    navController.navigate(R.id.action_dashboardFragment_to_usersDiscussionsFragment)
                    false
                }
                R.id.nav_drawer_token -> {
                    closeDrawer()
                    navController.navigate(R.id.action_dashboardFragment_to_tokenFragment)
                    false
                }
                else -> {
                    Timber.d("\"${it.title}\" menu item selected")
                    closeDrawer()
                    false
                }
            }
        }
    }

    /**
     * Close drawer
     */
    private fun closeDrawer() {
        val drawer = viewBinding.root
        if (drawer.isDrawerOpen(GravityCompat.START)) drawer.closeDrawer(GravityCompat.START)
    }

    /**
     * Hide drawer header
     */
    private fun hideDrawerHeader() {
        viewBinding.nvDashboardNavDrawer.getHeaderView(0).isInvisible = true
    }

    /**
     * Setup header for drawer
     */
    private fun setupDrawerHeader(userInfoModel: UserInfo) {
        val header = viewBinding.nvDashboardNavDrawer.getHeaderView(0)

        header.findViewById<TextView>(R.id.tvProfileName).text = listOf(userInfoModel.firstName, userInfoModel.lastName).joinNotEmpty(SPACE_DELIMITER)
        val avatarView = header.findViewById<ImageView>(R.id.ivAvatar)
        val textDrawable = ViewUtils.createRoundNamePlaceholder(
            values = listOf(userInfoModel.firstName, userInfoModel.lastName),
            textColor = ResourcesCompat.getColor(resources, R.color.primary, requireContext().theme),
            backColor = ResourcesCompat.getColor(resources, R.color.white, requireContext().theme)
        )
        if (userInfoModel.avatar == null) {
            avatarView.setImageDrawable(textDrawable)
        } else {
            GlideApp.with(requireContext())
                .load(userInfoModel.avatar)
                .placeholder(textDrawable)
                .error(textDrawable)
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .circleCrop()
                .into(avatarView)
        }

        header.isInvisible = false
    }
}