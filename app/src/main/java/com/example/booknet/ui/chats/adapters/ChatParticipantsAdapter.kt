package com.example.booknet.ui.chats.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.booknet.R
import com.example.booknet.databinding.ItemChatParticipantBinding
import com.example.booknet.di.GlideApp
import com.example.booknet.models.presentation.users.UserInfo
import com.example.booknet.ui.books.adapters.PeopleDiffCallback
import com.example.booknet.utils.ViewUtils

/**
 * Adapter for chat participants
 */
class ChatParticipantsAdapter(
//    private val currentUserId: Long,
    private val onItemClicked: (UserInfo) -> Unit,
    private val onOptionsClicked: (UserInfo, View) -> Unit,
) : ListAdapter<UserInfo, ChatParticipantsAdapter.ViewHolder>(PeopleDiffCallback) {

    class ViewHolder(
        private val viewBinding: ItemChatParticipantBinding,
//        private val currentUserId: Long,
        private val onItemClicked: (Int) -> Unit,
        private val onOptionsClicked: (Int, View) -> Unit
    ) : RecyclerView.ViewHolder(viewBinding.root) {

        init {
            viewBinding.apply {
                root.setOnClickListener { onItemClicked(absoluteAdapterPosition) }
                ivOptions.setOnClickListener { onOptionsClicked(absoluteAdapterPosition, root) }
            }
        }

        fun bind(userInfo: UserInfo) {
            viewBinding.apply {
                val context = root.context
                val resources = context.resources
                tvUserName.text = context.getString(R.string.books_people_full_name, userInfo.firstName, userInfo.lastName)
                val textDrawable = ViewUtils.createRoundNamePlaceholder(
                    values = listOf(userInfo.firstName, userInfo.lastName),
                    textColor = ResourcesCompat.getColor(resources, R.color.primary, context.theme),
                    backColor = ResourcesCompat.getColor(resources, R.color.white, context.theme)
                )
                if (userInfo.avatar == null) {
                    ivAvatar.setImageDrawable(textDrawable)
                } else {
                    GlideApp.with(context)
                        .load(userInfo.avatar)
                        .placeholder(textDrawable)
                        .error(textDrawable)
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .circleCrop()
                        .into(ivAvatar)
                }

                ivOptions.isVisible = false

                ivAvatar.isVisible = !userInfo.isSelected
                ivCheck.isVisible = userInfo.isSelected
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding = ItemChatParticipantBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(
            viewBinding = itemBinding,
//            currentUserId = currentUserId,
            onItemClicked = { position ->
                getItem(position)?.let { userInfo ->
                    onItemClicked(userInfo)
                }
            },
            onOptionsClicked = { position, view ->
                getItem(position)?.let { userInfo ->
                    onOptionsClicked(userInfo, view)
                }
            },
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}