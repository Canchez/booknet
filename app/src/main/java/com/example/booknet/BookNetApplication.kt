package com.example.booknet

import android.app.Application
import com.example.booknet.logging.LoggingModule
import dagger.hilt.android.HiltAndroidApp
import timber.log.Timber

/**
 * Application class for defining tree for Timber
 */
@HiltAndroidApp
class BookNetApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        Timber.plant(LoggingModule.provideLoggingTree())
    }
}