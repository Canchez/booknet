package com.example.booknet.di

import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

/**
 * Main module for app
 */
@Module
@InstallIn(SingletonComponent::class)
object MainModule