package com.example.booknet.di

import android.content.Context
import com.example.booknet.api.AccessTokenInterceptor
import com.example.booknet.api.ApiParameters.API_URL
import com.example.booknet.api.HttpClient
import com.example.booknet.api.RefreshTokenAuthenticator
import com.example.booknet.api.auth.AuthService
import com.example.booknet.api.books.BooksService
import com.example.booknet.api.books.LibraryService
import com.example.booknet.api.chats.ChatsService
import com.example.booknet.api.discussions.DiscussionsService
import com.example.booknet.api.profiles.ProfilesService
import com.example.booknet.api.trades.TradesService
import com.example.booknet.api.users.UserService
import com.example.booknet.data.auth.AuthRepository
import com.example.booknet.data.auth.AuthStorage
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/**
 * DI Module for API related objects
 */
@Module
@InstallIn(SingletonComponent::class)
object ApiModule {
    private const val AUTH_URL = "$API_URL/auth/"
    private const val API_BASE_URL = "$API_URL/"

    @Provides
    @Singleton
    fun provideAuthStorage(@ApplicationContext context: Context) =
        AuthStorage(context)

    @Provides
    @Singleton
    fun provideAccessTokenInterceptor(authStorage: AuthStorage) =
        AccessTokenInterceptor(authStorage)

    @Provides
    @Singleton
    fun provideRefreshTokenAuthenticator(authStorage: AuthStorage, authRepository: AuthRepository) =
        RefreshTokenAuthenticator(authStorage, authRepository)

    @Provides
    @Singleton
    fun provideAuthService(): AuthService =
        buildApiService(AUTH_URL, HttpClient.create())
            .create(AuthService::class.java)

    @Provides
    @Singleton
    fun provideUserService(
        accessTokenInterceptor: AccessTokenInterceptor,
        refreshTokenAuthenticator: RefreshTokenAuthenticator,
    ): UserService =
        buildApiService(AUTH_URL, HttpClient.create(accessTokenInterceptor, refreshTokenAuthenticator))
            .create(UserService::class.java)

    @Provides
    @Singleton
    fun provideBooksService(
        accessTokenInterceptor: AccessTokenInterceptor,
        refreshTokenAuthenticator: RefreshTokenAuthenticator,
    ): BooksService =
        buildApiService(API_BASE_URL, HttpClient.create(accessTokenInterceptor, refreshTokenAuthenticator))
            .create(BooksService::class.java)

    @Provides
    @Singleton
    fun provideDiscussionsService(
        accessTokenInterceptor: AccessTokenInterceptor,
        refreshTokenAuthenticator: RefreshTokenAuthenticator,
    ): DiscussionsService =
        buildApiService(API_BASE_URL, HttpClient.create(accessTokenInterceptor, refreshTokenAuthenticator))
            .create(DiscussionsService::class.java)

    @Provides
    @Singleton
    fun provideLibraryService(
        accessTokenInterceptor: AccessTokenInterceptor,
        refreshTokenAuthenticator: RefreshTokenAuthenticator,
    ): LibraryService =
        buildApiService(API_BASE_URL, HttpClient.create(accessTokenInterceptor, refreshTokenAuthenticator))
            .create(LibraryService::class.java)

    @Provides
    @Singleton
    fun provideTradesService(
        accessTokenInterceptor: AccessTokenInterceptor,
        refreshTokenAuthenticator: RefreshTokenAuthenticator,
    ): TradesService =
        buildApiService(API_BASE_URL, HttpClient.create(accessTokenInterceptor, refreshTokenAuthenticator))
            .create(TradesService::class.java)

    @Provides
    @Singleton
    fun provideProfilesService(
        accessTokenInterceptor: AccessTokenInterceptor,
        refreshTokenAuthenticator: RefreshTokenAuthenticator,
    ): ProfilesService =
        buildApiService(API_BASE_URL, HttpClient.create(accessTokenInterceptor, refreshTokenAuthenticator))
            .create(ProfilesService::class.java)


    @Provides
    @Singleton
    fun provideChatsService(
        accessTokenInterceptor: AccessTokenInterceptor,
        refreshTokenAuthenticator: RefreshTokenAuthenticator,
    ): ChatsService =
        buildApiService(API_BASE_URL, HttpClient.create(accessTokenInterceptor, refreshTokenAuthenticator))
            .create(ChatsService::class.java)

    /**
     * Build Retrofit Api Service with [baseUrl] and [client]
     */
    private fun buildApiService(baseUrl: String, client: OkHttpClient): Retrofit =
        Retrofit.Builder()
            .client(client)
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

}