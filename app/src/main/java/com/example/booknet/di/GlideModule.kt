package com.example.booknet.di

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

/**
 * Module for Glide
 */
@GlideModule
class GlideModule : AppGlideModule()