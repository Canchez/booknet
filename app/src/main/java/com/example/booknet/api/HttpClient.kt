package com.example.booknet.api

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor

/**
 * Methods for creation of HttpClient for APIs
 */
class HttpClient {
    companion object {

        /**
         * Create OkHttpClient
         *
         * Add HttpLogging interceptor
         */
        fun create() =
            OkHttpClient.Builder()
                .addInterceptor(
                    HttpLoggingInterceptor()
                        .apply { level = HttpLoggingInterceptor.Level.BODY }
                )
                .build()

        /**
         * Create OkHttpClient
         *
         * Add HttpLogging interceptor
         * Add Access Token interceptor
         * Add Refresh Token authenticator
         */
        fun create(
            accessTokenInterceptor: AccessTokenInterceptor,
            refreshTokenAuthenticator: RefreshTokenAuthenticator,
        ) =
            OkHttpClient.Builder()
                .addInterceptor(
                    HttpLoggingInterceptor()
                        .apply { level = HttpLoggingInterceptor.Level.BODY }
                )
                .addInterceptor(accessTokenInterceptor)
                .authenticator(refreshTokenAuthenticator)
                .build()
    }
}