package com.example.booknet.api.users

import com.example.booknet.models.entity.users.UserEntity
import retrofit2.Call
import retrofit2.http.GET

/**
 * Service for user related requests
 */
interface UserService {

    /**
     * User Info request
     */
    @GET("user-info/")
    fun getUserInfo(): Call<UserEntity>

}