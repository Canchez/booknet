package com.example.booknet.api

import okhttp3.OkHttpClient
import okhttp3.Request

/**
 * Utils methods for WebSockets
 */
object WebSocketUtils {

    /**
     * Create [OkHttpClient] for web socket
     *
     * TODO Investigate authentication
     */
    fun createWebSocketClient() =
        OkHttpClient.Builder()
            .build()

    /**
     * Build [Request] for web socket with [url]
     *
     * TODO Investigate authentication
     */
    fun buildWebSocketRequest(url: String) =
        Request.Builder()
            .url(url)
            .build()

}