package com.example.booknet.api

import com.example.booknet.data.auth.AuthStorage
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import javax.inject.Inject

/**
 * Interceptor that adds access token to requests
 */
class AccessTokenInterceptor @Inject constructor(
    private val authStorage: AuthStorage,
) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()

        if (authStorage.isUserLoggedIn == true) {
            request = request.addAccessToken()
        }

        return chain.proceed(request)
    }

    private fun Request.addAccessToken() =
        newBuilder()
            .header("Authorization", authStorage.authorizationHeader)
            .build()
}