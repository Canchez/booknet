package com.example.booknet.api.auth

import com.example.booknet.models.entity.auth.*
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

/**
 * Auth requests service
 */
interface AuthService {
    /**
     * Refresh token request
     */
    @POST("token/refresh/")
    suspend fun refreshAccessToken(@Body body: RefreshTokenRequestEntity): RefreshTokenResponseEntity

    /**
     * Registration request
     */
    @POST("registration/")
    fun register(@Body body: RegisterRequestEntity): Call<RegisterResponseEntity>

    /**
     * Login request
     */
    @POST("token/")
    fun getToken(@Body body: TokenRequestEntity): Call<TokenResponseEntity>
}