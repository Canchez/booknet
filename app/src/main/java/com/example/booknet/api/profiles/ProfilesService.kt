package com.example.booknet.api.profiles

import com.example.booknet.api.DataListWrapper
import com.example.booknet.models.entity.books.BookEntity
import com.example.booknet.models.entity.users.UserEntity
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Service for profile related requests
 */
interface ProfilesService {

    /**
     * Get books for profile with [profileId]
     *
     * Paginated
     */
    @GET("profiles/{id}/books/")
    fun getProfileBooks(
        @Path("id") profileId: Long,
        @Query("page") page: Int,
    ): Call<DataListWrapper<BookEntity>>

    /**
     * Search profiles using [searchQuery]
     *
     * Paginated
     */
    @GET("profiles/")
    fun searchProfiles(
        @Query("search") searchQuery: String,
    ): Call<DataListWrapper<UserEntity>>

    /**
     * Get profile with [profileId]
     */
    @GET("profiles/{profile_id}/")
    fun getProfile(
        @Path("profile_id") profileId: Long,
    ): Call<UserEntity>
}