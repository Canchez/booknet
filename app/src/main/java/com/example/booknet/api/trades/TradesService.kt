package com.example.booknet.api.trades

import com.example.booknet.api.DataListWrapper
import com.example.booknet.models.entity.trades.TradeOfferEntity
import com.example.booknet.models.entity.trades.TradeOfferPostEntity
import retrofit2.Call
import retrofit2.http.*

/**
 * Service for trade offer related requests
 */
interface TradesService {

    /**
     * Get trade offers for the user
     *
     * Paginated
     */
    @GET("trades/")
    fun getTrades(
        @Query("page") page: Int,
    ): Call<DataListWrapper<TradeOfferEntity>>

    /**
     * Add new [tradeOffer]
     */
    @POST("trades/")
    fun addTradeOffer(
        @Body tradeOffer: TradeOfferPostEntity,
    ): Call<TradeOfferEntity>

    /**
     * Get trade offer by [id]
     */
    @GET("trades/{id}/")
    fun getTradeById(
        @Path("id") id: Long,
    ): Call<TradeOfferEntity>

    /**
     * Update trade offer with [id] with info from [newTradeOffer]
     */
    @PUT("trades/{id}/")
    fun updateTradeById(
        @Path("id") id: Long,
        @Body newTradeOffer: TradeOfferPostEntity,
    ): Call<TradeOfferEntity>

    /**
     * Delete trade offer by [id]
     */
    @DELETE("trades/{id}/")
    fun deleteTradeById(
        @Path("id") id: Long,
    ): Call<Unit>
}