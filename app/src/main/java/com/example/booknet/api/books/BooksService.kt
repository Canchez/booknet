package com.example.booknet.api.books

import com.example.booknet.api.DataListWrapper
import com.example.booknet.models.domain.reviews.ReviewEntity
import com.example.booknet.models.domain.reviews.ReviewPostEntity
import com.example.booknet.models.entity.books.BookEntity
import com.example.booknet.models.entity.books.BookStatusEntity
import com.example.booknet.models.entity.users.UserEntity
import retrofit2.Call
import retrofit2.http.*

/**
 * Books related requests
 */
interface BooksService {

    /**
     * Search books in app using [searchQuery]
     *
     * Paginated
     */
    @GET("books/")
    fun searchBooks(
        @Query("search") searchQuery: String,
        @Query("page") page: Int,
    ): Call<DataListWrapper<BookEntity>>

    /**
     * Get most viewed books
     */
    @GET("books/most-viewed/")
    fun getMostViewedBooks(): Call<List<BookEntity>>

    /**
     * Get book by [id]
     */
    @GET("books/{id}/")
    fun getBookById(
        @Path("id") id: Long,
    ): Call<BookEntity>

    /**
     * Get reviews for the book with [id]
     */
    @GET("books/{id}/reviews/")
    fun getReviewsByBookId(
        @Path("id") id: Long,
    ): Call<List<ReviewEntity>>

    /**
     * Add [review] to the book with [id]
     */
    @POST("books/{id}/add_feedback/")
    fun addReviewToBookById(
        @Path("id") id: Long,
        @Body review: ReviewPostEntity
    ): Call<ReviewEntity>

    /**
     * Get people who want to read the book with [bookId]
     */
    @GET("books/{book_id}/want-to-read/")
    fun getPeopleWantToRead(
        @Path("book_id") bookId: Long,
    ): Call<List<UserEntity>>

    /**
     * Get people who want to trade book with [bookId]
     */
    @GET("books/{book_id}/want-to-trade/")
    fun getPeopleWantToTrade(
        @Path("book_id") bookId: Long,
    ): Call<List<UserEntity>>

    /**
     * Change user [status] of the book with [bookId]
     */
    @PATCH("books/{book_id}/change-status/")
    fun changeBookStatus(
        @Path("book_id") bookId: Long,
        @Body status: BookStatusEntity,
    ): Call<Unit>
}