package com.example.booknet.api.chats

import com.example.booknet.api.DataListWrapper
import com.example.booknet.models.entity.chats.ChatEntity
import com.example.booknet.models.entity.chats.ChatPostEntity
import retrofit2.Call
import retrofit2.http.*

/**
 * Service for chat related requests
 */
interface ChatsService {

    /**
     * Get chats of the user
     *
     * Paginated
     */
    @GET("dialogs/")
    fun getChats(
        @Query("page") page: Int,
    ): Call<DataListWrapper<ChatEntity>>

    /**
     * Get chat by [chatId]
     */
    @GET("dialogs/{dialog_id}")
    fun getChatById(
        @Path("dialog_id") chatId: Long,
    ): Call<ChatEntity>

    /**
     * Create chat with [chatEntity]
     */
    @POST("dialogs/")
    fun createChat(
        @Body chatEntity: ChatPostEntity,
    ): Call<ChatEntity>
}