package com.example.booknet.api.discussions

import com.example.booknet.api.DataListWrapper
import com.example.booknet.models.entity.discussions.CommentEntity
import com.example.booknet.models.entity.discussions.CommentPostEntity
import com.example.booknet.models.entity.discussions.DiscussionEntity
import com.example.booknet.models.entity.discussions.DiscussionPostEntity
import retrofit2.Call
import retrofit2.http.*

/**
 * Retrofit service for discussions requests
 */
interface DiscussionsService {

    /**
     * Get discussions of the app
     */
    @GET("discussions/")
    fun getDiscussions(): Call<List<DiscussionEntity>>

    /**
     * Get list of most commented discussion
     */
    @GET("discussions/most-commented/")
    fun getPopularDiscussions(): Call<List<DiscussionEntity>>

    /**
     * Get discussions created by current user
     */
    @GET("discussions/my/")
    fun getUsersDiscussions(): Call<List<DiscussionEntity>>

    /**
     * Get discussions that current user has commented on
     */
    @GET("discussions/commented-by-user/")
    fun getCommentedByUsersDiscussions(): Call<List<DiscussionEntity>>

    /**
     * Add [discussion]
     */
    @POST("discussions/")
    fun addDiscussion(@Body discussion: DiscussionPostEntity): Call<DiscussionEntity>

    /**
     * Get discussion by [id]
     */
    @GET("discussions/{id}/")
    fun getDiscussionById(
        @Path("id") id: Long,
    ): Call<DiscussionEntity>

    /**
     * Get comments of the discussion with [id]
     *
     * Paginated
     */
    @GET("discussions/{discussion_id}/comments/")
    fun getComments(
        @Path("discussion_id") id: Long,
        @Query("page") page: Int,
    ): Call<DataListWrapper<CommentEntity>>

    /**
     * Add [comment] to discussion with [id]
     */
    @POST("discussions/{discussion_id}/comments/")
    fun addComment(
        @Path("discussion_id") id: Long,
        @Body comment: CommentPostEntity,
    ): Call<CommentEntity>

    /**
     * Delete comment with [commentId] from discussion with [discussionId]
     */
    @DELETE("discussions/{discussion_id}/comments/{comment_id}/")
    fun deleteComment(
        @Path("discussion_id") discussionId: Long,
        @Path("comment_id") commentId: Long,
    ): Call<Unit>
}