package com.example.booknet.api

import com.google.gson.annotations.SerializedName

data class DataListWrapper<T>(
    @SerializedName("count") val count: Int,
    @SerializedName("total_pages") val totalPages: Int,
    @SerializedName("next") val next: String?,
    @SerializedName("previous") val previous: String?,
    @SerializedName("results") val results: List<T>,
)