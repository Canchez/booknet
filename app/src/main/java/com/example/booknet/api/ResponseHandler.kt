package com.example.booknet.api

import com.example.booknet.utils.Resource
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Response
import retrofit2.awaitResponse
import timber.log.Timber

object ResponseHandler {

    /**
     * Parse [response]
     *
     * @param expectData True if response should contain data and treat null data as error, false - otherwise
     */
    private fun <T> handle(response: Response<T>, expectData: Boolean): Resource<T> {
        return if (response.isSuccessful) {
            when {
                response.body() != null -> {
                    Resource.success(response.body(), responseCode = response.code())
                }
                response.raw().request.method == "DELETE" -> {
                    Resource.success(response.body(), responseCode = response.code())
                }
                !expectData -> {
                    Resource.success(response.body(), responseCode = response.code())
                }
                else -> {
                    Resource.error("Data is null", responseCode = response.code())
                }
            }
        } else {
            val errorResponse = Gson().fromJson(String(response.errorBody()?.bytes()!!), ErrorResponse::class.java)
            Timber.e(errorResponse.message)
            Resource.error(errorResponse.message, null, responseCode = response.code())
        }
    }

    /**
     * Try to execute request and parse the response from [call]
     *
     * @param expectData True if request should return data and treat null data as error, false - otherwise
     */
    suspend fun <T> executeRequest(call: Call<T>, expectData: Boolean = true): Resource<T> {
        return try {
            val response = call.awaitResponse()
            handle(response, expectData)
        } catch (e: Exception) {
            Timber.e("Error with request: ${e.stackTraceToString()}")
            Resource.error(e.message ?: "", responseCode = null)
        }
    }
}