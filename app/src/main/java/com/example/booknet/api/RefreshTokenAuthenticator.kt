package com.example.booknet.api

import com.example.booknet.data.auth.AuthRepository
import com.example.booknet.data.auth.AuthStorage
import kotlinx.coroutines.runBlocking
import okhttp3.Authenticator
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route
import javax.inject.Inject

/**
 * Authenticator that refreshes access token when it is not alive
 */
class RefreshTokenAuthenticator @Inject constructor(
    private val authStorage: AuthStorage,
    private val authRepository: AuthRepository,
) : Authenticator {
    override fun authenticate(route: Route?, response: Response): Request? {
        if (!isAccessTokenRequired(response)) return null

        runBlocking {
            authStorage.refresh(
                authRepository.refreshAccessToken(
                    authStorage.refreshTokenData
                )
            )
        }

        return response.request.addAccessToken()
    }

    private fun isAccessTokenRequired(response: Response) =
        response.request.header("Authorization") != null

    private fun Request.addAccessToken() =
        newBuilder()
            .header("Authorization", authStorage.authorizationHeader)
            .build()
}