package com.example.booknet.api

/**
 * Parameters for APIs
 */
object ApiParameters {

    /**
     * HTTPS protocol prefix
     */
    private const val HTTPS = "https://"

    /**
     * WSS protocol prefix
     */
    private const val WSS = "wss://"

    /**
     * HTTP protocol prefix
     */
    private const val HTTP = "http://"

    /**
     * WS protocol prefix
     */
    private const val WS = "ws://"

    /**
     * Base site URL
     */
    private const val SITE_URL = "91.243.85.208:8000"

    /**
     * Base API url
     */
    const val API_URL = "$HTTP$SITE_URL/api"

    /**
     * Url for WebSockets
     */
    const val WEB_SOCKET_BASE_URL = "$WS$SITE_URL/ws"
}