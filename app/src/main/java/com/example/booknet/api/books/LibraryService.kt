package com.example.booknet.api.books

import com.example.booknet.api.DataListWrapper
import com.example.booknet.models.entity.books.BookEntity
import com.example.booknet.models.entity.books.BookPostEntity
import com.example.booknet.models.entity.books.LibraryBookPostEntity
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

/**
 * Service for user library related requests
 */
interface LibraryService {

    /**
     * Get library for current user
     *
     * Paginated
     */
    @GET("books/library/")
    fun getLibrary(
        @Query("page") page: Int,
    ): Call<DataListWrapper<BookEntity>>

    /**
     * Add existing [book] to the library
     */
    @POST("books/library/add/")
    fun addBookToLibrary(
        @Body book: LibraryBookPostEntity
    ): Call<BookEntity>

    /**
     * Create new [book] and add to the library
     */
    @POST("books/library/create/")
    fun createBook(
        @Body book: BookPostEntity,
    ): Call<BookEntity>
}