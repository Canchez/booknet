package com.example.booknet.utils

import com.example.booknet.api.DataListWrapper

enum class Status {
    SUCCESS,
    ERROR,
    LOADING,
    EMPTY,
}

data class Resource<out T>(
    val status: Status,
    val data: T?,
    val message: String?,
    val responseCode: Int?,
) {
    companion object {

        fun <T> success(data: T?, responseCode: Int? = null): Resource<T> {
            return Resource(Status.SUCCESS, data, null, responseCode)
        }

        fun <T> error(message: String, data: T? = null, responseCode: Int? = null): Resource<T> {
            return Resource(Status.ERROR, data, message, responseCode)
        }

        fun <T> loading(data: T? = null): Resource<T> {
            return Resource(Status.LOADING, data, null, null)
        }

        fun <T> empty(): Resource<T> {
            return Resource(Status.EMPTY, null, null, null)
        }
    }
}

/**
 * Perform [transformation] on [Resource] from [T] to [R]
 */
fun <T, R> Resource<T>.transform(transformation: (T?) -> R?): Resource<R> {
    val transformedData = transformation(this.data)
    return Resource(status = this.status, data = transformedData, message = this.message, responseCode = this.responseCode)
}

fun <T, R> Resource<List<T>>.transformList(transformation: (T) -> R): Resource<List<R>> {
    val transformedList = this.data?.mapNotNull { transformation(it) } ?: emptyList()
    return Resource(status = this.status, data = transformedList, message = this.message, responseCode = this.responseCode)
}

fun <T, R> Resource<DataListWrapper<T>>.transformWithWrapper(transformation: (T) -> R): Resource<DataListWrapper<R>> {
    if (data == null) return Resource(
        status = this.status,
        data = null,
        message = this.message,
        responseCode = this.responseCode,
    )
    val transformedList = this.data.results.map { transformation(it) }
    val transformedData = DataListWrapper(
        count = data.count,
        totalPages = data.totalPages,
        next = data.next,
        previous = data.previous,
        results = transformedList
    )
    return Resource(
        status = this.status,
        data = transformedData,
        message = this.message,
        responseCode = this.responseCode,
    )
}

fun <T> Resource<DataListWrapper<T>>.combineWithWrapper(other: Resource<DataListWrapper<T>>): Resource<DataListWrapper<T>>? {
    val errorResource = when {
        this.status == Status.ERROR -> this
        other.status == Status.ERROR -> other
        else -> null
    }
    if (errorResource != null) return null

    val thisList = this.data?.results ?: emptyList()
    val otherList = other.data?.results ?: emptyList()

    val combinedList = thisList.plus(otherList)

    val combinedData = DataListWrapper(
        count = other.data?.count ?: 1,
        totalPages = other.data?.totalPages ?: 1,
        next = other.data?.next,
        previous = this.data?.previous,
        results = combinedList
    )

    return Resource(
        status = other.status,
        data = combinedData,
        message = other.message,
        responseCode = other.responseCode,
    )
}

/**
 * Filter data in [DataListWrapper] using [predicate]
 */
fun <T> Resource<DataListWrapper<T>>.filterWrapper(predicate: (T) -> Boolean): Resource<DataListWrapper<T>> {
    val oldList = this.data?.results ?: emptyList()
    val newList = oldList.filter { predicate(it) }

    val oldData = this.data
    val newData = DataListWrapper(
        count = oldData?.count ?: 1,
        totalPages = oldData?.totalPages ?: 1,
        next = oldData?.next,
        previous = oldData?.previous,
        results = newList,
    )
    return Resource(
        status = this.status,
        data = newData,
        message = this.message,
        responseCode = this.responseCode,
    )
}