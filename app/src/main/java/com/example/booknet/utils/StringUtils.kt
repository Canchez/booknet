package com.example.booknet.utils

import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat


/**
 * Empty delimiter for joining strings
 */
const val EMPTY_DELIMITER = ""

/**
 * Space delimiter for joining strings
 */
const val SPACE_DELIMITER = " "

/**
 * Returns a string containing the non-null and non-empty strings as Iterable object joined by [delimiter].
 */
@JvmOverloads
fun Iterable<Any?>.joinNotEmpty(delimiter: CharSequence, transform: ((String) -> (CharSequence))? = null) =
    this.mapNotNull { it?.toString() }
        .filterNot { it.isEmpty() }
        .joinToString(delimiter, transform = transform)

/**
 * Returns a string containing the non-null and non-empty strings as string array joined by [delimiter].
 */
@JvmOverloads
fun List<String?>.joinNotEmpty(delimiter: CharSequence, transform: ((String) -> (CharSequence))? = null) =
    this.asIterable().joinNotEmpty(delimiter, transform = transform)

/**
 * Returns concatenated first letters from strings in string array in the upper case.
 */
fun List<String?>.extractInitials() = this.joinNotEmpty(EMPTY_DELIMITER) { it.first().uppercaseChar().toString() }

/**
 * Get first and last item from string list
 *
 * If only one item is available - take only first one
 */
fun List<String>.getFirstAndLast(): List<String> = when (size) {
    0 -> emptyList()
    1 -> listOf(first())
    else -> listOf(first(), last())
}

/**
 * Convert DateTime string to [DateTime]
 */
fun String.toDate(): DateTime {
    val dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'")
    return DateTime.parse(this, dateTimeFormatter)
}