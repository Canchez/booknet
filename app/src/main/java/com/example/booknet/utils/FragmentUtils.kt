package com.example.booknet.utils

import android.widget.Toast
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment

/**
 * Show toast with [message] with [length]
 */
fun Fragment.showToastMessage(message: String, length: Int = Toast.LENGTH_SHORT) =
    Toast.makeText(requireContext(), message, length).show()

/**
 * Show toast with message from [messageResourceId] with [length]
 */
fun Fragment.showToastMessage(@StringRes messageResourceId: Int, vararg formatArgs: Any?, length: Int = Toast.LENGTH_SHORT) =
    showToastMessage(getString(messageResourceId, *formatArgs), length)