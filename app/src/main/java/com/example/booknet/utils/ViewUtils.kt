package com.example.booknet.utils

import android.content.Context
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import com.amulyakhare.textdrawable.TextDrawable
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.booknet.R
import com.example.booknet.databinding.ItemPersonBinding
import com.example.booknet.di.GlideApp
import com.example.booknet.models.presentation.users.UserInfo

object ViewUtils {

    /**
     * Create round text drawable with the first letters of the passed strings
     *
     * @param values strings
     * @return created drawable
     */
    fun createRoundNamePlaceholder(values: List<String?>, textColor: Int, backColor: Int): TextDrawable {
        val text = if (values.isEmpty()) "" else values.extractInitials()
        return TextDrawable.builder()
            .beginConfig()
            .textColor(textColor)
            .endConfig()
            .buildRound(text, backColor)
    }

    /**
     * Create rectangle text drawable with the first letters of the passed strings
     *
     * @param values strings
     * @return created drawable
     */
    fun createRectNamePlaceholder(values: List<String?>, textColor: Int, backColor: Int): TextDrawable {
        val text = if (values.isEmpty()) "" else values.extractInitials()
        return TextDrawable.builder()
            .beginConfig()
            .textColor(textColor)
            .endConfig()
            .buildRect(text, backColor)
    }

    /**
     * Setup [itemPersonBinding] with [userInfo] and [isOptionsVisible]
     */
    fun setupPersonItem(
        context: Context,
        itemPersonBinding: ItemPersonBinding,
        userInfo: UserInfo,
        isOptionsVisible: Boolean,
    ) {
        itemPersonBinding.apply {
            val resources = context.resources
            tvUserName.text = context.getString(R.string.books_people_full_name, userInfo.firstName, userInfo.lastName)
            val textDrawable = createRoundNamePlaceholder(
                values = listOf(userInfo.firstName, userInfo.lastName),
                textColor = ResourcesCompat.getColor(resources, R.color.primary, context.theme),
                backColor = ResourcesCompat.getColor(resources, R.color.white, context.theme)
            )
            if (userInfo.avatar == null) {
                ivAvatar.setImageDrawable(textDrawable)
            } else {
                GlideApp.with(context)
                    .load(userInfo.avatar)
                    .placeholder(textDrawable)
                    .error(textDrawable)
                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                    .circleCrop()
                    .into(ivAvatar)
            }

            ivOptions.isVisible = isOptionsVisible
        }
    }
}