package com.example.booknet.mappers.discussions

import com.example.booknet.mappers.users.toDomain
import com.example.booknet.mappers.users.toPresentation
import com.example.booknet.models.domain.discussions.CommentDomain
import com.example.booknet.models.domain.discussions.DiscussionDomain
import com.example.booknet.models.entity.discussions.CommentEntity
import com.example.booknet.models.entity.discussions.DiscussionEntity
import com.example.booknet.models.presentation.discussions.Comment
import com.example.booknet.models.presentation.discussions.DashboardDiscussion
import com.example.booknet.models.presentation.discussions.Discussion
import com.example.booknet.utils.toDate

/// Entity -> Domain

/**
 * Convert [DiscussionEntity] to [DiscussionDomain]
 */
fun DiscussionEntity.toDomain(): DiscussionDomain =
    DiscussionDomain(
        id = this.id,
        title = this.title,
        description = this.description,
        comments = this.comments
            .map { it.toDomain() }
            .sortedBy { it.dateCreated },
        author = this.author.toDomain(),
    )

/**
 * Convert [CommentEntity] to [CommentDomain]
 */
fun CommentEntity.toDomain(): CommentDomain =
    CommentDomain(
        id = this.id,
        message = this.message,
        author = this.author.toDomain(),
        dateCreated = this.dateCreated.toDate(),
        dateModified = this.dateModified.toDate(),
    )

/// Domain -> Presentation

/**
 * Convert [DiscussionDomain] to [DashboardDiscussion] for presentation on dashboard
 */
fun DiscussionDomain.toDashboardPresentation(): DashboardDiscussion =
    DashboardDiscussion(
        id = this.id,
        name = this.title,
    )

/**
 * Convert [DiscussionDomain] to [Discussion]
 */
fun DiscussionDomain.toPresentation(): Discussion =
    Discussion(
        id = this.id,
        title = this.title,
        description = this.description,
        comments = this.comments.map { it.toPresentation() },
        author = this.author.toPresentation(),
    )

/**
 * Convert [CommentDomain] to [Comment]
 */
fun CommentDomain.toPresentation(): Comment =
    Comment(
        id = this.id,
        message = this.message,
        author = this.author.toPresentation(),
        dateCreated = this.dateCreated,
    )
