package com.example.booknet.mappers.users

import com.example.booknet.models.domain.users.UserInfoDomain
import com.example.booknet.models.entity.users.UserEntity
import com.example.booknet.models.presentation.users.UserInfo

/// Entity -> Domain

/**
 * Convert [UserEntity] to [UserInfoDomain]
 */
fun UserEntity.toDomain(): UserInfoDomain =
    UserInfoDomain(
        id = this.id,
        firstName = this.firstName,
        lastName = this.lastName,
        email = this.email,
        avatar = this.avatar,
    )

/// Domain -> Presentation

/**
 * Convert [UserInfoDomain] to [UserInfo] for presentation
 */
fun UserInfoDomain.toPresentation(): UserInfo =
    UserInfo(
        id = this.id,
        firstName = this.firstName,
        lastName = this.lastName,
        email = this.email,
        avatar = this.avatar,
    )