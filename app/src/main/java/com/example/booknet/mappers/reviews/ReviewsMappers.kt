package com.example.booknet.mappers.reviews

import com.example.booknet.mappers.users.toDomain
import com.example.booknet.mappers.users.toPresentation
import com.example.booknet.models.domain.reviews.ReviewEntity
import com.example.booknet.models.entity.reviews.ReviewDomain
import com.example.booknet.models.presentation.reviews.Review

/// Entity -> Domain

/**
 * Convert [ReviewEntity] to [ReviewDomain]
 */
fun ReviewEntity.toDomain(): ReviewDomain =
    ReviewDomain(
        mark = this.mark,
        feedback = this.feedback,
        user = this.user.toDomain(),
    )

/// Domain -> Presentation

/**
 * Convert [ReviewDomain] to [Review]
 */
fun ReviewDomain.toPresentation(): Review =
    Review(
        mark = this.mark,
        feedback = this.feedback,
        user = this.user.toPresentation(),
    )