package com.example.booknet.mappers.chats

import com.example.booknet.mappers.users.toDomain
import com.example.booknet.mappers.users.toPresentation
import com.example.booknet.models.domain.chats.ChatDomain
import com.example.booknet.models.domain.chats.ChatMessageDomain
import com.example.booknet.models.entity.chats.ChatEntity
import com.example.booknet.models.entity.chats.ChatMessageEntity
import com.example.booknet.models.presentation.chats.Chat
import com.example.booknet.models.presentation.chats.ChatMessage
import org.joda.time.DateTime

/// Entity -> Domain

/**
 * Convert [ChatEntity] to [ChatDomain]
 */
fun ChatEntity.toDomain(): ChatDomain =
    ChatDomain(
        id = this.id,
        name = this.name,
        image = this.image,
        attendees = this.attendees.map { it.toDomain() },
        messages = this.messages.map { it.toDomain() },
    )

/**
 * Convert [ChatMessageEntity] to [ChatMessageDomain]
 */
fun ChatMessageEntity.toDomain(): ChatMessageDomain =
    ChatMessageDomain(
        id = this.id,
        senderId = this.senderId,
        senderData = this.senderData.toDomain(),
        content = this.content,
        createdAt = this.createdAt,
    )

/// Domain -> Presentation

/**
 * Convert [ChatDomain] to [Chat]
 */
fun ChatDomain.toPresentation(): Chat =
    Chat(
        id = this.id,
        name = this.name,
        image = this.image,
        attendees = this.attendees.map { it.toPresentation() },
        messages = this.messages.map { it.toPresentation() },
    )

/**
 * Convert [ChatMessageDomain] to [ChatMessage]
 */
fun ChatMessageDomain.toPresentation(): ChatMessage =
    ChatMessage(
        id = this.id,
        senderId = this.senderId,
        senderData = this.senderData.toPresentation(),
        content = this.content,
        createdAt = DateTime.parse(this.createdAt),
    )