package com.example.booknet.mappers.trades

import com.example.booknet.mappers.books.toDomain
import com.example.booknet.mappers.books.toPresentation
import com.example.booknet.mappers.users.toDomain
import com.example.booknet.mappers.users.toPresentation
import com.example.booknet.models.domain.trades.TradeOfferDomain
import com.example.booknet.models.entity.trades.TradeOfferEntity
import com.example.booknet.models.presentation.trades.TradeOffer
import com.example.booknet.models.presentation.trades.TradeOfferStatus

/// Entity -> Domain

/**
 * Convert [TradeOfferEntity] to [TradeOfferDomain]
 */
fun TradeOfferEntity.toDomain() =
    TradeOfferDomain(
        id = this.id,
        senderId = this.senderId,
        senderData = this.senderData.toDomain(),
        receiverId = this.receiverId,
        receiverData = this.receiverData.toDomain(),
        suggestedBooksIds = this.suggestedBooksIds,
        suggestedBooksData = this.suggestedBooksData.map { it.toDomain() },
        requestedBooksIds = this.requestedBooksIds,
        requestedBooksData = this.requestedBooksData.map { it.toDomain() },
        message = this.message,
        status = this.status,
    )

/// Domain -> Presentation

/**
 * Convert [TradeOfferDomain] to [TradeOffer]
 */
fun TradeOfferDomain.toPresentation() =
    TradeOffer(
        id = this.id,
        senderId = this.senderId,
        senderData = this.senderData.toPresentation(),
        receiverId = this.receiverId,
        receiverData = this.receiverData.toPresentation(),
        suggestedBooksIds = this.suggestedBooksIds,
        suggestedBooksData = this.suggestedBooksData.map { it.toPresentation() },
        requestedBooksIds = this.requestedBooksIds,
        requestedBooksData = this.requestedBooksData.map { it.toPresentation() },
        message = this.message,
        status = TradeOfferStatus.fromServerString(this.status),
    )