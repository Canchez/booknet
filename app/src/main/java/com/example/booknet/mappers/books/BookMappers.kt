package com.example.booknet.mappers.books

import com.example.booknet.models.domain.books.BookDomain
import com.example.booknet.models.domain.books.BookPostDomain
import com.example.booknet.models.entity.books.BookEntity
import com.example.booknet.models.entity.books.BookPostEntity
import com.example.booknet.models.presentation.books.Book
import com.example.booknet.models.presentation.books.BookStatus
import com.example.booknet.models.presentation.books.DashboardBook

/// Entity -> Domain

/**
 * Convert [BookEntity] to [BookDomain]
 */
fun BookEntity.toDomain(): BookDomain =
    BookDomain(
        id = this.id,
        title = this.title,
        author = this.author,
        genre = this.genre,
        pages = this.pages,
        viewersCount = this.viewersCount,
        readersCount = this.readersCount,
        description = this.description,
        averageRating = this.averageRating,
        reviewsAmount = this.reviewsCount,
        wantToReadAmount = this.wantToReadCount,
        wantToTradeAmount = this.wantToTradeCount,
        status = this.status,
    )

/// Domain -> Presentation

/**
 * Convert [BookDomain] to [Book] for presentation
 */
fun BookDomain.toPresentation(): Book =
    Book(
        id = this.id,
        title = this.title,
        author = this.author,
        genre = this.genre,
        pages = this.pages,
        readersCount = this.readersCount,
        description = this.description,
        averageRating = this.averageRating,
        reviewsAmount = this.reviewsAmount,
        wantToReadAmount = this.wantToReadAmount,
        wantToTradeAmount = this.wantToTradeAmount,
        status = BookStatus.fromServerString(this.status),
    )

/**
 * Convert [BookDomain] to [DashboardBook] for presentation on dashboard
 */
fun BookDomain.toDashboardPresentation(): DashboardBook =
    DashboardBook(
        id = this.id,
        title = this.title,
    )

// Domain -> Entity

fun BookPostDomain.toEntity(): BookPostEntity =
    BookPostEntity(
        title = this.title,
        pages = this.pages,
        description = this.description,
        status = this.status.serverString,
    )