package com.example.booknet.logging

import timber.log.Timber

/**
 * Module for logging
 */
object LoggingModule {

    /**
     * Provide tree for debug logging
     */
    fun provideLoggingTree(): Timber.Tree {
        return Timber.DebugTree()
    }
}